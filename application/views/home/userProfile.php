<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>rotary</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/bootstrap.min.css');?>">
    <!-- bootsnav -->
    <link href="<?php echo base_url('assets/front-end/css/bootsnav.css');?>" rel="stylesheet">
    <!-- animate.css -->
    <link href="<?php echo base_url('assets/front-end/css/animate.css'); ?>" rel="stylesheet">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- slider -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/vegas.min.css'); ?>">
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.theme.default.min.css'); ?>">
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400i,700|Montserrat+Alternates:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

    <!-- flat Icon -->
    <link href="<?php echo base_url('assets/front-end/css/flaticon.css');?>" rel="stylesheet">

    <!-- custom css -->
    <link href="<?php echo base_url('assets/front-end/css/userProfile.css');?>" rel="stylesheet">
</head>

<body>
  
  <nav class="navbar navbar-default navbar-fixed-top bootsnav" data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand navbar-brand-res hidden" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="<?php echo base_url('home/index'); ?>">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Club History</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/clubHistory'); ?>">History</a></li>
                            <li><a href="<?php echo base_url('home/clubPresident'); ?>">Club President</a></li>
                            <li><a href="<?php echo base_url('home/ourLeader'); ?>">Our Leader</a></li>
                            <li><a href="<?php echo base_url('home/director'); ?>">Director</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown">Member List</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/memberList'); ?>">All Members</a></li>
                            <li><a href="<?php echo base_url('home/iCom'); ?>">INSTALLATION COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/subCom'); ?>">SUB COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/board'); ?>">BOARD OF DIRECTORS 2017-2018</a></li>
                            <li><a href="<?php echo base_url('home/pastLeaders'); ?>">Our Past Leaders</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('home/pp'); ?>">Permanent Project</a></li>
                    
                    <a class="navbar-brand navbar-brand-sec" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
                    
                    <li><a href="<?php echo base_url('home/photos'); ?>">Photo Gallery</a></li>
                    <li><a href="<?php echo base_url('home/allNews'); ?>">All News</a></li>
                    <li><a href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                    <li><a href="<?php echo base_url('home/logout'); ?>">Log Out</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

  <section class="head"></section>
  
    <section class="fst">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="pro-pic">
                <img src="<?php echo base_url("$data->memberImage"); ?>" alt="...">
            </div>
            <h2 class="user-name"><?php echo $data->memberName; ?></h2>
          </div>
        </div>
      </div>
    </section>

    <section id="logout">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar">
              <nav class="navbar navbar-default">
                <div id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification <span class="badge"><?php echo count($infos); ?></span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                        <h4 class="dropdown-header">New Alerts:</h4>
                        <?php foreach ($infos as $info) {
                         ?>
                        <hr>
                        <a class="dropdown-item" href="#">
                          <span class="text-success">
                            <strong>
                              <i class="fa fa-long-arrow-up"></i>
                              <?php echo $info->eventTitle; ?></strong>
                          </span>
                          <span class="small float-right text-muted"><?php echo $info->eventDate; ?></span>
                          <div class="dropdown-message small">This is to inform you that u have TK. <?php echo ($info->perMemberCost-$info->eventCostPaidAmount)?> Due on this Event.</div>
                        </a>
                        <hr>
                        <?php } ?>
                        <a class="dropdown-item small" href="#">
                          View all alerts
                        </a>
                      </ul>
                    </li>
                    <!-- <li>
                      <a href="<?php echo base_url('Login/memberLogout'); ?>">Log Out  <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                    </li> -->
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="details">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="details">
              <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About</a></li>
                  <li role="presentation"><a href="#balance" aria-controls="balance" role="tab" data-toggle="tab">Balance</a></li>
                  <li role="presentation"><a href="#activity-logo" aria-controls="activity-logo" role="tab" data-toggle="tab">Activity Logo</a></li>
              </ul>
<?php include('messages.php');?>
              <!-- Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="about">
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Name</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo $data->memberName; ?></h4>
                      </div>
                    </div>   
                     <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>CI</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo $data->clubId; ?></h4>
                      </div>
                    </div>   
                     <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Mobile No</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo $data->memberMobile; ?></h4>
                      </div>
                    </div>    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Date Of Birth</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo date('M-d, Y',strtotime($data->Birthday)); ?></h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Address</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b><?php echo $data->memberAddress; ?></h4>
                      </div>
                    </div>

                    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Edit Passward
                    </button>
                    <?php echo form_open('home/updateUserPassword')?>
                      <div class="collapse" id="collapseExample">
                          <div class="form-group">
                            <label for="currentpassword">Current Password</label>
                            <input type="password" class="form-control" id="" name="memberPassword">
                          </div>
                          <div class="form-group">
                            <label for="newpassword">New Password</label>
                            <input type="password" class="form-control" id="" name="newPassword">
                          </div>
                          <div class="form-group">
                            <label for="confirmpassword">Confirm Password</label>
                            <input type="password" class="form-control" id="" name="confirmPassword">
                          </div>
                          <input type="hidden" class="form-control" id="" name="memberId" value="<?php echo $data->memberId; ?>">
                          <button class="btn btn-default" type="submit">Submit</button>
                      </div>
                  </div>
                    <?php echo form_close() ?>
                  <div role="tabpanel" class="tab-pane" id="balance">
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Total Balance</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo $data->memberTotalBalance; ?> TK</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Due</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b>:</b> <?php echo $data->memberDueAmount; ?> TK</h4>
                      </div>
                    </div>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="activity-logo">
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Events</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b style="margin-right: 5px;">:</b> Meeting</h4>
                        <h4 style="margin-left: 15px;">Sub Commette </h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2 post">
                        <h4><b>Events</b></h4>
                      </div>
                      <div class="col-md-10 post-name">
                        <h4><b style="margin-right: 5px;">:</b> Meeting</h4>
                        <h4 style="margin-left: 15px;">Sub Commette </h4>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- <section id="copywrite-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cf-left">
                        <i class="fa fa-copyright" aria-hidden="true"></i>
                        <h4><strong>rotaryclub</strong> 2016</h4>
                    </div>
                    <div class="cf-right">
                        <h4>Design by StarLab IT</h4>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/front-end/js/jquery.min.js'); ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/front-end/js/bootstrap.min.js'); ?>"></script>
    <!-- bootsnav -->
    <script src="<?php echo base_url('assets/front-end/js/bootsnav.js'); ?>"></script>
    <!-- slider -->
    <script src="<?php echo base_url('assets/front-end/js/vegas.min.js'); ?>"></script>
    <!-- owl carousle -->
    <script src="<?php echo base_url('assets/front-end/js/owl.carousel.min.js'); ?>"></script>
    <!-- custom js -->
    <script src="<?php echo base_url('assets/front-end/js/navbar-pic.js'); ?>"></script>
    <script src="<?php echo base_url('assets/front-end/js/userProfile.js'); ?>"></script>
</body>

</html>