<?php include('header.php');?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header text-center">Member Profile</h1>
    </div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php //echo form_open_multipart('admin/insertMember', 'class="addMember-form"')?>
        	<?php echo form_open_multipart('admin/updateProfile') ?>
        		
				<div class="row">
					<div class="col-md-3 col-md-offset-5">
						<div class="form-group">					
							<div class="profile-image">
						      <img src="<?php echo base_url($value->memberImage); ?>" alt="..." class="img-responsive">
						    </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="profile-table">
						<table class="table">
							<tr>
								<th>Name</th>
								<td>: <?php echo $value->memberName; ?></td>
							</tr>
							<tr>
								<th>Hpc</th>
								<td>: <?php echo $value->Position; ?></td>
							</tr>
							<tr>
								<th>Blood Group</th>				
								<td>: <?php echo $value->BloodGroup; ?></td>
							</tr>				
							<tr>
								<th>Birth Date</th>		
								<td>: <?php echo $value->Birthday; ?></td>
							</tr>			
							<tr>
								<th>Gender</th>		
								<td>: <?php echo $value->memberGender; ?></td>	
							</tr>
							<tr>			
								<th>User Type</th>		
								<td>: <?php echo $value->memberLoginTypeTitle; ?></td>	
							</tr>	
							<tr>
								<th>Member Position Type</th>		
								<td>: <?php echo $value->memberTypeTitle; ?></td>
							</tr>		
							<tr>
								<th>Phone</th>		
								<td>: <?php echo $value->memberMobile; ?></td>
							</tr>		
							<tr>
								<th>Email</th>
								<td>: <?php echo $value->memberEmail; ?></td>
							</tr>
								
							<tr>
								<th>Address</th>		
								<td><?php echo ': '.$value->memberAddress; ?></td>
							</tr>
						</table>
					</div>
				</div>
        		<div class="row  hidden hiddenButton">
        			<div class="col-md-4">
						<div class="form-group">
						    <label for="memberTotalBalance">Old Password</label>
						    <?php echo form_password(['name'=>'memberPassword', 'class'=>'form-control', 'value'=>set_value('memberPassword')]);?>
					    	<div class="errorClass"><?php echo form_error('memberPassword'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberDueAmount">New Password</label>
						    <?php echo form_password(['name'=>'newPassword', 'class'=>'form-control', 'value'=>set_value('newPassword')]);?>
					    	<div class="errorClass"><?php echo form_error('newPassword'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberDueAmount">Confirm Password</label>
						    <?php echo form_password(['name'=>'confirmPass', 'class'=>'form-control', 'value'=>set_value('confirmPass')]);?>
					    	<div class="errorClass"><?php echo form_error('confirmPass'); ?></div>
						</div>
					</div>
        		</div>
        		<?php echo form_input(['name'=>'memberId','type'=>'hidden', 'class'=>'form-control',  'value'=>set_value('memberId').$value->memberId]);?>
	        	<div class="row">
	        		<div class="col-md-12 add-buttons">
	        			<?php// echo form_submit('', 'Add' , 'class="btn btn-light add"'); ?>
	        			<a type="button" class="btn btn-light edit" id="showPass"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a>
	        			<button type="submit" class="btn btn-light add hidden hiddenButton"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
	        			<button type="reset" class="btn btn-light reset hidden hiddenButton"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
	        			<a class="btn btn-light back hidden hiddenButton" type="button" role="button" id="hidePass"><i class="fa fa-undo" aria-hidden="true"></i>Cancel</a>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
