<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add Member</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Member</a></li>
		  <li class="breadcrumb-item active">Add Member</li>
		</ol>
	</div>
</div>

<?php //include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php //echo form_open_multipart('admin/insertMember', 'class="addMember-form"')?>
        	<?php echo form_open_multipart('admin/insertMember') ?>
        	
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberName">Name</label>
						    <?php echo form_input(['name'=>'memberName', 'class'=>'form-control', 'value'=>set_value('userName')]);?>
					    	<div class="errorClass"><?php echo form_error('memberName'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="clubId">CL</label>
						    <?php echo form_input(['name'=>'clubId', 'class'=>'form-control', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('clubId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberNumber">Member Number</label>
						    <?php echo form_input(['name'=>'memberNumber', 'class'=>'form-control', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('memberNumber'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberJoinDate">Indate</label>
						    <?php echo form_input(['name'=>'memberJoinDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('memberJoinDate'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="Position">Hpc</label>
						    <?php echo form_input(['name'=>'Position', 'class'=>'form-control', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('Position'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="BloodGroup">Blood Group</label>
						    <select name="BloodGroup" class="form-control" required >		
								<option value="" selected="true" disabled="disabled">Blood Group</option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>	
								<option value="B+">B+</option>	
								<option value="B-">B-</option>	
								<option value="O+">O+</option>	
								<option value="O-">O-</option>	
								<option value="AB+">AB+</option>	
								<option value="AB-">AB-</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('BloodGroup'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="Birthday">Birth Date</label>
						    <?php echo form_input(['name'=>'Birthday', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('Birthday'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberTypeId">Member Type</label>
						    <select name="memberTypeId" class="form-control" required >					
								<option value="5">Member</option>
								<option value="1">MPHF</option>	
								<option value="2">MPHF MD</option>	
								<option value="3">PHF</option>	
								<option value="4">RFSM</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberTypeId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberGender">Member Gender</label>
						    <select name="memberGender" class="form-control">					
								<option value="Male">Male</option>	
								<option value="Female">Female</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberGender'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberLoginTypeId">User Type</label>
						    <select name="memberLoginTypeId" class="form-control">					
								<option value="1">Admin</option>	
								<option value="2">User</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberLoginTypeId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberPositionType">Member Position Type</label>
						    <select name="memberPositionType" class="form-control">					
								<option value="1">President</option>	
								<option value="2">Secretary</option>	
								<option value="3">President Elect</option>	
								<option value="4">Other</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberLoginTypeId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberStatus">Member Status</label>
						    <select name="memberStatus" class="form-control">				
								<option value="1">Active</option>	
								<option value="2">Inactive</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberStatus'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberMobile">Phone</label>
						    <?php echo form_input(['name'=>'memberMobile', 'class'=>'form-control', 'type'=>'number', 'value'=>set_value('memberMobile')]);?>
					    	<div class="errorClass"><?php echo form_error('memberMobile'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberEmail">Email</label>
						    <?php echo form_input(['name'=>'memberEmail', 'class'=>'form-control', 'value'=>set_value('memberEmail')]);?>
					    	<div class="errorClass"><?php echo form_error('memberEmail'); ?></div>
						</div>
					</div>
					<!--<div class="col-md-4">
						<div class="form-group" >
							<div class="btn">
								<span>Choose file</span>
								<?php //echo form_upload(['name'=>'image', 'type'=>'file', 'value'=>set_value('image')]);
									 // echo form_error('image');
								?>
							</div>
						</div>
					</div>-->
					<div class="col-md-4">
						<div class="form-group" >
							<label>Member Image</label>
							<?php echo form_upload(['name'=>'memberImage', 'class'=>'form-control', 'type'=>'file', 'required'=>'required', 'value'=>set_value('memberImage')]);
								  echo form_error('memberImage');
							?>	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberTotalBalance">Password</label>
						    <?php echo form_password(['name'=>'memberPassword', 'class'=>'form-control', 'value'=>set_value('')]);?>
					    	<div class="errorClass"><?php echo form_error('memberPassword'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberDueAmount">Confirm Password</label>
						    <?php echo form_password(['name'=>'confirmPass', 'class'=>'form-control', 'value'=>set_value('')]);?>
					    	<div class="errorClass"><?php echo form_error('confirmPass'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="memberAddress">Address</label>
						    <?php echo form_textarea(['name'=>'memberAddress', 'class'=>'form-control', 'rows'=>'3', 'value'=>set_value('memberAddress')]);?>
					    	<div class="errorClass"><?php echo form_error('memberAddress'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberTotalBalance">Last Balance</label>
						    <?php echo form_input(['name'=>'memberTotalBalance', 'class'=>'form-control', 'value'=>set_value('memberTotalBalance')]);?>
					    	<div class="errorClass"><?php echo form_error('memberTotalBalance'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberDueAmount">Last Due</label>
						    <?php echo form_input(['name'=>'memberDueAmount', 'class'=>'form-control', 'value'=>set_value('memberDueAmount')]);?>
					    	<div class="errorClass"><?php echo form_error('memberDueAmount'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="WeedingDate">Wedding Date</label>
						    <?php echo form_input(['name'=>'WeedingDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('WeedingDate')]);?>
					    	<div class="errorClass"><?php echo form_error('WeedingDate'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseName">Spouse Name</label>
						    <?php echo form_input(['name'=>'SpouseName', 'class'=>'form-control', 'value'=>set_value('SpouseName')]);?>
					    	<div class="errorClass"><?php echo form_error('SpouseName'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseBirthday">Spouse Birth Date</label>
						    <?php echo form_input(['name'=>'SpouseBirthday', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('SpouseBirthday')]);?>
					    	<div class="errorClass"><?php echo form_error('SpouseBirthday'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseBloodGroup">Spouse Blood Group</label>
						    <select name="SpouseBloodGroup" class="form-control">		
								<option value="" selected="true" disabled="disabled">Blood Group</option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>	
								<option value="B+">B+</option>	
								<option value="B-">B-</option>	
								<option value="O+">O+</option>	
								<option value="O-">O-</option>	
								<option value="AB+">AB+</option>	
								<option value="AB-">AB-</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('SpouseBloodGroup'); ?></div>
						</div>
					</div>
				</div>
        	
        	<div class="row">
        		<div class="col-md-12 add-buttons">
        			<?php// echo form_submit('', 'Add' , 'class="btn btn-light add"'); ?>
        			<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>
        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
        		</div>
        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
