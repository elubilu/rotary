<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">All News</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">News</a></li>
		  <li class="breadcrumb-item active">All News</li>
		</ol>
	</div>
</div>


<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
		<div class="jumborton">
			<table class="table table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>#</th>
						<th>NEWS TITLE</th>	
						<th>NEWS DETAILS</th>	
						<th>NEWS DATE</th>			
						<th>OPTIONS</th>		
					</tr>
				</thead>
				
				<tbody>
					<?php  foreach ($data as $value) {
						
					?>
					<tr>
						<td><?php  ?></td>
						<td><?php echo $value->newsTitle; ?></td>
						<td><?php $this->load->helper('text');
                            $value->newsDetails=word_limiter($value->newsDetails, 30);
                            echo $value->newsDetails; ?></td> 
						<td><?php echo $value->newsDate; ?></td>
						<td class="option">
							<a href="<?php echo base_url("admin/newsDetails/{$value->newsInfoId}"); ?>" title="Details">
								<span class="fa-stack fa-lg">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-info fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</td> 
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php include('footer.php');?>
