$(function(){

    $(".slide-btn button").click(function(){
        $(".slideshow").removeClass("hidden");
        $(".slideshow").addClass("show");
        $(".owl-carousel").addClass("hidden");
        $(".owl-carousel").removeClass("show");
        $(".thumb-btn button").addClass("show");
        $(".thumb-btn button").removeClass("hidden");
        $(".slide-btn button").addClass("hidden");
        $(".slide-btn button").removeClass("show");
    });

    $(".thumb-btn button").click(function(){
        $(".slideshow").removeClass("show");
        $(".slideshow").addClass("hidden");
        $(".owl-carousel").addClass("show");
        $(".owl-carousel").removeClass("hidden");
        $(".thumb-btn button").removeClass("show");
        $(".thumb-btn button").addClass("hidden");
        $(".slide-btn button").addClass("show");
        $(".slide-btn button").removeClass("hidden");
    });

    // picture
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    // pic slider
    $("#photo-slider").vegas({
        slides: [
            {
                src: "../assets/front-end/images/slider/1.jpg"
            },
            {
                src: "../assets/front-end/images/slider/4.jpg"
            },
            {
                src: "../assets/front-end/images/slider/5.jpg"
            }
        ]
    });


})

