$(function() {
	
	/*slider*/
    $("#fst-slider").vegas({
        slides: [
        	{
                src: "../assets/front-end/images/slider/8.jpg"
            },
            {
                src: "../assets/front-end/images/slider/4.jpg"
            },
            {
                src: "../assets/front-end/images/slider/5.jpg"
            }
        ]
    });

    /*height*/
    var windowHeight = $(window).height();
    var headerHeight = $('#header').height();
    var navHeight = $('nav').height();
    var sliderheight = windowHeight - headerHeight - navHeight;
    //$('#after-nav').css("height", sliderheight);
    $('#fst-slider').css("height", sliderheight);

    //$('.national-anthem').css("height", sliderheight);

    // owl carousel
    $('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    //nav:false,
		    dots:true,
		    responsiveClass:true,
		    autoplay:true,
		    autoplayTimeout:7000,
		    autoplayHoverPause:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:1,
		            //nav:false,
		            dots: true,
		        },
		        1000:{
		            items:1,
		            nav:true,
		            dots: true,
		            loop:true
		        }
		    }
		});

})