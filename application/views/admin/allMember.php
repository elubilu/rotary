<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">All Member</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Member</a></li>
		  <li class="breadcrumb-item active">All Member</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
		<div class="jumborton">
			<table class="table table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>#</th>
						<th>NAME</th>
						<th>CI</th>				
						<th>Hpc</th>		
						<th>PHONE</th>		
						<th>EMAIL</th>		
						<th>DETAILS</th>		
					</tr>
				</thead>
				
				<tbody>
					<?php foreach($data as $value){ ?>
					<tr>
						<td></td>
						<td><?php echo $value->memberName ?></td>
						<td><?php echo $value->clubId ?></td> 
						<td><?php echo $value->Position ?></td>
						<td><?php echo $value->memberMobile ?></td>
						<td><?php echo $value->memberEmail ?></td>
						<td class="option">
							<a href="<?php echo base_url("admin/memberDetails/{$value->memberId}"); ?>" title="Details">
								<span class="fa-stack fa-lg">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-info fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</td> 
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php include('footer.php');?>
