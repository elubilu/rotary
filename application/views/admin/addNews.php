<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add News</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item active">Add News</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open_multipart('admin/insertNews', 'class="addNews-form"')?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						    <label for="newsTitle">News Title</label>
						    <?php echo form_input(['name'=>'newsTitle', 'class'=>'form-control','required'=>'required', 'value'=>set_value('newsTitle')]);?>
					    	<div class="errorClass"><?php echo form_error('newsTitle'); ?></div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						    <label for="newsDetails">News Details</label>
						    <?php echo form_textarea(['name'=>'newsDetails', 'class'=>'form-control', 'rows'=>'3', 'required'=>'required', 'value'=>set_value('newsDetails')]);?>
					    	<div class="errorClass"><?php echo form_error('newsDetails'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group" >
							<label>Main Image</label>
							<?php echo form_upload(['name'=>'newsImage', 'class'=>'form-control', 'type'=>'file',  'required'=>'required','value'=>set_value('newsImage')]);
								  echo form_error('newsImage');
							?>		
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group" >
							<label>Other Images</label>
							<?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
									  echo form_error('news_pic');
								?>		
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group">
						    <label for="newsDate">Date</label>
						    <?php echo form_input(['name'=>'newsDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('newsDate')]);?>
					    	<div class="errorClass"><?php echo form_error('newsDate'); ?></div>
						</div>
					</div>
				</div>
        	
	        	<div class="row">
	        		<div class="col-md-12 add-buttons">
	        			<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>
	        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
