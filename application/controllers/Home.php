<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('home_model');
	}
	
	public function index(){
	    $data=$this->home_model->get_all_events();	
	    $value=$this->home_model->get_all_news();
	    //print_r($value);exit();
	   $check=$this->session->userdata();
	    //print_r($value); exit();

		$this->load->view('home/index',['data'=>$data,'valu'=>$value,'check'=>$check]);
	}
	public function memberList(){
		$data=$this->home_model->get_all_members();
		 $check=$this->session->userdata();
		$this->load->view('home/memberList',['data'=>$data,'check'=>$check]);
	}
	public function board(){	
		 $check=$this->session->userdata();
		$this->load->view('home/board',['check'=>$check]);
	} 
	public function subCom(){
		 $check=$this->session->userdata();
		$data=$this->home_model->get_all_sub_com_members();
		//print_r($data); exit();
		$this->load->view('home/subCom',['data'=>$data,'check'=>$check]);
	}
	public function clubHistory(){	
		 $check=$this->session->userdata();
		 $this->load->view('home/clubHistory',['check'=>$check]);
	}
	public function clubPresident(){	
		 	$check=$this->session->userdata();
			$this->load->view('home/clubPresident',['check'=>$check]);
	}
	public function contactUs(){	
			$check=$this->session->userdata();
			$this->load->view('home/contactUs',['check'=>$check]);
	}
	public function director(){	
			$check=$this->session->userdata();
			$this->load->view('home/director',['check'=>$check]);
	}
	public function iCom(){	
			$info=$this->home_model->get_all_ins_committee();
			//print_r($info); exit();
			$check=$this->session->userdata();
			$data=$this->home_model->get_all_ins_com_members($info);
			//print_r($data); exit();
			$this->load->view('home/iCom',['data'=>$data]);
	}

	public function login(){	
			$this->load->view('home/login');
	}
	public function news($id){
	        $image=$this->home_model->get_all_images_by_id($id);
	        $info=$this->home_model->get_news_by_id($id);
	        $check=$this->session->userdata();
			$this->load->view('home/news',['image'=>$image,'info'=>$info,'check'=>$check]);
	}
	public function ourLeader(){	
		 	 $check=$this->session->userdata();
			$this->load->view('home/ourLeader',['check'=>$check]);
	}
	public function pp(){	
		 $check=$this->session->userdata();
			$this->load->view('home/pp',['check'=>$check]);
	}
	public function photos(){	
			 $check=$this->session->userdata();
			$this->load->view('home/photos',['check'=>$check]);
	}
	public function pastLeaders(){	
			 $check=$this->session->userdata();
			$this->load->view('home/pastLeaders',['check'=>$check]);
	}
	public function allNews(){	
		 	$check=$this->session->userdata();
			$data=$this->home_model->get_all_news();

			$this->load->view('home/allNews',['data'=>$data,'check'=>$check]);
	}
	public function userProfile(){	
		if($this->session->userdata('member_id')){
			$id=$this->session->userdata('member_id');
			$data=$this->home_model->get_member_details_by_id($id);
			$infos=$this->home_model->get_notification_details_by_id($id);
			//$infos['totalNotification']=$infos->num_rows();
			//print_r($infos); exit();

			$this->load->view('home/userProfile',['data'=>$data,'infos'=>$infos]);
		}
		else{
			return redirect('home/index');
		}
	}


	public function pmo(){	
			$check=$this->session->userdata();
			$this->load->view('home/pmo',['check'=>$check]);
	}
	public function pmt(){	
		 $check=$this->session->userdata();
			$this->load->view('home/pmt',['check'=>$check]);
	}
	public function pmth(){	
			 $check=$this->session->userdata();
			$this->load->view('home/pmth',['check'=>$check]);
	}
    public function updateUserPassword()
    {
    	$this->load->library('form_validation');
        if ($this->form_validation->run('adminPassword') == FALSE){
        	$this->session->set_flashdata('feedback_failed', 'Update Member Password Failed for Validation !');
            redirect('home/userProfile');
        }
        else{

	    	$data=$this->input->post();
	    	if($data){
	    		$Password=$data['memberPassword'];
	    	unset($data['confirmPassword']);
	    	unset($data['memberPassword']);

	    	$data['memberPassword']=$data['newPassword'];
	    	unset($data['newPassword']);
	    	$id=$this->session->userdata('member_id');
	    	//print_r($id); exit();
	    	$pass=$this->home_model->get_pass_by_id($id);
	    	//print_r($data['memberPassword']); echo "   " ; print_r($pass); exit();
	    	if($Password==$pass){
			    	if($this->home_model->update_user_password($data))
			    	{
			    		$id=$this->session->userdata('member_id');
						$data=$this->home_model->get_member_details_by_id($id);
						$this->session->set_flashdata('feedback_successfull', 'Update Member Password Successfully');
						$this->load->view('home/userProfile',['data'=>$data ]);
						
			    	}
			    	else 
			    	{
			    		$this->session->set_flashdata('feedback_failed', 'Update Member Password Failed!');
			    	 	redirect('home/userProfile');
			    	}
			    		
			    		
			    }
			  else 
		    	{
		    		$this->session->set_flashdata('feedback_failed', ' Member Password Wrong!');
		    		redirect('home/userProfile');
		    		
		    	}  
			}
	   

	    }

    }

}

