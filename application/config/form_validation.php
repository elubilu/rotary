    <?php 
    $config = array(
           'login_form' => array(
                array(
    			   'field' => 'email',
                   'label' => 'User Email',
                   'rules' => 'required|valid_email' 
                ),
                array(
                   'field' => 'password',
                   'label' => 'Password',
                   'rules' => 'required|min_length[8]|md5', 
                )
                   
    		), 
          'signup_form' => array(
                array(
                   'field' => 'memberName',
                   'label' => 'Name',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'clubId',
                   'label' => 'CI',
                   //'rules' => 'required|is_unique[member_info.clubId]', 
                   'rules' => 'required', 
                ), 
                array(
                   'field' => 'memberNumber',
                   'label' => 'Member Number',
                   'rules' => 'required|is_unique[member_info.memberNumber]', 
                ), array(
                   'field' => 'memberJoinDate',
                   'label' => 'InDate',
                   'rules' => '', 
                ), array(
                   'field' => 'Position',
                   'label' => 'Hpc',
                   'rules' => 'required', 
                ), array(
                   'field' => 'BloodGroup',
                   'label' => 'Blood Group',
                   'rules' => 'required', 
                ), array(
                   'field' => 'Birthday',
                   'label' => 'Birth Date',
                   'rules' => 'required', 
                ), array(
                   'field' => 'memberTypeId',
                   'label' => 'Member Type',
                   'rules' => 'required', 
                ), array(
                   'field' => 'memberGender',
                   'label' => 'Member Gender',
                   'rules' => 'required', 
                ),array(
                   'field' => 'memberLoginTypeId',
                   'label' => 'User Type',
                   'rules' => 'required', 
                ),array(
                   'field' => 'memberMobile',
                   'label' => 'Phone',
                   'rules' => 'required|regex_match[/^[0-9]{11}$/]', 
                ),array(
                   'field' => 'memberEmail',
                   'label' => 'Email',
                   'rules' => 'required|valid_email|is_unique[member_info.memberEmail]', 
                ),array(
                   'field' => 'memberAddress',
                   'label' => 'Address',
                   'rules' => 'required', 
                ),array(
                   'field' => 'memberTotalBalance',
                   'label' => 'Last Balance',
                   'rules' => 'required|numeric', 
                ),array(
                   'field' => 'memberDueAmount',
                   'label' => 'Last Due',
                   'rules' => 'required|numeric', 
                ),array(
                   'field' => 'WeedingDate',
                   'label' => 'Weeding Date',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseName',
                   'label' => 'Spouse Name',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseBirthday',
                   'label' => 'Spouse Birth Date',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseBloodGroup',
                   'label' => 'Spouse Blood Group',
                   'rules' => '', 
                ),array(
                   'field' => 'memberPassword',
                   'label' => 'Member Password',
                   'rules' => 'required|min_length[8]|md5', 
                ),array(
                   'field' => 'confirmPass',
                   'label' => 'Confirm Password',
                   'rules' => 'required|md5|matches[memberPassword]', 
                ),
                   
        ), 
       'update_member' => array(
                array(
                   'field' => 'memberName',
                   'label' => 'Name',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'clubId',
                   'label' => 'CL',
                   'rules' => 'required', 
                ), 
                array(
                   'field' => 'memberNumber',
                   'label' => 'Member Number',
                   'rules' => 'required', 
                ), array(
                   'field' => 'memberJoinDate',
                   'label' => 'InDate',
                   'rules' => '', 
                ), array(
                   'field' => 'Position',
                   'label' => 'Hpc',
                   'rules' => 'required', 
                ), array(
                   'field' => 'BloodGroup',
                   'label' => 'Blood Group',
                   'rules' => '', 
                ), array(
                   'field' => 'Birthday',
                   'label' => 'Birth Date',
                   'rules' => 'required', 
                ), array(
                   'field' => 'memberTypeId',
                   'label' => 'Member Type',
                   'rules' => '', 
                ), array(
                   'field' => 'memberGender',
                   'label' => 'Member Gender',
                   'rules' => '', 
                ),array(
                   'field' => 'memberLoginTypeId',
                   'label' => 'User Type',
                   'rules' => '', 
                ),array(
                   'field' => 'memberMobile',
                   'label' => 'Phone',
                   'rules' => 'required', 
                ),array(
                   'field' => 'memberEmail',
                   'label' => 'Email',
                   'rules' => 'required|valid_email', 
                ),array(
                   'field' => 'memberAddress',
                   'label' => 'Address',
                   'rules' => 'required', 
                ),array(
                   'field' => 'memberTotalBalance',
                   'label' => 'Balance',
                   'rules' => 'required|numeric', 
                ),array(
                   'field' => 'memberDueAmount',
                   'label' => 'Last Due',
                   'rules' => 'required|numeric', 
                ),array(
                   'field' => 'WeedingDate',
                   'label' => 'Weeding Date',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseName',
                   'label' => 'Spouse Name',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseBirthday',
                   'label' => 'Spouse Birth Date',
                   'rules' => '', 
                ),array(
                   'field' => 'SpouseBloodGroup',
                   'label' => 'Spouse Blood Group',
                   'rules' => '', 
                ),
                   
        ), 
    'insertEvents' => array(
                array(
                   'field' => 'event_type_eventId',
                   'label' => 'Event Type',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'eventDate',
                   'label' => 'Event Date',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'eventTitle',
                   'label' => 'Event Title',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'eventDetails',
                   'label' => 'Event Details',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'eventPlace',
                   'label' => 'Event Place',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'eventPerHeadAmount',
                   'label' => 'Event Cost (Per Head)',
                   'rules' => 'required|numeric', 
                ),
                   
        ),  'updateEvents' => array(
                array(
                   'field' => 'event_type_eventId',
                   'label' => 'Event Type',
                   'rules' => '' 
                ),
                array(
                   'field' => 'eventDate',
                   'label' => 'Event Date',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'eventTitle',
                   'label' => 'Event Title',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'eventDetails',
                   'label' => 'Event Details',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'eventPlace',
                   'label' => 'Event Place',
                   'rules' => 'required' 
                ),
                array(
                   'field' => 'eventPerHeadAmount',
                   'label' => 'Event Cost (Per Head)',
                   'rules' => 'required|numeric', 
                ),
                   
        ),     
    'EventsPayment' => array(
                array(
                   'field' => 'member_info_memberId',
                   'label' => 'Member Name',
                   'rules' => 'required',
                ),
                array(
                   'field' => 'event_info_eventId',
                   'label' => 'Event Name',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'eventPaidAmount',
                   'label' => 'Amount',
                   'rules' => 'required|numeric' 
                ),
                array(
                   'field' => 'paymentPaidType',
                   'label' => 'Payment Type',
                   'rules' => 'required', 
                ),  
                array(
                   'field' => 'paymentDate',
                   'label' => 'Date',
                   'rules' => 'required', 
                ),
                   
        ), 
      'createCommittee' => array(
                array(
                   'field' => 'committee_member_title',
                   'label' => 'Committee Title',
                   'rules' => 'required',
                ),
                array(
                   'field' => 'committee_member_type',
                   'label' => 'Select Committee Member',
                   'rules' => 'required', 
                ),  array(
                   'field' => 'committee_member_responsibility',
                   'label' => 'Responsibility',
                   'rules' => 'required' 
                ),
               
        ),
       'attendanceSheet' => array(
                array(
                   'field' => 'event_info_eventId',
                   'label' => 'Meeting Title',
                   'rules' => 'required',
                ),
                array(
                   'field' => 'meetingDate',
                   'label' => 'Meeting Date',
                   'rules' => 'required', 
                ),  
        ),  
       'insertNews' => array(
                array(
                   'field' => 'newsTitle',
                   'label' => 'Title',
                   'rules' => 'max_length[650]|min_length[5]',
                ),
        ),  
       'addSubCommittee' => array(
                array(
                   'field' => 'subCommitteeTitle',
                   'label' => 'Sub Committee Title',
                   'rules' => 'required',
                ), 
                array(
                   'field' => 'subCommitteeYear',
                   'label' => 'Year',
                   'rules' => 'required',
                ),
        ),
      'addInstalationCommittee' => array(
              array(
                 'field' => 'instalationCommitteeTitle',
                 'label' => 'Instalation Committee Title',
                 'rules' => 'required',
              ), 
              array(
                 'field' => 'instalationCommitteeYear',
                 'label' => 'Year',
                 'rules' => 'required',
              ),
              array(
                 'field' => 'event_info_instalationCommitteeEventId',
                 'label' => 'Event',
                 'rules' => 'required',
              ),
        ),  
      
         'changePassword' => array(
              array(
                 'field' => 'memberPassword',
                 'label' => 'Old Password',
                 'rules' => 'required|md5',
              ), 
              array(
                 'field' => 'newPassword',
                 'label' => 'New Password',
                 'rules' => 'required|md5|min_length[8]',
              ),
              array(
                 'field' => 'confirmPass',
                 'label' => 'Confirm Password',
                 'rules' => 'required|md5|matches[newPassword]',
              ),
        ), 
        'adminPassword' => array(
                    array(
                       'field' => 'memberPassword',
                       'label' => 'Old Password',
                       'rules' => 'md5',
                    ), 
                    array(
                       'field' => 'newPassword',
                       'label' => 'New Password',
                       'rules' => 'md5|min_length[8]',
                    ),
                    array(
                       'field' => 'confirmPassword',
                       'label' => 'Confirm Password',
                       'rules' => 'md5|matches[newPassword]',
                    ),
              ), 

          
    	);
    	
    ?>	
    	