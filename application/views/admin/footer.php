</div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
  
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/js/metisMenu.min.js'); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('assets/js/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/morris-data.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/js/sb-admin-2.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/datepicker.en.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-timepicker.min.js'); ?>"></script>
    <script type="text/javascript">
        $('#timepicker1').timepicker();
    </script>
    <script src="<?php echo base_url('assets/js/script.js'); ?>"></script>

</body>

</html>
