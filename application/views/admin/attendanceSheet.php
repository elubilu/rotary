 <?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Attendance Sheet</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item active">Attendance Sheet</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/insertMeetingInfo', 'class="addMember-form"')?>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						    <label for="event_info_eventId">Meeting Title</label>
						    <select name="event_info_eventId" class="form-control" value="">
						    <?php foreach ($data1 as $info) {
						    	
						    ?>					
								<option value="<?php echo $info->event_infoId?>"><?php echo $info->eventTitle; ?></option>	
							<?php } ?>
							</select>
					    	<div class="errorClass"><?php echo form_error('event_info_eventId'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="meetingDate">Meeting Date</label>
						    <?php echo form_input(['name'=>'meetingDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('meetingDate'); ?></div>
						</div>
					</div>					
				</div>
        	
        	<div class="attendenceTable">
        		<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>Select</th>
				      <th>Name</th>
				      <th>ID</th>
				      <th>Position</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($data as $value){ ?>
				    <tr>
				      <td>
						<div class="form-check">
						  <label class="form-check-label">
						    <input class="form-check-input" name="members[]" type="checkbox" id="blankCheckbox" value="<?php echo $value->memberId ?>" aria-label="...">
						  </label>
						</div>
				      </td>
				      <td><?php echo $value->memberName ?></td>
				      <td><?php echo $value->clubId ?></td>
				      <td><?php echo $value->Position ?></td>
				    </tr>
				    <?php } ?>
				    
				  </tbody>
				</table>
        	</div>
        	<div class="row">
        		<div class="col-md-12 add-buttons">
        			<?php// echo form_submit('', 'Add' , 'class="btn btn-light add"'); ?>
        			<!--<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>-->
        			<button type="submit" class="btn save"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
        		</div>
        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
