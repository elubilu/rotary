$(function(){
	// owl carousel
    $('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    center: true,
	    //nav:false,
	    dots:true,
	    responsiveClass:true,
	    autoplay:true,
	    autoplayTimeout:7000,
	    autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1,
	            nav:true
	        },
	        600:{
	            items:2,
	            //nav:false,
	            dots: true,
	        },
	        1000:{
	            items:2,
	            nav:true,
	            dots: true,
	            loop:true
	        }
	    }
	});
})