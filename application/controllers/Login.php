<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('login_model');
		
	}

	public function index()
	{
		
		$this->load->view('login/login_view');
	}
	
	
	public function logout(){
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('user_role');
		return redirect('login');
	}
	public function memberLogout(){
		$this->session->unset_userdata('member_id');
		return redirect('home/index');
	}
	
	public function user_login()
	{
		$this->load->library('form_validation');
		if($this->form_validation->run('login_form')){
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$data=$this->login_model->login_value( $email,$password );
			//print_r($data);exit;
			if($data){

				$this->session->set_userdata('admin_id',$data['memberId']);
				$this->session->set_userdata('user_role',$data['memberLoginTypeId']);
				if($data['memberLoginTypeId']==1){
				//print_r($data);exit;
					$link = array(
					'status'=>true, 
					'redirect'=>base_url('Admin/dashboard')
					);
				}
				else{
					$link = array(
					'status'=>true, 
					'redirect'=>base_url('Home/index')
					);
				}
				echo json_encode($link);

			}
			else{
				
				$data = array(
				'errors' => 'invalid user name and password',
				'status'=>false
				);
				echo json_encode($data);
					
			}
			
		}
		else{
				$data = array(
					'password' => form_error('password'),
					'email' => form_error('email'),
					'status'=>false
				);
				echo json_encode($data);
		}

	}
	public function member_login()
	{
		$this->load->library('form_validation');
		if($this->form_validation->run('login_form')){
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$data=$this->login_model->login_value( $email,$password );
			//print_r($data);exit;
			if($data){

				$this->session->set_userdata('member_id',$data['memberId']);
					$link = array(
					'status'=>true, 
					'redirect'=>base_url('Home/userProfile')
					);
				echo json_encode($link);

			}
			else{
				
				$data = array(
				'errors' => 'invalid user name and password',
				'status'=>false
				);
				echo json_encode($data);
					
			}
			
		}
		else{
				$data = array(
					'password' => form_error('password'),
					'email' => form_error('email'),
					'status'=>false
				);
				echo json_encode($data);
		}

	}
}



