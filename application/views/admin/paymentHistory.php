<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">All Members Payment History</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Event</a></li>
		  <li class="breadcrumb-item active">Payment Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');  $sum=0;?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<div class="attendenceTable">
        		<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>Name</th>
				      <th>Payment Status</th>
				      <th>Amount</th>
				      <th>Date</th>
				      <th>Event Name</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($data as $value){ ?>
				    <tr>
				      <td><?php echo $value->memberName ?></td>
				      <td><?php if($value->paymentPaidType == 1) {echo 'Done';} else if($value->paymentPaidType == 2) {echo 'Half Payment';} else {echo "Pending";}; ?></td>
				      <td><?php if($value->paymentPaidType == 1 || $value->paymentPaidType == 2) $sum+=$value->eventPaidAmount; echo $value->eventPaidAmount;
				       ?></td>
				      <td><?php echo date('M-d, Y  h:i a',strtotime($value->paymentDate)); ?></td>
				      <td><?php echo $value->eventTitle ?></td>
				    </tr>
				    <?php } ?>
				  </tbody>
				</table>
        	</div>
        	<h2 class="text-center">Total Amount: <?php echo $sum; ?></h2>
        	<!--<div class="row">
        		<div class="col-md-12">
        			<a class="btn btn-light back" href="<?php echo base_url("admin/allEvents"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>-->
        </div>
    </div>
</div>

<?php include('footer.php');?>
