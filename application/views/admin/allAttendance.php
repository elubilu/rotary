<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">All Events</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Events</a></li>
		  <li class="breadcrumb-item active">All Events</li>
		</ol>
	</div>
</div>


<?php include('messages.php');?>
<?php if($data){ ?>
<div class="row">
	<div class="col-md-12">
		<div class="jumborton">
			<table class="table table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>#</th>
						
						<th>Meeting DATE</th>	
						<th>Event TITLE</th>		
						<th>OPTIONS</th>		
					</tr>
				</thead>
				
				<tbody>
					<?php foreach($data as $value){ ?>
					<tr>
						<td></td>
						<td><?php echo date('M-d, Y  h:i a',strtotime($value->meetingDate)) ?></td> 
						<td><?php echo $value->eventTitle ?></td>
						<td class="option">
							<a href="<?php echo base_url("admin/attendanceDetails/{$value->attendanceInfoId}"); ?>" title="Details">
								<span class="fa-stack fa-lg">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-info fa-stack-1x fa-inverse"></i>
								</span>
							</a>
							<a href="<?php echo base_url(""); ?>" title="Add Member">
								<span class="fa-stack fa-lg">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
								</span>
							</a>
							<a href="<?php echo base_url(""); ?>" title="Payment Details">
								<span class="fa-stack fa-lg">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-arrow-right fa-stack-1x fa-inverse"></i>
								</span>
							</a>
						</td> 
					</tr>
				   <?php } ?>
						
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php } ?>
<?php include('footer.php');?>
