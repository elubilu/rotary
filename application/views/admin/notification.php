<?php include('header.php');?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Notification</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item active">Notification</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>
<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
			<div class="row">
				<div class="notification">

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#member" aria-controls="member" role="tab" data-toggle="tab">Member Birthday</a></li>
				    <li role="presentation"><a href="#wedding" aria-controls="wedding" role="tab" data-toggle="tab">Wedding Date</a></li>
				    <li role="presentation"><a href="#wife" aria-controls="wife" role="tab" data-toggle="tab">Wife's Birthday</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content" style="margin-top: 20px;">
				  	<?php foreach ($memberBirthDate as  $value) {
				    		# code...
				    	?>
				    <div role="tabpanel" class="tab-pane active" id="member">
				    	
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3 control-label">Name :</label>
										<div class="col-md-9">
										  <p class="form-control-static"><?php echo $value->memberName; ?></p>
										</div>
									</div>
								</div>
								<div class="row">
								  <div class="form-group">
									<label class="col-md-3 control-label">Birth Date :</label>
									<div class="col-md-9">
									  <p class="form-control-static"><?php echo $value->Birthday; ?></p>
									</div>
								  </div>
							    </div>
							</div>
						</div>
					</div>
					<?php} foreach ($memberWeedingDate as  $value1) {
						# code...
					 ?>
				    <div role="tabpanel" class="tab-pane" id="wedding">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3 control-label">Name :</label>
										<div class="col-md-9">
										  <p class="form-control-static"><?php echo $value1->memberName; ?></p>
										</div>
									</div>
								</div>
								<div class="row">
								  <div class="form-group">
									<label class="col-md-3 control-label">Wedding Date :</label>
									<div class="col-md-9">
									  <p class="form-control-static"><?php echo $value1->WeedingDate; ?></p>
									</div>
								  </div>
							    </div>
							</div>
						</div>
					</div>
					<?php } foreach ($spouseBirthDate as  $value2) {
						# code...
					 ?>
				    <div role="tabpanel" class="tab-pane" id="wife">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3 control-label">Name :</label>
										<div class="col-md-9">
										  <p class="form-control-static"><?php echo $value2->memberName; ?></p>
										</div>
									</div>
								</div>
								<div class="row">
								  <div class="form-group">
									<label class="col-md-3 control-label">Birth Date :</label>
									<div class="col-md-9">
									  <p class="form-control-static"><?php echo $value2->SpouseBirthday; ?></p>
									</div>
								  </div>
							    </div>
							</div>
						</div>
					</div>
					<?php } ?>
				  </div>

				</div>
			</div>
        </div>
    </div>
</div>

<?php include('footer.php');?>
