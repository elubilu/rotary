<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add Installation Committee Member</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Installation Committee</a></li>
		  <li class="breadcrumb-item active">Add Installation Committee Member</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	
				
	        	<div class="addMemberForEvent">
	        		<table class="table table-bordered">
					  <thead class="thead-inverse">
					    <tr>
					      <th>Name</th>
					      <th>ID</th>
					      <th>Position</th>
					      <th>Responsibility</th>
					      <th>Option</th>
					    </tr>
					  </thead>
					  <tbody>
					   <?php foreach($data as $value){ ?>
					   <?php $check=0; ?>
					    <tr>
					    	<?php if($value1){
					    		foreach ($value1 as  $info) {
					    		if($value->memberId==$info->member_info_installationCommitteeMemberId) 
					    			{
					     echo form_open('admin/updateMemberForInstallationCommittee', 'class="addMember-form"');
					    				$check=5;
					    				?>
					      <td><?php echo $value->memberName ?></td>
					      <td></td>
					      <td><input type="text" required name="installationCommitteeMemberPosition" value="<?php echo $info->installationCommitteeMemberPosition; ?>" style="width:100%;" /></td>
					      <input type="hidden" name="installation_committee_Info_installationCommitteeInfoId" style="width:100%;" value="<?php echo $id  ?>" />
					      <input type="hidden" name="member_info_installationCommitteeMemberId" style="width:100%;" value="<?php echo $value->memberId   ?>" />
					      <td><textarea required name="installationCommitteeMemberResponsibility" id="" cols="" rows="1" style="width:100%;"><?php echo $info->installationCommitteeMemberResponsibility; ?></textarea></td>
						  <td><button type="submit" class="btn btn-light committee-add"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
						  <a href="<?php echo base_url("admin/installationCommitteeDeleteMember/{$info->installationCommitteeDetailsId}/{$info->installation_committee_Info_installationCommitteeInfoId}"); ?>" >
											<span class="btn btn-danger">
											  <i class="fa fa-trash" aria-hidden="true"></i>
											</span>
										  </a></td>
						  <?php echo form_close();  ?>					    				
					    				<?php 
					    			}
					    	   } 
					    	}
					    	?>
					    	
					    <?php if($check==0) { 
					    	echo form_open('admin/storeMemberForInstallationCommittee', 'class="addMember-form"');
					    	?>
					      <td><?php echo $value->memberName ?></td>
					      <td></td>
					      <td><input type="text"  name="installationCommitteeMemberPosition" style="width:100%;" required /></td>
					      <input type="hidden" name="installation_committee_Info_installationCommitteeInfoId" style="width:100%;" value="<?php echo $id  ?>" />
					      <input type="hidden" name="member_info_installationCommitteeMemberId" style="width:100%;" value="<?php echo $value->memberId   ?>" />
					      <td><textarea  name="installationCommitteeMemberResponsibility" id="" cols="" rows="1" style="width:100%;" required></textarea></td>
					      <td> <button type="submit" class="btn btn-light committee-add"><i class="fa fa-plus-square-o" aria-hidden="true"></i></button></td>
					      <?php echo form_close();  } ?>
					     
					    </tr>
					    <?php } ?>
					   
					  </tbody>
					</table>
	        	</div>
        	<div class="row">
        		<div class="col-md-12 add-buttons">
        			<button type="submit" class="btn save"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
        			<a class="btn btn-light back" href="<?php echo base_url("admin/installationCommittee"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        	<?php// echo form_hidden('event_info_eventId',$infos{'id'});
        		  //echo form_hidden('perMemberCost',$infos{'cost'});
        	?>
        	
        </div>
    </div>
</div>

<?php include('footer.php');?>
