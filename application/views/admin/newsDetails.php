<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">News Details</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">All News</a></li>
		  <li class="breadcrumb-item active">News Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open_multipart('admin/updateNews', 'class="addNews-form"')?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						    <label for="newsTitle">News Title</label>
						    <?php echo form_input(['name'=>'newsTitle', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'value'=>set_value('newsTitle').$value->newsTitle]);?>
					    	<div class="errorClass"><?php echo form_error('newsTitle'); ?></div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						    <label for="newsDetails">News Details</label>
						    <?php echo form_textarea(['name'=>'newsDetails', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'rows'=>'3', 'value'=>set_value('newsDetails').$value->newsDetails]);?>
					    	<div class="errorClass"><?php echo form_error('newsDetails'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group" >
							<label>Main Image</label>
							<?php echo form_upload(['name'=>'newsImage', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'type'=>'file', 'required'=>'required', 'value'=>set_value('newsImage')]);
								  echo form_error('newsImage');
							?>		
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group" >
							<label>Other Images</label>
							<?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'type'=>'file', 'value'=>set_value('news_pic')]);
									  echo form_error('news_pic');
								?>		
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="form-group">
						    <label for="newsDate">Date</label>
						    <?php echo form_input(['name'=>'newsDate', 'class'=>'form-control datepicker-here removeDis', 'disabled'=>'disabled', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('newsDate').$value->newsDate]);?>
					    	<div class="errorClass"><?php echo form_error('newsDate'); ?></div>
						</div>
					</div>
				</div>
				<?php echo form_input(['name'=>'newsInfoId', 'type'=>'hidden' , 'class'=>'form-control datepicker-here removeDis', 'disabled'=>'disabled', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('newsInfoId').$value->newsInfoId]);?>
	        	<div class="row">
	        		<div class="col-md-12 edit-buttons">
						<a type="button" class="btn btn-light edit" id="removeDisabled"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

						<button type="submit" class="btn save hidden hiddenButton"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>

						<a class="btn cancel hidden hiddenButton" id="addDisabled" ><i class="fa fa-reply-all" aria-hidden="true"></i>  Cancel</a>
						
						<a class="btn btn-light back" href="<?php echo base_url("admin/allNews"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="jumborton">
		<?php echo form_open_multipart('admin/updateNews', 'class="addNews-form"')?>
			<div class="row">
				<div class="col-md-3 col-md-offset-4">
					<div class="form-group">						
						<div class="row">
							<div class="news-main-image m-b-5">
						      <img src="<?php echo base_url('assets/img/back.jpg'); ?>" class="img-responsive">
						    </div>
						</div>
					    <?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
								  echo form_error('news_pic');
							?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">						
						<div class="row">
							<div class="news-main-image m-b-5">
						      <img src="<?php echo base_url('assets/img/back.jpg'); ?>" class="img-responsive">
						    </div>
						</div>
					    <?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
								  echo form_error('news_pic');
							?>
					</div>
				</div>
				<div class="col-md-3 ">
					<div class="form-group">						
						<div class="row">
							<div class="news-main-image m-b-5">
						      <img src="<?php echo base_url('assets/img/back.jpg'); ?>" class="img-responsive">
						    </div>
						</div>
					    <?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
								  echo form_error('news_pic');
							?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">						
						<div class="row">
							<div class="news-main-image m-b-5">
						      <img src="<?php echo base_url('assets/img/back.jpg'); ?>" class="img-responsive">
						    </div>
						</div>
					    <?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
								  echo form_error('news_pic');
							?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">						
						<div class="row">
							<div class="news-main-image m-b-5">
						      <img src="<?php echo base_url('assets/img/back.jpg'); ?>" class="img-responsive">
						    </div>
						</div>
					    <?php echo form_upload(['name'=>'news_pic[]', 'multiple'=>'multiple', 'class'=>'form-control', 'type'=>'file', 'value'=>set_value('news_pic')]);
								  echo form_error('news_pic');
							?>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<button type="submit" class="btn save"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
</div>

<?php include('footer.php');?>
