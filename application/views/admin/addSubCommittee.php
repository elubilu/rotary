<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Create Sub Committee</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Committee</a></li>
		  <li class="breadcrumb-item active">Add Sub Committee</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/insertSubCommitteeMember', 'class="addSubCommittee-form"')?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						    <label for="subCommitteeTitle">Sub Committee Title</label>
						    <?php echo form_input(['name'=>'subCommitteeTitle', 'class'=>'form-control', 'value'=>set_value('userName')]);?>
					    	<div class="errorClass"><?php echo form_error('subCommitteeTitle'); ?></div>
						</div>
					</div>
					<!--<div class="col-md-6">
						<div class="form-group">
						    <label for="committee_member_type">Select Member For Committee</label>
						    <select name="committee_member_type" class="form-control">					
								<option value="1">Member 1</option>	
								<option value="2">Member 2</option>	
							</select>
					    	<div class="errorClass"><?php //echo form_error('committee_member_type'); ?></div>
						</div>
					</div>-->
					<div class="col-md-12">
						<div class="form-group">
						    <label for="subCommitteeYear">Year</label>
						    <?php echo form_input(['name'=>'subCommitteeYear', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('ci')]);?>
					    	<div class="errorClass"><?php echo form_error('subCommitteeYear'); ?></div>
						</div>
					</div>
					<!--<div class="col-md-12">
						<div class="form-group">
						    <label for="committee_member_responsibility">Responsibility</label>
						    <?php //echo form_textarea(['name'=>'committee_member_responsibility', 'class'=>'form-control', 'rows'=>'3', 'value'=>set_value('address')]);?>
					    	<div class="errorClass"><?php //echo form_error('committee_member_responsibility'); ?></div>
						</div>
					</div>-->
				</div>	
	        	<div class="row">
	        		<div class="col-md-12 add-buttons">
	        			<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>
	        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
	        			<a class="btn btn-light back" href="<?php echo base_url("admin/subCommittee"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
    	</div>
	</div>
</div>
<?php include('footer.php');?>
