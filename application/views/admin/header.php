<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rotary Club</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />

    <!-- MetisMenu CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/metisMenu.min.css'); ?>" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sb-admin-2.css'); ?>" />

    <!-- Morris Charts CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/morris.css'); ?>" />

    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Rotary Club</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo base_url('admin/profile'); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/member'); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> Member<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/addMember'); ?>">Add Member</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/allMember'); ?>">All Member</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/event'); ?>"><i class="fa fa-table fa-fw"></i> Events<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/addEvents'); ?>">Add Events</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/allEvents'); ?>">All Events</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/memberEventsPayment'); ?>">Member Events Payment</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/paymentHistory'); ?>"><i class="fa fa-google-wallet fa-fw"></i> Payment History</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/committee'); ?>"><i class="fa fa-edit fa-fw"></i> Committee<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/subCommittee'); ?>"> Sub Committe</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/installationCommittee'); ?>"> Installation Committee</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!--<li>
                            <a href="<?php echo base_url('admin/createCommittee'); ?>"><i class="fa fa-edit fa-fw"></i> Create Committee</a>
                        </li>-->
                        <!--<li>
                            <a href="<?php echo base_url('admin/attendanceSheet'); ?>"><i class="fa fa-wrench fa-fw"></i> Attendance Sheet</a>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url('admin/attendance'); ?>"><i class="fa fa-wrench fa-fw"></i> Attendance<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/attendanceSheet'); ?>">Attendance Sheet</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/allAttendance'); ?>">All Attendance</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!--<li>
                            <a href="<?php// echo base_url('admin/addNews'); ?>"><i class="fa fa-external-link" aria-hidden="true"></i> Add News</a>
                        </li>-->
                        <li>
                            <a href="<?php echo base_url('admin/news'); ?>"><i class="fa fa-external-link" aria-hidden="true"></i> News<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/addNews'); ?>">Add News</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/allNews'); ?>">All News</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/notification'); ?>"><i class="fa fa-sitemap fa-fw"></i> Notification</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/gallery'); ?>"><i class="fa fa-camera"></i> Gallery</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/others'); ?>"><i class="fa fa-files-o fa-fw"></i> Others<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('admin/blankPage'); ?>">Blank Page</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('admin/logInPage'); ?>">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            
            
        