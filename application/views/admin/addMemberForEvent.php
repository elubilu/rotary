<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add Event Member</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item active">Add Event Member</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/storeMemberForEvent', 'class="addMember-form"')?>
				
	        	<div class="addMemberForEvent">
	        		<table class="table">
					  <thead class="thead-inverse">
					    <tr>
					      <th>Select</th>
					      <th>Name</th>
					      <th>ID</th>
					      <th>Position</th>
					      <th>Option</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($data as $value){ ?>
					  	
					    <tr>
					      <td>
							<div class="form-check">
							  <label class="form-check-label">
								<?php if($selected){ ?>
								<?php if(in_array($value->memberId,$selected,true)){ ?>
									<input class="form-check-input" name="" type="checkbox" id="blankCheckbox" value="" aria-label="..." checked >
							    <?php } else{ ?>
									<input class="form-check-input" name="memberList[]" type="checkbox" id="blankCheckbox" value="<?php echo $value->memberId ?>" aria-label="...">
								<?php } } else{ ?>
									<input class="form-check-input" name="memberList[]" type="checkbox" id="blankCheckbox" value="<?php echo $value->memberId ?>" aria-label="...">
								<?php } ?>
							  </label>
							</div>
					      </td>
					      <td><?php echo $value->memberName ?></td>
					      <td><?php echo $value->clubId ?></td>
					      <td><?php echo $value->Position ?></td>
					      <td> <?php  if($selected){ ?>
					      		 <a href="<?php echo base_url("admin/deleteEventMember/{$value->memberId}/{$event_id}"); ?>">
											<span class="btn btn-danger">
											  <i class="fa fa-times" aria-hidden="true"></i>
											</span>
								  </a>
								  <?php  }  ?>
					      	</td>
					    </tr>
					    <?php } ?>
					    
					  </tbody>
					</table>
	        	</div>
        	<div class="row">
        		<div class="col-md-12 add-buttons">
        			<?php// echo form_submit('', 'Add' , 'class="btn btn-light add"'); ?>
        			<!--<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>-->
        			<button type="submit" class="btn save"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
        			<a class="btn btn-light back" href="<?php echo base_url("admin/allEvents"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        	<?php echo form_hidden('event_info_eventId',$infos{'id'});
        		  echo form_hidden('perMemberCost',$infos{'cost'});
        	?>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
