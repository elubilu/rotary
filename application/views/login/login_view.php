﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Al Hamra Internation Limited Account Management System">
    <meta name="author" content="Mahfuz Ahamed">

    <title>Rotary Club</title>
    <!-- Custom Fonts -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/datepicker.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/dataTables.bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/metisMenu.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/back-end/css/style.css'); ?>" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style type="text/css">
		@font-face {
			font-family: 'Nunito Sans';
			font-style: normal;
			font-weight: 400;
			src: url('<?php echo base_url('assets/back-end/fonts/NunitoSans-Regular.ttf'); ?>');
		}
		body{
			~background:linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
			~background:linear-gradient(141deg, #2D3A40 0%, #2D3A40 51%, #36464D 75%);
			background-image:url(assets/img/1.jpg);
			background-repeat: no-repeat;
			~background-position: 50% 0;
		    -ms-background-size: cover;
		    -o-background-size: cover;
		    -moz-background-size: cover;
		    -webkit-background-size: cover;
		    background-size: cover;
			~background-size: 100% 100%;
			~height:100%;
			~background-color: #41CAC0;
		}
		.flex-center{
			display: flex;
		  	align-items: center;
		  	justify-content: center;
		}
		.thumbnail .caption{
			color: #fff;
		}
		.form-control, .form-control[readonly], .select2-container--default .select2-selection--single {
			~background-color: #FFF;
			background-color: #0D2E41;
			border: 1px solid #000;
			color: #fff;
		}
		.owner h1{
			color: #FFF;
			font-weight: 700;
			font-size: 30px;
			text-shadow: 3px 3px #000;
			margin-bottom: 25px;
		}
		.login-part .thumbnail{
			border: 0px solid #3092c1;
			border-radius:0px;
			padding-bottom:20px;
			margin-bottom:0px;
			background-color: #1D4A5F;
		}
		.developer .thumbnail{
			border: 0px solid #DDD;
			border-radius:0px;
			margin-bottom:0px;
			padding-bottom:0px;
			color:#FFF;
			~background-color:#E3E8EA;
			background-color:#446878;
		}			
		.developer .thumbnail .caption, .developer .thumbnail .caption a{
			~color:#000;
			color:#fff;
		}		
		h2{
			margin-top: 10px;
			font-weight:700;
		}

		.btn-success, .btn-success:hover, .btn-success:focus , .btn-success:active{
		    color: #fff;
		    background-color: #01A7A3;
		    border-color: #01A7A3;
		}
		.btn-danger, .btn-danger:hover, .btn-danger:focus, .btn-danger:active {
		    color: #fff;
		    background-color: #F85F73;
		    border-color: #F85F73;
		}

		.btn:focus,
		.btn:active:focus,
		.btn.active:focus,
		.btn.focus,
		.btn:active.focus,
		.btn.active.focus {
		  	outline: none;
		  	outline-offset: 0px;
		}
		.btn:active,
		.btn.active {
			outline: 0;
			-webkit-box-shadow: none;
				box-shadow: none;
		}
	</style>
</head>

<body>
    <div>
        <?php echo $this->session->flashdata('login'); ?>
    </div>


    <?php if($this->session->flashdata('feedback_successfull'))
			{ ?>
    <div class="alert alert-success">
        <strong>Success!</strong>
        <?php echo $this->session->flashdata('feedback_successfull'); ?>
    </div>
    <?php } ?>


    <?php echo form_open(); 
		
        /*echo form_input(['name'=>'user_id','placeholder'=>'User ID','value'=>set_value('user_id')]);
        echo form_error('user_id');
        echo form_password(['name'=>'pass','placeholder'=>'Your Password']);
        echo form_error('pass');
        echo form_submit(['name'=>'login', 'value'=>'Login']) ;
        echo form_reset(['name'=>'reset', 'value'=>'Reset']) ;

        echo form_close();*/
    ?>
	<section class="login-page flex-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row owner">
						<div class="col-md-12 text-center">
							<h1>Welcome to Rotary Club</h1>
						</div>
					</div>
					<div class="row login-part">
						<div class="col-md-4 col-md-offset-4">
							<div class="thumbnail">
								<div class="caption text-center">
									<div class="row m-bottom-25">
										<div class="col-md-12">
											<h2>LOG IN</h2>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div id="result" style="display: none" class="text-xs-center text-sm-center text-md-center text-lg-center text-xl-center red-text">
											<p class="label_output" id="value"></p>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">	
												<?php echo form_input(['name'=>'email', 'id' => 'name', 'placeholder' => 'User Name', 'class'=>'form-control input-lg', 'value'=>set_value('email')]); 
												echo form_error('email'); ?>
												<div style="" id="email" class="warningSize red-text"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<?php echo form_password(['name'=>'password', 'id' => 'pwd', 'placeholder' => 'Password', 'class'=>'form-control input-lg']); 
												echo form_error('password'); ?>
												<div style="" id="password" class="warningSize red-text"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<button type="submit" class="btn btn-success" value="Login" name="login" id="submit"><i class="fa fa-sign-in fa-fw"></i> Login</button>
											<button type="reset" class="btn btn-danger" value="Reset" name="login"><i class="fa fa-refresh fa-fw"></i> Reset</button>
											<?php echo form_close();?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row developer">
						<div class="col-md-4 col-md-offset-4 text-center">
							<div class="thumbnail">
								<div class="caption">
									<b>System Developed By >> <a href="http://starlabit.com.bd/">StarLab IT</a></b>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <!-- jQuery -->
	<script src="<?php echo base_url('assets/back-end/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/bootstrap-datepicker.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/metisMenu.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/back-end/js/script.js'); ?>"></script>
	<script type="text/javascript">
        // Ajax post
        $(document).ready(function() {
            $("#submit").click(function(event) {
                event.preventDefault();
                var email = $("input#name").val();
                var password = $("input#pwd").val();
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + "login/user_login",
                    dataType: 'json',
                    data: {
                        email: email,
                        password: password
                    },
                    success: function(res) {
                        if (res) {
                            // Show Entered Value
                            if (res.status === true)
                                document.location.href = res.redirect;
                            else {
                                jQuery("div#result").show();
                                jQuery("#value").html(res.errors);
                                jQuery("#email").html(res.email);
                                jQuery("#password").html(res.password);
                                //jQuery("div#value_pwd").html(res.pwd);
                            }
                        }
                    }
                });
            });
        });
    </script>
    <script>
    	var windowHeight = $(window).height();
		$('.login-page').css('min-height', windowHeight);    	
    </script>
</body>

</html>