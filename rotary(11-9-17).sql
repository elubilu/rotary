-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2017 at 07:45 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotary`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_details`
--

CREATE TABLE `attendance_details` (
  `attendanceDetailsId` int(11) NOT NULL,
  `attendabce_info_attendanceInfoId` int(11) NOT NULL,
  `member_info_memberId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `attendance_details`
--

INSERT INTO `attendance_details` (`attendanceDetailsId`, `attendabce_info_attendanceInfoId`, `member_info_memberId`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `attendance_info`
--

CREATE TABLE `attendance_info` (
  `attendanceInfoId` int(11) NOT NULL,
  `event_info_eventId` int(11) NOT NULL,
  `meetingDate` datetime NOT NULL,
  `attendanceStatus` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `attendance_info`
--

INSERT INTO `attendance_info` (`attendanceInfoId`, `event_info_eventId`, `meetingDate`, `attendanceStatus`) VALUES
(1, 2, '2017-09-05 00:00:00', 1),
(2, 2, '2017-09-05 00:00:00', 1),
(3, 2, '2017-09-11 00:00:00', 1),
(4, 2, '2017-09-04 00:00:00', 1),
(5, 2, '2017-09-11 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `committee_details`
--

CREATE TABLE `committee_details` (
  `committee_detailsId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_info`
--

CREATE TABLE `committee_info` (
  `committee_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `donation_info`
--

CREATE TABLE `donation_info` (
  `donation_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_info`
--

CREATE TABLE `event_info` (
  `event_infoId` int(11) NOT NULL,
  `eventTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `eventDetails` text COLLATE utf8_bin NOT NULL,
  `eventPlace` varchar(400) COLLATE utf8_bin NOT NULL,
  `event_type_eventId` int(11) NOT NULL,
  `eventDate` datetime NOT NULL,
  `eventPerHeadAmount` double NOT NULL,
  `eventStatua` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `eventAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_info`
--

INSERT INTO `event_info` (`event_infoId`, `eventTitle`, `eventDetails`, `eventPlace`, `event_type_eventId`, `eventDate`, `eventPerHeadAmount`, `eventStatua`, `eventAddedDate`) VALUES
(1, 'title', 'details', 'sylhet', 1, '2017-08-31 17:15:00', 5000, 1, '0000-00-00 00:00:00'),
(2, 'Installtion', 'Everthing', 'sylhet', 2, '2017-08-07 17:30:00', 320, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `event_member_info`
--

CREATE TABLE `event_member_info` (
  `eventMemberInfoId` int(11) NOT NULL,
  `event_info_eventId` int(11) NOT NULL,
  `member_info_memberId` int(11) NOT NULL,
  `perMemberCost` double NOT NULL,
  `eventCostPaymentStatus` int(2) NOT NULL DEFAULT '0' COMMENT '1=done, 2=half Payment, 0=pending ',
  `eventCostPaidAmount` double NOT NULL DEFAULT '0',
  `eventPaymentDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `event_member_info`
--

INSERT INTO `event_member_info` (`eventMemberInfoId`, `event_info_eventId`, `member_info_memberId`, `perMemberCost`, `eventCostPaymentStatus`, `eventCostPaidAmount`, `eventPaymentDate`) VALUES
(1, 1, 1, 5000, 1, 5000, '2017-08-29 00:00:00'),
(2, 1, 2, 5000, 2, 500, '2017-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `event_payment_info`
--

CREATE TABLE `event_payment_info` (
  `eventPaymentInfoId` int(11) NOT NULL,
  `event_info_eventId` int(11) NOT NULL,
  `eventPaidAmount` double NOT NULL,
  `member_info_memberId` int(11) NOT NULL,
  `paymentPaidType` int(2) NOT NULL COMMENT '1=paid, 2=half Payment',
  `paymentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paymentReceivedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_payment_info`
--

INSERT INTO `event_payment_info` (`eventPaymentInfoId`, `event_info_eventId`, `eventPaidAmount`, `member_info_memberId`, `paymentPaidType`, `paymentDate`, `paymentReceivedBy`) VALUES
(1, 2, 500, 2, 1, '2017-08-02 00:00:00', 0),
(2, 1, 5000, 1, 1, '2017-08-29 00:00:00', 0),
(3, 1, 500, 2, 2, '2017-08-31 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `eventTypeId` int(100) NOT NULL,
  `eventTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`eventTypeId`, `eventTypeTitle`) VALUES
(1, 'Programme'),
(2, 'Meeting');

-- --------------------------------------------------------

--
-- Table structure for table `instalation_committee_info`
--

CREATE TABLE `instalation_committee_info` (
  `instalationCommitteeInfoId` int(100) NOT NULL,
  `instalationCommitteeTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `event_info_instalationCommitteeEventId` int(11) NOT NULL,
  `instalationCommitteeYear` date NOT NULL,
  `instalationCommitteeStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `instalationCommitteeAddedDate` datetime NOT NULL,
  `instalationCommitteeAddedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `instalation_committee_info`
--

INSERT INTO `instalation_committee_info` (`instalationCommitteeInfoId`, `instalationCommitteeTitle`, `event_info_instalationCommitteeEventId`, `instalationCommitteeYear`, `instalationCommitteeStatus`, `instalationCommitteeAddedDate`, `instalationCommitteeAddedBy`) VALUES
(1, 'Volunteer', 1, '2017-09-13', 1, '2017-09-11 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `installation_committee_details`
--

CREATE TABLE `installation_committee_details` (
  `installationCommitteeDetailsId` int(100) NOT NULL,
  `installation_committee_Info_installationCommitteeInfoId` int(11) NOT NULL,
  `member_info_installationCommitteeMemberId` int(11) NOT NULL,
  `installationCommitteeMemberPosition` varchar(400) COLLATE utf8_bin NOT NULL,
  `installationCommitteeMemberResponsibility` text COLLATE utf8_bin NOT NULL,
  `installationCommitteeMemberAddedDate` datetime NOT NULL,
  `installationCommitteeMemberStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `logo_info`
--

CREATE TABLE `logo_info` (
  `logo_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE `member_info` (
  `memberId` int(100) NOT NULL,
  `memberName` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberNumber` varchar(100) COLLATE utf8_bin NOT NULL,
  `clubId` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberJoinDate` date NOT NULL,
  `memberImage` varchar(750) COLLATE utf8_bin NOT NULL,
  `Position` varchar(100) COLLATE utf8_bin NOT NULL,
  `BloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `WeedingDate` date NOT NULL,
  `Birthday` date NOT NULL,
  `SpouseName` varchar(100) COLLATE utf8_bin NOT NULL,
  `SpouseBirthday` date NOT NULL,
  `SpouseBloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddress` text COLLATE utf8_bin NOT NULL,
  `memberMobile` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberEmail` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberPassword` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `memberTypeId` int(100) NOT NULL,
  `memberPositionType` int(6) NOT NULL DEFAULT '4' COMMENT 'to sort member positions for view',
  `memberGender` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memberLoginTypeId` int(100) NOT NULL,
  `memberTotalBalance` double NOT NULL,
  `memberDueAmount` double NOT NULL,
  `memberEditDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`memberId`, `memberName`, `memberNumber`, `clubId`, `memberJoinDate`, `memberImage`, `Position`, `BloodGroup`, `WeedingDate`, `Birthday`, `SpouseName`, `SpouseBirthday`, `SpouseBloodGroup`, `memberAddress`, `memberMobile`, `memberEmail`, `memberPassword`, `memberStatus`, `memberTypeId`, `memberPositionType`, `memberGender`, `memberAddedDate`, `memberLoginTypeId`, `memberTotalBalance`, `memberDueAmount`, `memberEditDate`) VALUES
(1, 'Kshoma Kanta Chakrabartty', '12345', 'Accounting Management', '2007-12-01', 'images/031.jpg', 'President', 'B+', '1986-02-16', '1955-02-12', 'Ratna Rani Chakrabartty', '1959-05-05', 'O+', 'Mohona 104, Block#B, Korerpara, Sylhet.', '01720147149', 'rtn.kshoma@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 1, 0, 'Male', '2017-08-23 15:25:38', 1, 1000, 0, '2017-08-23 09:28:31'),
(2, 'admin', '12365', 'admn', '2017-08-09', 'images/0511.jpg', 'admin', 'B+', '1970-01-01', '2017-08-23', '', '1970-01-01', '', 'address', '12345678901', 'admin@gmail.com', '25f9e794323b453885f5181f1b624d0b', 1, 5, 4, 'Female', '2017-08-24 02:47:57', 1, 500, 0, '2017-08-23 20:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `member_login_type`
--

CREATE TABLE `member_login_type` (
  `memberLoginTypeId` int(100) NOT NULL,
  `memberLoginTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_login_type`
--

INSERT INTO `member_login_type` (`memberLoginTypeId`, `memberLoginTypeTitle`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `member_payment_info`
--

CREATE TABLE `member_payment_info` (
  `member_payment_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_type`
--

CREATE TABLE `member_type` (
  `memberTypeId` int(100) NOT NULL,
  `memberTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_type`
--

INSERT INTO `member_type` (`memberTypeId`, `memberTypeTitle`) VALUES
(1, 'MPHF'),
(2, 'MPHF MD'),
(3, 'PHF'),
(4, 'RFSM'),
(5, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

CREATE TABLE `news_images` (
  `newsImagesId` int(11) NOT NULL,
  `news_info_newsId` int(11) NOT NULL,
  `newsImage` varchar(400) COLLATE utf8mb4_bin NOT NULL,
  `newsImageStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_info`
--

CREATE TABLE `news_info` (
  `newsInfoId` int(100) NOT NULL,
  `newsTitle` varchar(700) COLLATE utf8_bin NOT NULL,
  `newsDetails` text COLLATE utf8_bin NOT NULL,
  `newsMainImage` varchar(400) COLLATE utf8_bin NOT NULL,
  `newsDate` date NOT NULL,
  `newsStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_info`
--

INSERT INTO `news_info` (`newsInfoId`, `newsTitle`, `newsDetails`, `newsMainImage`, `newsDate`, `newsStatus`) VALUES
(1, 'Registration', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. \r\n\r\nt has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 'images/back7.jpg', '2017-08-01', 1),
(2, 'Member Registration', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'images/20663103_1504720202920920_381609570_o1.jpg', '1970-01-01', 1),
(3, 'Membership', 'Proposal of the club : Raised by PDG Rtn. M.A. Wahab in October 1985. \r\nFirst organizing meeting : October, 1985 at the residence of Mr. Mujibul Haq Choudhury. \r\nFirst meeting as provisional club : Convened on 7th January, 1986 at Hotel Hiltown, Sylhet. \r\nSponsor Rotary Club : Rotary Club of Sylhet \r\nOfficial admission into Rotary : 16th April, 1986 \r\nCharter President : Rtn. Mujibul Haq Choudhury \r\nCharter Secretary : Rtn. Md. Enamul Haque PHF, MC \r\nNumber of charter members : 25 \r\nClub ID No. : 023619 \r\nChief guest at the charter \r\npresentation ceremony : Professor Mohammed Abdul Aziz, Principal, M.C College, Sylhet. \r\nWeekly meeting : Every Friday (Winter: 1830 hrs. Summer: 1930 hrs.) \r\nVenue : Jalalabad Disabled Rehab Centre & Hospital Hazrat Manikpir (R) Road, Sylhet. \r\nGSE Team leader from the club : Rtn. PP Dr. Monzurul Hoque Choudhury MPHF, MC to Texas, USA in 1995-96. Rtn. PP Shohid Ahmed Choudhury PHF to BC, Canada in the year 2010-11 \r\nDistrict Governor 2010-11 : Rtn. Dr. Monzurul Hoque Choudhury MPHF, MC \r\nDistrict Governo 2016-17 : Rtn. Shohid Ahmed Chowdhury PHF \r\nDistrict Governo 2019-20 : Rtn. M Ataur Rahman Pir PHF MC', 'images/back31.jpg', '2017-09-05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_committee_details`
--

CREATE TABLE `sub_committee_details` (
  `subCommitteeDetailsId` int(100) NOT NULL,
  `sub_committee_Info_subCommitteeInfoId` int(11) NOT NULL,
  `member_info_subCommitteeMemberId` int(11) NOT NULL,
  `subCommitteeMemberPosition` varchar(400) COLLATE utf8_bin NOT NULL,
  `subCommitteeMemberResponsibility` text COLLATE utf8_bin NOT NULL,
  `subCommitteeMemberAddedDate` datetime NOT NULL,
  `subCommitteeMemberStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `sub_committee_info`
--

CREATE TABLE `sub_committee_info` (
  `subCommitteeInfoId` int(100) NOT NULL,
  `subCommitteeTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `subCommitteeYear` date NOT NULL,
  `subCommitteeStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `subCommitteeAddedDate` datetime NOT NULL,
  `subCommitteeAddedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sub_committee_info`
--

INSERT INTO `sub_committee_info` (`subCommitteeInfoId`, `subCommitteeTitle`, `subCommitteeYear`, `subCommitteeStatus`, `subCommitteeAddedDate`, `subCommitteeAddedBy`) VALUES
(1, 'Organizer', '2017-09-13', 1, '2017-09-11 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_details`
--
ALTER TABLE `attendance_details`
  ADD PRIMARY KEY (`attendanceDetailsId`);

--
-- Indexes for table `attendance_info`
--
ALTER TABLE `attendance_info`
  ADD PRIMARY KEY (`attendanceInfoId`);

--
-- Indexes for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD PRIMARY KEY (`committee_detailsId`);

--
-- Indexes for table `committee_info`
--
ALTER TABLE `committee_info`
  ADD PRIMARY KEY (`committee_infoId`);

--
-- Indexes for table `donation_info`
--
ALTER TABLE `donation_info`
  ADD PRIMARY KEY (`donation_infoId`);

--
-- Indexes for table `event_info`
--
ALTER TABLE `event_info`
  ADD PRIMARY KEY (`event_infoId`);

--
-- Indexes for table `event_member_info`
--
ALTER TABLE `event_member_info`
  ADD PRIMARY KEY (`eventMemberInfoId`);

--
-- Indexes for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  ADD PRIMARY KEY (`eventPaymentInfoId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`eventTypeId`);

--
-- Indexes for table `instalation_committee_info`
--
ALTER TABLE `instalation_committee_info`
  ADD PRIMARY KEY (`instalationCommitteeInfoId`);

--
-- Indexes for table `installation_committee_details`
--
ALTER TABLE `installation_committee_details`
  ADD PRIMARY KEY (`installationCommitteeDetailsId`);

--
-- Indexes for table `logo_info`
--
ALTER TABLE `logo_info`
  ADD PRIMARY KEY (`logo_infoId`);

--
-- Indexes for table `member_info`
--
ALTER TABLE `member_info`
  ADD PRIMARY KEY (`memberId`),
  ADD UNIQUE KEY `memberEmail` (`memberEmail`),
  ADD UNIQUE KEY `memberNumber` (`memberNumber`);

--
-- Indexes for table `member_login_type`
--
ALTER TABLE `member_login_type`
  ADD PRIMARY KEY (`memberLoginTypeId`);

--
-- Indexes for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  ADD PRIMARY KEY (`member_payment_infoId`);

--
-- Indexes for table `member_type`
--
ALTER TABLE `member_type`
  ADD PRIMARY KEY (`memberTypeId`);

--
-- Indexes for table `news_images`
--
ALTER TABLE `news_images`
  ADD PRIMARY KEY (`newsImagesId`);

--
-- Indexes for table `news_info`
--
ALTER TABLE `news_info`
  ADD PRIMARY KEY (`newsInfoId`);

--
-- Indexes for table `sub_committee_details`
--
ALTER TABLE `sub_committee_details`
  ADD PRIMARY KEY (`subCommitteeDetailsId`);

--
-- Indexes for table `sub_committee_info`
--
ALTER TABLE `sub_committee_info`
  ADD PRIMARY KEY (`subCommitteeInfoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_details`
--
ALTER TABLE `attendance_details`
  MODIFY `attendanceDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `attendance_info`
--
ALTER TABLE `attendance_info`
  MODIFY `attendanceInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `committee_details`
--
ALTER TABLE `committee_details`
  MODIFY `committee_detailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_info`
--
ALTER TABLE `committee_info`
  MODIFY `committee_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donation_info`
--
ALTER TABLE `donation_info`
  MODIFY `donation_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_info`
--
ALTER TABLE `event_info`
  MODIFY `event_infoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event_member_info`
--
ALTER TABLE `event_member_info`
  MODIFY `eventMemberInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  MODIFY `eventPaymentInfoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `eventTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `instalation_committee_info`
--
ALTER TABLE `instalation_committee_info`
  MODIFY `instalationCommitteeInfoId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `installation_committee_details`
--
ALTER TABLE `installation_committee_details`
  MODIFY `installationCommitteeDetailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logo_info`
--
ALTER TABLE `logo_info`
  MODIFY `logo_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_info`
--
ALTER TABLE `member_info`
  MODIFY `memberId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `member_login_type`
--
ALTER TABLE `member_login_type`
  MODIFY `memberLoginTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  MODIFY `member_payment_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_type`
--
ALTER TABLE `member_type`
  MODIFY `memberTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `news_images`
--
ALTER TABLE `news_images`
  MODIFY `newsImagesId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_info`
--
ALTER TABLE `news_info`
  MODIFY `newsInfoId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_committee_details`
--
ALTER TABLE `sub_committee_details`
  MODIFY `subCommitteeDetailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_committee_info`
--
ALTER TABLE `sub_committee_info`
  MODIFY `subCommitteeInfoId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
