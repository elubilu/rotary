<?php include('header.php');?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Member Details</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Member</a></li>
		  <li class="breadcrumb-item active">Member Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open_multipart('admin/updateMember', 'class="memberDetails-form"')?>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberName">Name</label>
						    <?php echo form_input(['name'=>'memberName', 'class'=>'form-control removeDis ', 'disabled'=>'disabled', 'value'=>set_value('').$data->memberName]);?>
						   	<div class="errorClass"><?php echo form_error('memberName'); ?></div>

						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="clubId">CL</label>
						    <?php echo form_input(['name'=>'clubId', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'value'=>set_value('ci').$data->clubId]);?>
						    <div class="errorClass"><?php echo form_error('clubId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberNumber">Member Number</label>
						    <?php echo form_input(['name'=>'memberNumber', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'value'=>set_value('memberNumber').$data->memberNumber]);?>
					    	<div class="errorClass"><?php echo form_error('memberNumber'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberJoinDate">Indate</label>
						    <?php echo form_input(['name'=>'memberJoinDate', 'class'=>'form-control removeDis datepicker-here', 'disabled'=>'disabled',
						     'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('').$data->memberJoinDate]);?>
						    <div class="errorClass"><?php echo form_error('memberJoinDate'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="Position">Hpc</label>
						    <?php echo form_input(['name'=>'Position', 'class'=>'form-control removeDis',
						    'disabled'=>'disabled', 'value'=>set_value('ci').$data->Position]);?>
						    <div class="errorClass"><?php echo form_error('Position'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="BloodGroup">Blood Group</label>
						    <select name="BloodGroup" class="form-control removeDis" disabled required>		
								<option value="<?php echo $data->BloodGroup ?>" selected disabled > <?php echo $data->BloodGroup ?></option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>	
								<option value="B+">B+</option>	
								<option value="B-">B-</option>	
								<option value="O+">O+</option>	
								<option value="O-">O-</option>	
								<option value="AB+">AB+</option>	
								<option value="AB-">AB-</option>	
							</select>
						    <div class="errorClass"><?php echo form_error('BloodGroup'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="Birthday">Birth Date</label>
						    <?php echo form_input(['name'=>'Birthday', 'class'=>'form-control removeDis datepicker-here', 'disabled'=>'disabled', 'data-position'=>'bottom left', 
						    'data-language'=>'en', 'value'=>set_value('ci').$data->Birthday]);?>
						    <div class="errorClass">
						    <?php echo form_error('Birthday'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberTypeId">Member Type</label>
						    <select name="memberTypeId" class="form-control removeDis" disabled required>		
								<option value="<?php echo $data->memberTypeId ?>" selected disabled > <?php echo $data->memberTypeTitle ?></option>
								<option value="5">Member</option>
								<option value="1">MPHF</option>	
								<option value="2">MPHF MD</option>	
								<option value="3">PHF</option>	
								<option value="4">RFSM</option>		
							</select>
							<div class="errorClass"><?php echo form_error('memberTypeId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberGender">Member Gender</label>
						    <select name="memberGender" class="form-control removeDis" disabled required>				
								<option value="<?php echo $data->memberGender ?>" selected disabled > <?php echo $data->memberGender ?></option>	
								<option value="Male">Male</option>	
								<option value="Female">Female</option>
							</select>
					    	<div class="errorClass"><?php echo form_error('memberGender'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberLoginTypeId">User Type</label>
						    <select name="memberLoginTypeId" class="form-control removeDis"  disabled required>					
								<option value="<?php echo $data->memberLoginTypeId ?>" selected disabled>
									<?php if($data->memberLoginTypeId == 1) {echo 'Admin';} else {echo "User";}; ?>
								</option>	
								<option value="1">Admin</option>	
								<option value="2">User</option>	
							</select>
							<div class="errorClass"><?php echo form_error('memberLoginTypeId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberPositionType">Member Position Type</label>
						    <select name="memberPositionType" class="form-control removeDis"  disabled required>
						    	<option value="<?php echo $data->memberPositionType ?>" selected disabled><?php if($data->memberPositionType == 1) {echo 'President';} else if($data->memberPositionType == 2) {echo 'Secretary';} else if($data->memberPositionType == 3) {echo 'President Elect';} else {echo "Other";}; ?></option>		
								<option value="1">President</option>	
								<option value="2">Secretary</option>	
								<option value="3">President Elect</option>	
								<option value="4">Other</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberPositionType'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberStatus">Member Status</label>
						    <select name="memberStatus" class="form-control removeDis" disabled required>				
								<option value="<?php echo $data->memberStatus ?>" selected disabled><?php if($data->memberStatus == 1) {echo 'Active';} else {echo "Inactive";}; ?></option>	
								<option value="1">Active</option>	
								<option value="2">Inactive</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('memberStatus'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberMobile">Phone</label>
						    <?php echo form_input(['name'=>'memberMobile', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'type'=>'number', 'value'=>set_value('ci').$data->memberMobile]);?>						
						    <div class="errorClass"><?php echo form_error('memberMobile'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="memberEmail">Email</label>
						    <?php echo form_input(['name'=>'memberEmail', 'class'=>'form-control removeDis',
						    'disabled'=>'disabled', 'value'=>set_value('ci').$data->memberEmail]);?>
						     <div class="errorClass"><?php echo form_error('memberEmail'); ?></div>
						</div>
					</div>
					<div class="col-md-3 col-md-offset-5">
						<div class="form-group">						
							<div class="profile-image">
						      <img src="<?php echo base_url($data->memberImage); ?>" alt="..." class="img-responsive">
						    </div>
						</div>
					</div>
					<div class="col-md-4 col-md-offset-4">
						<div class="form-group" >
							<label>Member Image</label>
							<?php echo form_upload(['name'=>'memberImage', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'type'=>'file', 'value'=>set_value($data->memberImage).$data->memberImage]);?>
							<div class="errorClass"><?php echo form_error('memberImage'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="memberAddress">Address</label>
						    <?php echo form_textarea(['name'=>'memberAddress', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'rows'=>'3', 'value'=>set_value('').$data->memberAddress]);?>
							<div class="errorClass"><?php echo form_error('memberAddress'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberTotalBalance">Balance</label>
						    <?php echo form_input(['name'=>'memberTotalBalance', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'value'=>set_value('memberTotalBalance').$data->memberTotalBalance]);?>
					    	<div class="errorClass"><?php echo form_error('memberTotalBalance'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="memberDueAmount">Last Due</label>
						    <?php echo form_input(['name'=>'memberDueAmount', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'value'=>set_value('memberDueAmount').$data->memberDueAmount]);?>
					    	<div class="errorClass"><?php echo form_error('memberDueAmount'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="WeedingDate">Wedding Date</label>
						    <?php echo form_input(['name'=>'WeedingDate', 'class'=>'form-control removeDis datepicker-here', 
						    'data-position'=>'bottom left', 'data-language'=>'en', 'disabled'=>'disabled','value'=>set_value('').$data->WeedingDate]);?>
							<div class="errorClass"><?php echo form_error('WeedingDate'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseName">Spouse Name</label>
						    <?php echo form_input(['name'=>'SpouseName', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'value'=>set_value('ci').$data->SpouseName]);?>
							<div class="errorClass"><?php echo form_error('SpouseName'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseBirthday">Spouse Birth Date</label>
						    <?php echo form_input(['name'=>'SpouseBirthday', 'class'=>'form-control removeDis datepicker-here', 
						    'data-position'=>'bottom left', 'data-language'=>'en', 'disabled'=>'disabled', 'value'=>set_value('').$data->SpouseBirthday]);?>
							<div class="errorClass"><?php echo form_error('SpouseBirthday'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="SpouseBloodGroup">Spouse Blood Group</label>
						    <select name="SpouseBloodGroup"  class="form-control removeDis" disabled >		
								<option value="<?php echo $data->SpouseBloodGroup ?>" selected disabled><?php echo $data->SpouseBloodGroup ?></option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>	
								<option value="B+">B+</option>	
								<option value="B-">B-</option>	
								<option value="O+">O+</option>	
								<option value="O-">O-</option>	
								<option value="AB+">AB+</option>	
								<option value="AB-">AB-</option>	
							</select>
						    <div class="errorClass"><?php //echo form_error('SpouseBloodGroup'); ?></div>					
						</div>
					</div>
				</div>
        	<?php echo form_input(['name'=>'memberId', 'class'=>'form-control removeDis', 'type'=>'hidden','disabled'=>'disabled', 'value'=>set_value('').$data->memberId]);?>
        	<div class="row">
        		<div class="col-md-12 edit-buttons">
					<a type="button" class="btn btn-light edit" id="removeDisabled"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

					<button type="submit" class="btn save hidden hiddenButton"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>

					<a class="btn cancel hidden hiddenButton" id="addDisabled" ><i class="fa fa-reply-all" aria-hidden="true"></i>  Cancel</a>
					
					<a class="btn btn-light back" href="<?php echo base_url("admin/allMember"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>