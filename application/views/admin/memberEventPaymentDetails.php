<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Member Event Payment Details</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Event</a></li>
		  <li class="breadcrumb-item active">Payment Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<div class="attendenceTable">
        		<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>Name</th>
				      <th>Payment Status</th>
				      <th>Amount</th>
				      <th>Date</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($data as $value){ ?>
				    <tr>
				      <td><?php echo $value->memberName ?></td>
				      <td><?php if($value->eventCostPaymentStatus == 1) {echo 'Done';} else if($value->eventCostPaymentStatus == 2) {echo 'Half Payment';} else {echo "Pending";}; ?></td>
				      <td><?php if($value->eventCostPaymentStatus == 1 || $value->eventCostPaymentStatus == 2) echo $value->eventCostPaidAmount
				       ?></td>
				      <td><?php echo date('M-d, Y  h:i a',strtotime($value->eventPaymentDate)); ?></td>
				    </tr>
				    <?php } ?>
				  </tbody>
				</table>
        	</div>
        	<div class="row">
        		<div class="col-md-12">
        			<a class="btn btn-light back" href="<?php echo base_url("admin/allEvents"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        </div>
    </div>
</div>

<?php include('footer.php');?>
