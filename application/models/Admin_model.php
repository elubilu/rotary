<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

   public function insertMember($data)
	{
		return $this->db->insert('member_info', $data );
	}
	public function get_all_members()
	{
		$data=$this->db 
						->select('*')
						->from('member_info')
						->where('memberStatus',1)
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function get_all_members_name_id()
	{
		$data=$this->db 
						->select('memberName,memberId')
						->from('member_info')
						->where('memberStatus',1)
						->get();
		if($data)
		{
			return $data->result();
		}

	}	
	public function get_responsibility_insta($id)
	{
		$data=$this->db 
						->select('*')
						->from('installation_committee_details')
						->where('installationCommitteeMemberStatus',1)
						->where('installation_committee_Info_installationCommitteeInfoId',$id)
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function get_responsibility_sub($id)
	{
		$data=$this->db 
						->select('*')
						->from('sub_committee_details')
						//->where('installationCommitteeMemberStatus',1)
						->where('sub_committee_Info_subCommitteeInfoId',$id)
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function get_details_by_Id($id)
	{
		$data=$this->db 
						->select('*,member_type.memberTypeTitle')
						->from('member_info')
						->join('member_type', 'member_type.memberTypeId = member_info.memberTypeId')
						->join('member_login_type', 'member_login_type.memberLoginTypeId = member_info.memberLoginTypeId')
						->where('memberId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}

	}
	public function get_all_meeting_attendance()
	{
		$data=$this->db 
						->select('*,event_info.eventTitle')
						->from('attendance_info')
						->join('event_info', 'event_info.event_infoId = attendance_info.event_info_eventId')
						->order_by("meetingDate", "asc")
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function insertEvents($data)
	{
		return $this->db->insert('event_info', $data );
	}
	public function get_all_events()
	{
		$data=$this->db 
						->select('*')
						->from('event_info')
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function insertEventsPayment($data)
	{
		if($this->db->insert('event_payment_info', $data ))
		{
			$value['eventCostPaidAmount']=$data['eventPaidAmount'];
			$value['eventCostPaymentStatus']=$data['paymentPaidType'];
			$value['eventPaymentDate']=$data['paymentDate'];
			$memberId=$data['member_info_memberId'];
			$eventId=$data['event_info_eventId'];
			return $this->db
		  				->where('member_info_memberId',$memberId)
		  				->where('event_info_eventId',$eventId)
		   				->update('event_member_info',$value);
		  

		}
	   else return FALSE;
	}
	public function get_event_details_by_Id($id)
	{
		$data=$this->db 
						->select('*')
						->from('event_info')
						->where('event_infoId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}

	}	
	public function get_attendance_details_by_Id($id)
	{
		$data=$this->db 
						->select('*')
						->from('attendance_details')
						->where('attendabce_info_attendanceInfoId',$id)
						->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function get_attendance_info_by_Id($id)
	{
		$data=$this->db 
						->select('*,event_info.eventTitle')
						->from('attendance_info')
						->join('event_info', 'event_info.event_infoId = attendance_info.event_info_eventId')
						->where('attendanceInfoId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}

	}
	public function updateEvents($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('event_infoId',$data['event_infoId'])
		   				->update('event_info',$data);
		 return  $data;
	}
	public function update_sub_committee_member($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('sub_committee_Info_subCommitteeInfoId',$data['sub_committee_Info_subCommitteeInfoId'])
		  				->where('member_info_subCommitteeMemberId',$data['member_info_subCommitteeMemberId'])
		   				->update('sub_committee_details',$data);
		 return  $data;
	}
	public function update_ins_committee_member($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('installation_committee_Info_installationCommitteeInfoId',$data['installation_committee_Info_installationCommitteeInfoId'])
		  				->where('member_info_installationCommitteeMemberId',$data['member_info_installationCommitteeMemberId'])
		   				->update('installation_committee_details',$data);
		 return  $data;
	}
	public function update_member_info($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('memberId',$data['memberId'])
		   				->update('member_info',$data);
		 return  $data;
	}
	 public function insertComitteeMember($data)
	{
		return $this->db->insert('committee_details', $data );
	}
	public function insertMeetingInfo($data)
	{
		$data['attendanceStatus']=1;
		$members['member_info_memberId']=$data['members'];

		unset($data['members']);
		if($this->db->insert('attendance_info', $data )){
			$value['attendabce_info_attendanceInfoId']=$this->db->insert_id();
			foreach ($members['member_info_memberId'] as $info) {
				$value['member_info_memberId']=$info;
				$this->db->insert('attendance_details',$value);
			}

		//exit();
          return true;
        }

        
	}

	public function get_event_cost_by_event_ID($id)
	{
		$data=$this->db 
				->select('eventPerHeadAmount')
				->from('event_info')
				->where('event_infoId',$id)
				->get();
		if($data)
		{
			return $data->row()->eventPerHeadAmount;
		}
	}

	public function store_member_for_event($data)
	{
		//print_r($data['event_info_eventId']); exit;
		$value['event_info_eventId']=$data['event_info_eventId'];
		$value['perMemberCost']=$data['perMemberCost'];
		if($data['memberList']){
			foreach ($data['memberList'] as $info) {
				$value['member_info_memberId']=$info;
				$this->db->insert('event_member_info',$value);
			}
			return true;
		}
		else{
			return false;
		}

		
		//print_r($info);exit;
	}

	public function selected_member_event_ID($event_infoId)
	{
		$data=$this->db 
			->select('member_info_memberId')
			->from('event_member_info')
			->where('event_info_eventId',$event_infoId)
			->get();

		$members = array();
		if($data->num_rows()){
			$q= $data->result();
			foreach($q as $q){
				array_push($members,$q->member_info_memberId);
			}
			return $members;
		}
		else{
			return FALSE;
		}
	}

	public function get_members_event_payment_details($id)
	{
		$data=$this->db 
					->select('event_member_info.member_info_memberId,event_member_info.eventCostPaymentStatus,event_member_info.eventCostPaidAmount,member_info.memberName,event_member_info.eventPaymentDate')
					->from('event_member_info')
					->join('member_info', 'event_member_info.member_info_memberId = member_info.memberId', 'inner')
					->where('event_info_eventId',$id)
					->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function get_all_members_event_payment_details()
	{
		$data=$this->db 
					->select('event_payment_info.eventPaymentInfoId,event_payment_info.eventPaidAmount,event_payment_info.paymentPaidType,event_payment_info.paymentDate,member_info.memberName,event_info.eventTitle')
					->from('event_payment_info')
					->join('member_info', 'event_payment_info.member_info_memberId = member_info.memberId')
					->join('event_info', 'event_payment_info.event_info_eventId = event_info.event_infoId')
					->get();
		if($data)
		{
			return $data->result();
		}

	}
	public function insert_news($data,$images)
	{
		$this->db->trans_start();
		if($this->db->insert('news_info', $data )){
			$img['news_info_newsId']=$this->db->insert_id();
			foreach($images as $image):
				$img['newsImage']=$image;
				$this->db->insert('news_images',$img);
			endforeach;
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return false;
		}	
		else return true;
	}
	public function insert_sub_committee($data)
	{
		//print_r($data); exit();
		
		//unset($data['member_info_subCommitteeMemberId']);
		return $this->db->insert('sub_committee_info', $data );
	}
	public function insert_instalation_committee($data)
	{
		
		return $this->db->insert('instalation_committee_info', $data );
	}
	public function store_sub_committee_responsibility($data)
	{
		
		return $this->db->insert('sub_committee_details', $data );
	}
	public function store_installation_committee_responsibility($data)
	{
		
		return $this->db->insert('installation_committee_details', $data );
	}
	public function get_all_sub_committee_members()
	{
		$data=$this->db 
				->select('*')
				->from('sub_committee_info')
				->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_all_instalation_committee_members()
	{
		$data=$this->db 
				->select('*')
				->from('instalation_committee_info')
				->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_sub_committee_info_by_id($id)
	{
		$data=$this->db 
				->select('*')
				->from('sub_committee_info')
				->where('subCommitteeInfoId',$id)
				->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_instalation_committee_info_by_id($id)
	{
		$data=$this->db 
				->select('*')
				->from('instalation_committee_info')
				->where('instalationCommitteeInfoId',$id)
				->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_news_info_by_id($id)
	{
		$data=$this->db 
				->select('*')
				->from('news_info')
				->where('newsInfoId',$id)
				->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_all_news()
	{
		$data=$this->db 
				->select('*')
				->from('news_info')
				->where('newsStatus',1)
				->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function update_news($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('newsInfoId',$data['newsInfoId'])
		   				->update('news_info',$data);
		 return  $data;
	}
	public function update_sub_committee_info($data)
	{
		//print_r($data);exit();
		unset($data['member_info_subCommitteeMemberId']);
		unset($data['committee_member_responsibility']);
		$data=$this->db
		  				->where('subCommitteeInfoId',$data['subCommitteeInfoId'])
		   				->update('sub_committee_info',$data);
		 return  $data;
	}
	public function update_Installation_committee_info($data)
	{
		//print_r($data);exit();
		unset($data['committee_member_type']);
		unset($data['committee_member_responsibility']);
		$data=$this->db
		  				->where('instalationCommitteeInfoId',$data['instalationCommitteeInfoId'])
		   				->update('instalation_committee_info',$data);
		 return  $data;
	}
	public function get_position_name_by_id($id)
	{
		 $data=$this->db 
					->select('memberLoginTypeTitle')
					->from('member_login_type')
					->where('memberLoginTypeId',$id)
					->get();
		if($data)
		{
			return $data->row()->memberLoginTypeId;
		}
	}
	public function get_members_birthday($start,$end)
	{
		 $data=$this->db 
					->select('memberName,Birthday,memberId')
					->from('member_info')
					->where('Birthday>=',$start)
					->where('Birthday<=',$end)
					->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_members_weeding($start,$end)
	{
		 $data=$this->db 
					->select('memberName,WeedingDate,memberId')
					->from('member_info')
					->where('WeedingDate>=',$start)
					->where('WeedingDate<=',$end)
					->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_spouse_birthday($start,$end)
	{
		 $data=$this->db 
					->select('memberName,SpouseBirthday,memberId')
					->from('member_info')
					->where('SpouseBirthday>=',$start)
					->where('SpouseBirthday<=',$end)
					->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function update_password($data)
	{
		
		return $this->db
					->where('memberId',$this->session->userdata('admin_id'))
	  				->update('member_info',$data);
		
	}
	public function get_all_meeting_title()
	{
		$data=$this->db 
						->select('*')
						->from('event_info')
						->where('event_type_eventId',2)
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_sub_committee_by_detailsId($id)
	{
		$data=$this->db 
						->select('sub_committee_Info_subCommitteeInfoId')
						->from('sub_committee_details')
						->where('subCommitteeDetailsId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_inst_memberId_by_detailsId($id)
	{
		$data=$this->db 
						->select('installation_committee_Info_installationCommitteeInfoId')
						->from('installation_committee_details')
						->where('installationCommitteeDetailsId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function delete_sub_member($id)
	{
		return $this->db
						->where('subCommitteeDetailsId',$id)
						->delete('sub_committee_details');
						

	}
	public function delete_installation_member($id)
	{
		//print_r($id);exit;
		return $this->db
						->where('installationCommitteeDetailsId',$id)
						->delete('installation_committee_details');
						

	}	
	public function delete_event_member($id)
	{
		//print_r($id);exit;
		return $this->db
						->where('eventMemberInfoId',$id)
						->delete('event_member_info');
						

	}

}