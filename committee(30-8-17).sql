-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2017 at 02:54 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotary`
--

-- --------------------------------------------------------

--
-- Table structure for table `instalation_committee_info`
--

CREATE TABLE `instalation_committee_info` (
  `instalationCommitteeInfoId` int(100) NOT NULL,
  `instalationCommitteeTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `event_info_instalationCommitteeEventId` int(11) NOT NULL,
  `instalationCommitteeYear` date NOT NULL,
  `instalationCommitteeStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `instalationCommitteeAddedDate` datetime NOT NULL,
  `instalationCommitteeAddedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `installation_committee_details`
--

CREATE TABLE `installation_committee_details` (
  `installationCommitteeDetailsId` int(100) NOT NULL,
  `installation_committee_Info_installationCommitteeInfoId` int(11) NOT NULL,
  `member_info_installationCommitteeMemberId` int(11) NOT NULL,
  `installationCommitteeMemberPosition` varchar(400) COLLATE utf8_bin NOT NULL,
  `installationCommitteeMemberResponsibility` text COLLATE utf8_bin NOT NULL,
  `installationCommitteeMemberAddedDate` datetime NOT NULL,
  `installationCommitteeMemberStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `sub_committee_details`
--

CREATE TABLE `sub_committee_details` (
  `subCommitteeDetailsId` int(100) NOT NULL,
  `sub_committee_Info_subCommitteeInfoId` int(11) NOT NULL,
  `member_info_subCommitteeMemberId` int(11) NOT NULL,
  `subCommitteeMemberPosition` varchar(400) COLLATE utf8_bin NOT NULL,
  `subCommitteeMemberResponsibility` text COLLATE utf8_bin NOT NULL,
  `subCommitteeMemberAddedDate` datetime NOT NULL,
  `subCommitteeMemberStatus` int(2) NOT NULL COMMENT '1=Active, 0=InActive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `sub_committee_info`
--

CREATE TABLE `sub_committee_info` (
  `subCommitteeInfoId` int(100) NOT NULL,
  `subCommitteeTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `subCommitteeYear` date NOT NULL,
  `subCommitteeStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `subCommitteeAddedDate` datetime NOT NULL,
  `subCommitteeAddedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `instalation_committee_info`
--
ALTER TABLE `instalation_committee_info`
  ADD PRIMARY KEY (`instalationCommitteeInfoId`);

--
-- Indexes for table `installation_committee_details`
--
ALTER TABLE `installation_committee_details`
  ADD PRIMARY KEY (`installationCommitteeDetailsId`);

--
-- Indexes for table `sub_committee_details`
--
ALTER TABLE `sub_committee_details`
  ADD PRIMARY KEY (`subCommitteeDetailsId`);

--
-- Indexes for table `sub_committee_info`
--
ALTER TABLE `sub_committee_info`
  ADD PRIMARY KEY (`subCommitteeInfoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `instalation_committee_info`
--
ALTER TABLE `instalation_committee_info`
  MODIFY `instalationCommitteeInfoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `installation_committee_details`
--
ALTER TABLE `installation_committee_details`
  MODIFY `installationCommitteeDetailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_committee_details`
--
ALTER TABLE `sub_committee_details`
  MODIFY `subCommitteeDetailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_committee_info`
--
ALTER TABLE `sub_committee_info`
  MODIFY `subCommitteeInfoId` int(100) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
