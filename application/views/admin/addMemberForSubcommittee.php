<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add Sub Committee Member</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Sub Committee</a></li>
		  <li class="breadcrumb-item active">Add Sub Committee Member</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>
<?php ?>
<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	
				
	        	<div class="addMemberForEvent">
	        		<table class="table table-bordered">
					  <thead class="thead-inverse">
					    <tr>
					      <th>Name</th>
					      <th>ID</th>
					      <th>Position</th>
					      <th>Responsibility</th>
					      <th>Option</th>
					    </tr>
					  </thead>
					  <tbody>
					  <?php foreach($data as $value){ ?>
					  <?php $check=0; ?>
					    <tr>
					    		<?php if($value1){
					    		foreach ($value1 as  $info) {
					    		if($value->memberId==$info->member_info_subCommitteeMemberId) 
					    			{
					    				$check=5;
					    				?>
								      <?php echo form_open('admin/updateMemberForSubCommittee', 'class="addMember-form"')?>
								      <td><?php echo $value->memberName ?></td>
								      <td></td>
								      <td><input type="text"  requiredname="subCommitteeMemberPosition" value="<?php echo $info->subCommitteeMemberPosition; ?>" style="width:100%;" /></td>
								      <input type="hidden"   name="sub_committee_Info_subCommitteeInfoId" style="width:100%;" value="<?php echo $id  ?>" />
								      <input type="hidden" name="member_info_subCommitteeMemberId" style="width:100%;" value="<?php echo $value->memberId   ?>" />
								      <td><textarea required  name="subCommitteeMemberResponsibility" id="" cols="" rows="1" style="width:100%;"><?php echo $info->subCommitteeMemberResponsibility; ?></textarea></td>
								      <td> <button type="submit" class="btn btn-light committee-add"> <i class="fa fa-pencil-square" aria-hidden="true"></i></button>
									      <a href="<?php echo base_url("admin/subCommitteeDeleteMember/{$info->subCommitteeDetailsId}/{$info->sub_committee_Info_subCommitteeInfoId}"); ?>">
											<span class="btn btn-danger">
											  <i class="fa fa-trash" aria-hidden="true"></i>
											</span>
										  </a>
								      </td>
								    	<?php echo form_close() ?>
					    				<?php 
					    			}
					    		 } 
					    	 } 
					    	 if($check==0) { ?>
							      <?php  echo form_open('admin/storeMemberForSubCommittee', 'class="addMember-form"')?>
							      <td><?php echo $value->memberName ?></td>
							      <td></td>
							      <td><input type="text" required name="subCommitteeMemberPosition" style="width:100%;" /></td>
							      <td><textarea required name="subCommitteeMemberResponsibility" id="" cols="" rows="1" style="width:100%;"></textarea></td>
							      <input type="hidden" name="sub_committee_Info_subCommitteeInfoId" value="<?php echo $id  ?>"   />
							      <input type="hidden" name="member_info_subCommitteeMemberId" value="<?php echo $value->memberId   ?>" />
							      <td> <button type="submit" class="btn btn-light committee-add"><i class="fa fa-plus-square-o" aria-hidden="true"></i></button></td>
							      <?php echo form_close() ?>
							     <?php } ?>
					    </tr>
					    
					    <?php } ?>
					  </tbody>
					</table>
	        	</div>
        	<div class="row">
        		<div class="col-md-12 add-buttons">
        			<button type="submit" class="btn save"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
        			<a class="btn btn-light back" href="<?php echo base_url("admin/installationCommittee"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        	<?php// echo form_hidden('event_info_eventId',$infos{'id'});
        		  //echo form_hidden('perMemberCost',$infos{'cost'});
        	?>
        	
        </div>
    </div>
</div>

<?php include('footer.php');?>
