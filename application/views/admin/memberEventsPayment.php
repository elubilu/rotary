<?php include('header.php');?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Member Event Payment</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Member</a></li>
		  <li class="breadcrumb-item active">Member Event Payment</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/insertEventsPayment', 'class="addMember-form"')?>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						    <label for="member_info_memberId">Member Name</label>
						    <select name="member_info_memberId" class="form-control">	
                                <?php foreach ($data as $name) {
                                	
                                ?>
								<option value="<?php echo $name->memberId ?>"><?php echo $name->memberName ?></option>	
								<?php }?>
							</select>
					    	<div class="errorClass"><?php echo form_error('member_info_memberId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="event_info_eventId">Event Name</label>
						    <select name="event_info_eventId" class="form-control" value="">					
								<?php foreach ($value as $event) {
                                	
                                ?>
								<option value="<?php echo $event->event_infoId ?>"><?php echo $event->eventTitle ?></option>	
								<?php }?>
							</select>
					    	<div class="errorClass"><?php echo form_error('event_info_eventId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventPaidAmount">Amount</label>
						    <?php echo form_input(['name'=>'eventPaidAmount', 'class'=>'form-control', 'value'=>set_value('eventPaidAmount')]);?>
					    	<div class="errorClass"><?php echo form_error('eventPaidAmount'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="paymentPaidType">Payment Type</label>
						    <select name="paymentPaidType" class="form-control">					
								<option value="1">Full</option>	
								<option value="2">Partial</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('paymentPaidType'); ?></div>
						</div>
					</div>
				<!--	/* <div class="col-md-4">
						<div class="form-group">
						    <label for="payment_due">Due</label>
						    <?php //echo form_input(['name'=>'payment_due', 'class'=>'form-control', 'value'=>set_value('payment_due')]);?>
					    	<div class="errorClass"><?php //echo form_error('payment_due'); ?></div>
						</div>
					</div> */ -->
					<div class="col-md-4">
						<div class="form-group">
						    <label for="paymentDate">Date</label>
						    <?php echo form_input(['name'=>'paymentDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('paymentDate')]);?>
					    	<div class="errorClass"><?php echo form_error('paymentDate'); ?></div>
						</div>
					</div>
				</div>
        	
        	<div class="row">
        		<div class="col-md-12 add-buttons">
    			<?php// echo form_submit('', 'Add' , 'class="btn btn-light add"'); ?>
    			<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Submit</button>
    			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
    		</div>
        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
