<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->model('admin_model');
		if($this->session->userdata('user_role')!=1)return redirect('login/');
	}
	
	public function header(){
		$this->load->view('admin/header');
	}
	
	public function dashboard(){
		$this->load->view('admin/dashboard');
	}

  			/* Member Information */

	public function addMember(){
		$this->load->view('admin/addMember');
	}
 	public function insertMember()
	{
		
		$this->load->library('form_validation');
        if ($this->form_validation->run('signup_form') == FALSE){
            $this->load->view('admin/addMember');
        }
        else{
			$data=$this->input->post();
			$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
			$data['memberAddedDate']=$dt->format('Y-m-d H:i:s');
			$data['memberJoinDate']= date('Y-m-d', strtotime($data['memberJoinDate']));
			$data['Birthday']= date('Y-m-d', strtotime($data['Birthday']));
			$data['WeedingDate']= date('Y-m-d', strtotime($data['WeedingDate']));
			$data['SpouseBirthday']= date('Y-m-d', strtotime($data['SpouseBirthday']));
			if (($_FILES['memberImage']['name'])){
				$images = array();
				//echo "Hi"; //exit;
				
				if (isset($_FILES['memberImage'])&& $_FILES['memberImage']['size'] > 0) {
					$config['upload_path'] = './images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					//echo "no"; exit;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('memberImage')) {
						$this->session->set_flashdata('feedback_failed', 'Image upload failed');
						return redirect('admin/addMember');
					}
					else {
							$data1 = $this->upload->data(); //array('upload_data' => $this->upload->data());
							$image ="images/" . $data1['raw_name'] . $data1['file_ext'];
							$data['memberImage']=$image;
							//print_r($data); exit;
					}
				}
				unset($data['confirmPass']);
				if($this->admin_model->insertMember($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Member Successfully');
	            return redirect('admin/allMember');
				}
				else {
					$this->session->set_flashdata('feedback_failed', 'Added Member Failed!');
					return redirect('admin/addMember');
				}
			}
			
			else {
				//print_r($this->input->post()); exit;
				$this->session->set_flashdata('feedback_failed', 'Image Field Required!');
				 $this->load->view('admin/addMember');
			}
	    }
	}
	public function allMember(){
		$data=$this->admin_model->get_all_members();
		$this->load->view('admin/allMember',['data'=>$data]);
	}

	public function memberDetails($id){
		$data=$this->admin_model->get_details_by_Id($id);
		$this->load->view('admin/memberDetails',['data'=>$data]);
	}
   public function updateMember()
   {
   		$data=$this->input->post();
		
		//print_r($data);exit;

   		$this->load->library('form_validation');
        if ($this->form_validation->run('update_member') == FALSE){
        	/*echo "validation problem";
        	exit;*/
           $data=$this->admin_model->get_details_by_Id($data['memberId']);
			$this->load->view('admin/memberDetails',['data'=>$data]);
        }
        else{
			$data=$this->input->post();
			//unset($data['memberImage']);
			$images = array();
			if (isset($_FILES['memberImage'])&& $_FILES['memberImage']['size'] > 0) {
				$config['upload_path'] = './images/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				//echo "no"; exit;
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('memberImage')) {
					$this->session->set_flashdata('feedback_failed', 'Image upload failed');
					return redirect('admin/allMember');
				}
				else {
						$data1 = $this->upload->data(); //array('upload_data' => $this->upload->data());
						$image ="images/" . $data1['raw_name'] . $data1['file_ext'];
						$data['memberImage']=$image;
						//print_r($data); exit;
				}
			}
	   		if($this->admin_model->update_member_info($data)){
		   	$data=$this->admin_model->get_all_members();
		   	$this->session->set_flashdata('feedback_successfull', 'member updated successfully');
			$this->load->view('admin/allMember',['data'=>$data]);
			}
		}
	}
	public function editMember(){
        
		$this->load->view('admin/editMember');
	}

			/* Event Information */

	public function addEvents(){
		$this->load->view('admin/addEvents');
	}

	public function allEvents(){
		$data=$this->admin_model->get_all_events();
		$this->load->view('admin/allEvents',['data'=>$data]);
	}

	public function eventDetails($id){
		$data=$this->admin_model->get_event_details_by_Id($id);
		$this->load->view('admin/eventDetails',['value'=>$data]);
	}
    public function insertEvents()
	{
		$data=$this->input->post();
		

		$this->load->library('form_validation');
        if ($this->form_validation->run('insertEvents')==FALSE){
        	//print_r($data);exit();
            $this->load->view('admin/addEvents');
        }
        else{
			$data['eventDate']= date('Y-m-d', strtotime($data['eventDate']));
			$data['eventTime']= date('H:i:s', strtotime($data['eventTime']));
			$data['eventDate']= $data['eventDate']." ".$data['eventTime'];
			unset($data['eventTime']);
	        if($this->admin_model->insertEvents($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Event Successfully');
	            return redirect('admin/allEvents');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Added Event Failed!');
	            return redirect('admin/addEvents');
	        }
	    }
	}	
	public function updateEvents()
	{
		$data=$this->input->post();
		// print_r($data); exit();
		//$data['eventDate']= date('Y-m-d', strtotime($data['eventDate']));
        $data1=$this->admin_model->get_event_details_by_Id($data['event_infoId']);
		$this->load->library('form_validation');
        if ($this->form_validation->run('updateEvents')==FALSE){
        	//print_r($data);exit();
        	
            $this->load->view('admin/eventDetails',['value'=>$data1]);
        }
        else{
        	$data['eventDate']= date('Y-m-d', strtotime($data['eventDate']));
			$data['eventTime']= date('H:i:s', strtotime($data['eventTime']));
			$data['eventDate']= $data['eventDate']." ".$data['eventTime'];
			unset($data['eventTime']);
	        if($this->admin_model->updateEvents($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Updated Event Successfully');
	            return redirect('admin/allEvents');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Update Event Failed!');
	            return redirect('admin/eventDetails',['value'=>$data1]);
	        }
	    }
	}

	public function memberEventPaymentDetails($id){
		$data=$this->admin_model->get_members_event_payment_details($id);
		$this->load->view('admin/memberEventPaymentDetails',['data'=>$data]);
	}
	public function paymentHistory(){
		$data=$this->admin_model->get_all_members_event_payment_details();
		//print_r($data); exit();

		$this->load->view('admin/paymentHistory',['data'=>$data]);
	}
			/* Events Payment Information */

	public function memberEventsPayment(){

		$data1=$this->admin_model->get_all_members();
		$data2=$this->admin_model->get_all_events();
		$this->load->view('admin/memberEventsPayment',['data'=>$data1,'value'=>$data2]);
	}
    public function insertEventsPayment()
	{
		$data=$this->input->post();
		//print_r($data);exit();
		$data['paymentDate']= date('Y-m-d', strtotime($data['paymentDate']));
		$this->load->library('form_validation');
        if ($this->form_validation->run('EventsPayment') == FALSE){
            $data1=$this->admin_model->get_all_members();
			$data2=$this->admin_model->get_all_events();
			$this->load->view('admin/memberEventsPayment',['data'=>$data1,'value'=>$data2]);
        }
        else{
	        if($this->admin_model->insertEventsPayment($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Events Payment Successfully');
	            return redirect('admin/allEvents');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Events Payment Failed!');
	            return redirect('admin/memberEventsPayment');
	        }
	    }
	}
	
	public function addMemberForEvent($event_infoId){
		$data=$this->admin_model->get_all_members();
		$info['cost']=$this->admin_model->get_event_cost_by_event_ID($event_infoId);
		$info['id']=$event_infoId;
		$selected_member=$this->admin_model->selected_member_event_ID($event_infoId);
		//print_r($info);
		//exit();
		$this->load->view('admin/addMemberForEvent',['data'=>$data, 'infos'=>$info, 'selected'=>$selected_member,'event_id'=>$event_infoId]);
	}
	public function deleteEventMember($id,$event_id)
	{
		if($this->admin_model->delete_event_member($id)){
			
			redirect('admin/addMemberForEvent/'.$event_id);
		}
	}
	public function storeMemberForEvent(){
		$data=$this->input->post();
			if($data['memberList']){
			if($this->admin_model->store_member_for_event($data)){
				$this->session->set_flashdata('feedback_successfull', 'Member For Event Successfully Added');
				return redirect('admin/allEvents');
			}
			else{
				$this->session->set_flashdata('feedback_failed', 'Member Add For Event Failed!');
				return redirect('admin/allEvents');
			}
		}
		else{
				$this->session->set_flashdata('feedback_failed', 'Please Select Member For Add');
				return redirect('admin/allEvents');
			}

		//$info=$this->admin_model->selected_member_event_ID($event_infoId);
		
	}
	 /* Committee Information*/
	public function createCommittee(){
		$this->load->view('admin/createCommittee');
	}

	public function subCommittee(){
		$data=$this->admin_model->get_all_sub_committee_members();
		//$data['subCommitteeYear']= date('Y/m/d', strtotime($data['subCommitteeYear']));
		$this->load->view('admin/subCommittee',['data'=>$data]);
	}

	public function installationCommittee(){
		$data=$this->admin_model->get_all_instalation_committee_members();
		$this->load->view('admin/installationCommittee',['data'=>$data]);
	}

	public function addSubCommittee(){
		$this->load->view('admin/addSubCommittee');
	}

	public function subCommitteeDetails($id){
	   $data=$this->admin_model->get_sub_committee_info_by_id($id);
	   //print_r($data);exit();
	   //$data['subCommitteeYear']= date('%Y/%m/%d', strtotime($data['subCommitteeYear']));
		$this->load->view('admin/subCommitteeDetails',['data'=>$data]);
	}
	public function updateSubCommitteeDetails(){
		$value=$this->input->POST();
	      if($this->admin_model->update_sub_committee_info($value)){
		    $data=$this->admin_model->get_all_sub_committee_members();
		   
			$this->load->view('admin/subCommittee',['data'=>$data]);
		}
	}

	public function addMemberForSubcommittee($id){
		$data=$this->admin_model->get_all_members_name_id();
		$value=$this->admin_model->get_responsibility_sub($id);
		$this->load->view('admin/addMemberForSubcommittee',['data'=>$data,'id'=>$id, 'value1'=>$value]);
	}
	
	public function addInstallationCommittee(){
		$this->load->view('admin/addInstallationCommittee');
	}

	public function installationCommitteeDetails($id){
		$data=$this->admin_model->get_instalation_committee_info_by_id($id);
		$this->load->view('admin/installationCommitteeDetails',['data'=>$data]);
	}

	public function addMemberForInstallationcommittee($id){
		//print_r($id); exit();
		$data=$this->admin_model->get_all_members_name_id();
		$value=$this->admin_model->get_responsibility_insta($id);
		//print_r($value); exit();
		$this->load->view('admin/addMemberForInstallationcommittee',['data'=>$data,'id'=>$id,'value1'=>$value]);
	}
	public function updateInstallationCommitteeDetails(){
		$value=$this->input->POST();
		//print_r($value); exit();
	      if($this->admin_model->update_Installation_committee_info($value)){
		    $data=$this->admin_model->get_all_instalation_committee_members();
		   
			$this->load->view('admin/installationCommittee',['data'=>$data]);
		}
	}

	public function insertCommitteeMember()
	{
		$data=$this->input->post();
		$data['committee_member_added_date']=date('Y-m-d');
		
		$this->load->library('form_validation');
        if ($this->form_validation->run('createCommittee') == FALSE){
            $this->load->view('admin/createCommittee');
        }
        else{
	        if($this->admin_model->insertComitteeMember($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Committee Member Successfully');
	            return redirect('admin/allMember');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Added Committee Member Failed!');
	            return redirect('admin/createCommittee');
	        }
	    }
	}
	public function insertSubCommitteeMember()
	{
		$data=$this->input->post();
		$data['subCommitteeAddedDate']=date('Y-m-d');
		$data['subCommitteeYear']= date('Y-m-d', strtotime($data['subCommitteeYear']));
		$this->load->library('form_validation');
        if ($this->form_validation->run('addSubCommittee') == FALSE){
            $this->load->view('admin/addSubCommittee');
        }
        else{
	        if($this->admin_model->insert_sub_committee($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Sub Committee Member Successfully');
	            return redirect('admin/subCommittee');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Added Sub Committee Member Failed!');
	            return redirect('admin/addSubCommittee');
	        }
	    }
	}
	public function insertInstalationCommitteeMember()
	{
		$data=$this->input->post();
		$data['instalationCommitteeAddedDate']=date('Y-m-d');
		$data['instalationCommitteeYear']= date('Y-m-d', strtotime($data['instalationCommitteeYear']));
		$this->load->library('form_validation');
        if ($this->form_validation->run('addInstalationCommittee') == FALSE){
            $this->load->view('admin/addInstallationCommittee');
        }
        else{
	        if($this->admin_model->insert_instalation_committee($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Instalation Committee Member Successfully');
	            return redirect('admin/installationCommittee');
	        }
	        else {
	            $this->session->set_flashdata('feedback_failed', 'Added Instalation Committee Member Failed!');
	            return redirect('admin/addInstallationCommittee');
	        }
	    }
	}

			/* Meeting Attendance Information*/

	public function attendanceSheet(){
		$data=$this->admin_model->get_all_members();
		$data1=$this->admin_model->get_all_meeting_title();
		$this->load->view('admin/attendanceSheet',['data'=>$data,'data1'=>$data1]);
	}
	public function attendanceDetails($id){
		$data1=$this->admin_model->get_attendance_info_by_Id($id);
		$data=$this->admin_model->get_attendance_details_by_Id($id);

		//print_r($data); exit();
		$check=array();
		foreach ($data as  $value) {
			$check[]=$this->admin_model->get_details_by_Id($value->member_info_memberId);
		}
	//	print_r($check); exit();
		
		$this->load->view('admin/attendanceDetails',['data'=>$check,'data1'=>$data1]);
	}
	public function allAttendance(){
		$data=$this->admin_model->get_all_meeting_attendance();
		//print_r($data); exit();
		//$data1=$this->admin_model->get_all_meeting_title();
		$this->load->view('admin/allAttendance',['data'=>$data]);
	}

	public function insertMeetingInfo()
	{
		$data=$this->input->post();

		//$data['committee_member_added_date']=date('Y-m-d');
		//print_r(); exit();
		$data['meetingDate']= date('Y-m-d', strtotime($data['meetingDate']));
		//date_default_timezone_set('Asia/Dhaka'); 

		//$data['time']=time();
        date_default_timezone_set('Asia/Dhaka');
        $Time=time();
         $data['meetingDate']+=$Time;
        //print_r($members); exit();
		$this->load->library('form_validation');
		
	
        if ($this->form_validation->run('attendanceSheet') == FALSE){
        	$value=$this->admin_model->get_all_members();
			$data1=$this->admin_model->get_all_meeting_title();
            $this->load->view('admin/attendanceSheet',['data'=>$value,'data1'=>$data1]);
        }
        else{
        	if($data['members']){

	        if($this->admin_model->insertMeetingInfo($data)){
	            $this->session->set_flashdata('feedback_successfull', 'Added Member Attendance Successfully');
	            return redirect('admin/attendanceSheet',['data'=>$value,'data1'=>$data1]);
	        }
	        else {
	        	$value=$this->admin_model->get_all_members();
				$data1=$this->admin_model->get_all_meeting_title();
	            $this->session->set_flashdata('feedback_failed', 'Added Member Attendance Failed!');
	            return redirect('admin/attendanceSheet',['data'=>$value,'data1'=>$data1]);
	        }
	      }
	       else {
	        	$value=$this->admin_model->get_all_members();
				$data1=$this->admin_model->get_all_meeting_title();
	            $this->session->set_flashdata('feedback_failed', 'Added Member Attendance Failed!');
	            return redirect('admin/attendanceSheet',['data'=>$value,'data1'=>$data1]);
	        }

	    }
	}
	/* News Info */
	public function addNews(){
		$this->load->view('admin/addNews');
	}
	public function insertNews(){
		$images = array();
		if (($_FILES['news_pic']['name'])){
			$data = $this->input->post();
			$filesCount = count($_FILES['news_pic']['name']);
			//if($filesCount>5){ $filesCount=5; }
			for($i = 0; $i< $filesCount; $i++){
				$_FILES['userFile']['name'] = $_FILES['news_pic']['name'][$i];
				$_FILES['userFile']['type'] = $_FILES['news_pic']['type'][$i];
				$_FILES['userFile']['tmp_name'] = $_FILES['news_pic']['tmp_name'][$i];
				$_FILES['userFile']['error'] = $_FILES['news_pic']['error'][$i];
				$_FILES['userFile']['size'] = $_FILES['news_pic']['size'][$i];

				$config['upload_path'] ='./images/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('userFile')){
					$data1 = $this->upload->data();
					$image ="images/" . $data1['raw_name'] . $data1['file_ext'];
					$j=$i+1;
					$images[$j]=$image;
				}
				else{
					//$this->session->set_flashdata('feedback_failed', 'Image upload failed');
					//return redirect('admin/addNews');
				}
			}	
		}
		$data=$this->input->post();
		//print_r($images); exit();
		$this->load->library('form_validation');
		$data['newsDate']= date('Y-m-d', strtotime($data['newsDate']));

	    if ($this->form_validation->run('insertNews') == FALSE){
	    	
            $this->load->view('admin/addNews');
        }
        else{
			if (($_FILES['newsImage']['name'])){
				if (isset($_FILES['newsImage'])&& $_FILES['newsImage']['size'] > 0) {
					$config['upload_path'] = './images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					//echo "no"; exit;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('newsImage')) {
						$this->session->set_flashdata('feedback_failed', 'Image upload failed');
						return redirect('admin/addMember');
					}
					else {
						$data1 = $this->upload->data(); //array('upload_data' => $this->upload->data());
						$image ="images/" . $data1['raw_name'] . $data1['file_ext'];
						$data['newsMainImage']=$image;
						//print_r($data); exit;
					}
				}

				if($this->admin_model->insert_news($data,$images)){
				$this->session->set_flashdata('feedback_successfull', 'Added News Successfully');
				return redirect('admin/allNews');
				}
				else {
					$this->session->set_flashdata('feedback_failed', 'Added News Failed!');
					return redirect('admin/addNews');
				}
			}
			
			else{
				//print_r($this->input->post()); exit;
				$this->session->set_flashdata('feedback_failed', 'Image Field Required!');
				 $this->load->view('admin/addNews');
			}
		
	     }
	}

	public function allNews(){
		$data=$this->admin_model->get_all_news();
		$this->load->view('admin/allNews',['data'=>$data]);
	}
	public function subCommitteeDeleteMember($id,$committee_id){
		if($this->admin_model->delete_sub_member($id)){

			//$check=$this->admin_model->get_sub_committee_by_detailsId($id);
			//print_r($check); exit();
			redirect('admin/addMemberForSubcommittee/'.$committee_id);
		}
		
	} 
	public function installationCommitteeDeleteMember($id,$committee_id){
		//print_r($committee_id); exit;
		if($this->admin_model->delete_installation_member($id)){
			
			redirect('admin/addMemberForInstallationcommittee/'.$committee_id);
		}
		
	}

	public function newsDetails($id){
		$data=$this->admin_model->get_news_info_by_id($id);
		$this->load->view('admin/newsDetails',['value'=>$data]);
	}

	public function updateNews(){
		$value=$this->input->post();
       if($this->admin_model->update_news($value) ){
			$data=$this->admin_model->get_all_news();
			$this->load->view('admin/allNews',['data'=>$data]);
	 }

	}
	public function updateMemberForSubCommittee(){
		$value=$this->input->post();
       if($this->admin_model->update_sub_committee_member($value) ){
			$check=$value['sub_committee_Info_subCommitteeInfoId'];
	      	//print_r($check); exit();
		    redirect('admin/addMemberForSubcommittee/'.$check);
	 }
	}
	public function updateMemberForInstallationCommittee(){
		$value=$this->input->post();
       if($this->admin_model->update_ins_committee_member($value) ){
			$check=$value['installation_committee_Info_installationCommitteeInfoId'];
	      	//print_r($check); exit();
		    redirect('admin/addMemberForInstallationcommittee/'.$check);
	 }

	}

	public function messages(){
		$this->load->view('admin/messages');
	}

	public function notification(){
		$start = date('Y-m-d', strtotime('+0 days'));
		$end = date('Y-m-d', strtotime('+3 days'));
		//$=date('+5 days',strtotime($start));
		//print_r($end); exit();
		$memberBirthDate=$this->admin_model->get_members_birthday($start,$end);
		$memberWeedingDate=$this->admin_model->get_members_weeding($start,$end);
		$spouseBirthDate=$this->admin_model->get_spouse_birthday($start,$end);
		//print_r()

		$this->load->view('admin/notification',['memberBirthDate'=>$memberBirthDate,'memberWeedingDate'=>$memberWeedingDate,'spouseBirthDate'=>$spouseBirthDate]);
	}

	public function gallery(){
		$this->load->view('admin/gallery');
	}
	
	public function blankPage(){
		$this->load->view('admin/blankPage');
	}
	
	public function logInPage(){
		$this->load->view('admin/logInPage');
	}

	public function profile(){
		$memberId=$this->session->userdata('admin_id');
		$data=$this->admin_model->get_details_by_Id($memberId);
		/*
		//print_r($data->memberTypeId); exit();
		$id=$data->memberLoginTypeId;
		//print_r($id); exit();
		$data->memberLoginTypeTitle=$this->admin_model->get_position_name_by_id($id);*/
		$this->load->view('admin/profile',['value'=>$data]);
	}
	
	public function updateProfile()
	{
		$memberId=$this->session->userdata('admin_id');
		$this->load->library('form_validation');
        if ($this->form_validation->run('changePassword') == FALSE){
        	
			$data1=$this->admin_model->get_details_by_Id($memberId);
            $this->load->view('admin/profile',['value'=>$data1]);
        }
        else{

			$data=$this->input->post();
			//print_r($data); exit();
			//$id=$data['memberId'];
			$value=$this->admin_model->get_details_by_Id($memberId);
			
        	if($value->memberPassword==$data['memberPassword'])
        	{
        		$info['memberPassword']=$data['newPassword'];
        		
        		if($this->admin_model->update_password($info)){
        			$this->session->set_flashdata('feedback_successfull', 'You are Successfully Changed Your Password.');
	        		redirect('login/logout');
	        	}
        	}
        	else 
        	{
        		$this->session->set_flashdata('feedback_failed', 'Old Password is Wrong!!');
        		$this->load->view('admin/profile',['value'=>$value]);
        	}
		   
		}
	}
	public function storeMemberForSubCommittee()
	{
		$value=$this->input->post();
		$value['subCommitteeMemberAddedDate']=date('Y-m-d');
		$value['subCommitteeMemberStatus']=1;
		//print_r($value); exit();
	      if($this->admin_model->store_sub_committee_responsibility($value)){
	      	$check=$value['sub_committee_Info_subCommitteeInfoId'];
	      	//print_r($check); exit();
		    redirect('admin/addMemberForSubcommittee/'.$check);
			//$this->load->view(,['data'=>$data]);['data'=>$value->sub_committee_Info_subCommitteeInfoId]
		}
	}	
	public function storeMemberForInstallationCommittee()
	{
		$value=$this->input->post();
		$value['installationCommitteeMemberAddedDate']=date('Y-m-d');
		$value['installationCommitteeMemberStatus']=1;
		
	      if($this->admin_model->store_installation_committee_responsibility($value)){
	      	$check=$value['installation_committee_Info_installationCommitteeInfoId'];
	      	//print_r($check); exit();
		    redirect('admin/addMemberForInstallationcommittee/'.$check);
			//$this->load->view(,['data'=>$data]);['data'=>$value->sub_committee_Info_subCommitteeInfoId]
		}
	}

	public function sendMail(){

		$data = $this->input->post();
		print_r($data);

		$data=$this->input->post();

	    // print_r($data); exit();

		$message="";
		$subject = "";

  	$subject = "Deshi Car Hire: ". $data['subject'];
  	$message="";
  	$message = $message ."Full name: " . $data['name'];
  	$message = $message ."\n Contact Number: " . $data['mobile'];
  	$message = $message ."\n Email: " . $data['email'];
  	$message = $message ."\n Details: \n" . $data['message'];

		// echo $message." ".$subject;

    $this->load->library('email');

    $this->email->from('test@starlabit.top', $data['name']);
    $this->email->to('starlabteam@gmail.com');

		// echo $subject."\n".$message;
		// exit();
		$this->email->subject($subject);
    $this->email->message($message);

		if($this->email->send()){
			$this->session->set_flashdata('feedback_successfull', 'Message Send Successful');
		}
		else{
			$this->session->set_flashdata('feedback_failed', 'Please Try Again');
		}

	    redirect($this->agent->referrer());
	}
	
}
