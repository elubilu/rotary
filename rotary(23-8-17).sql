-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2017 at 01:49 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotary`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_info`
--

CREATE TABLE `attendance_info` (
  `attendance_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_details`
--

CREATE TABLE `committee_details` (
  `committee_detailsId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_info`
--

CREATE TABLE `committee_info` (
  `committee_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `donation_info`
--

CREATE TABLE `donation_info` (
  `donation_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_info`
--

CREATE TABLE `event_info` (
  `event_infoId` int(11) NOT NULL,
  `eventTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `eventDetails` text COLLATE utf8_bin NOT NULL,
  `event_type_eventId` int(11) NOT NULL,
  `eventDate` datetime NOT NULL,
  `eventPerHeadAmount` double NOT NULL,
  `eventStatua` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `eventAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_payment_info`
--

CREATE TABLE `event_payment_info` (
  `event_payment_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `eventTypeId` int(100) NOT NULL,
  `eventTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `logo_info`
--

CREATE TABLE `logo_info` (
  `logo_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE `member_info` (
  `memberId` int(100) NOT NULL,
  `memberName` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberNumber` varchar(100) COLLATE utf8_bin NOT NULL,
  `clubId` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberJoinDate` date NOT NULL,
  `memberImage` varchar(750) COLLATE utf8_bin NOT NULL,
  `Position` varchar(100) COLLATE utf8_bin NOT NULL,
  `BloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `WeedingDate` date NOT NULL,
  `Birthday` date NOT NULL,
  `SpouseName` varchar(100) COLLATE utf8_bin NOT NULL,
  `SpouseBirthday` date NOT NULL,
  `SpouseBloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddress` text COLLATE utf8_bin NOT NULL,
  `memberMobile` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberEmail` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberPassword` varchar(100) COLLATE utf8_bin NOT NULL,
  `Status` int(100) NOT NULL,
  `memberTypeId` int(100) NOT NULL,
  `memberGender` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memberLoginTypeId` int(100) NOT NULL,
  `memberTotalBalance` double NOT NULL,
  `memberDueAmount` double NOT NULL,
  `memberEditDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`memberId`, `memberName`, `memberNumber`, `clubId`, `memberJoinDate`, `memberImage`, `Position`, `BloodGroup`, `WeedingDate`, `Birthday`, `SpouseName`, `SpouseBirthday`, `SpouseBloodGroup`, `memberAddress`, `memberMobile`, `memberEmail`, `memberPassword`, `Status`, `memberTypeId`, `memberGender`, `memberAddedDate`, `memberLoginTypeId`, `memberTotalBalance`, `memberDueAmount`, `memberEditDate`) VALUES
(1, 'Kshoma Kanta Chakrabartty', '12345', 'Accounting Management', '2007-12-01', 'images/031.jpg', 'President', 'B+', '1986-02-16', '1955-02-12', 'Ratna Rani Chakrabartty', '1959-05-05', 'O+', 'Mohona 104, Block#B, Korerpara, Sylhet.', '01720147149', 'rtn.kshoma@gmail.com', '25d55ad283aa400af464c76d713c07ad', 0, 1, 'Male', '2017-08-23 15:25:38', 1, 1000, 0, '2017-08-23 09:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `member_login_type`
--

CREATE TABLE `member_login_type` (
  `memberLoginTypeId` int(100) NOT NULL,
  `memberLoginTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_login_type`
--

INSERT INTO `member_login_type` (`memberLoginTypeId`, `memberLoginTypeTitle`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `member_payment_info`
--

CREATE TABLE `member_payment_info` (
  `member_payment_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_type`
--

CREATE TABLE `member_type` (
  `memberTypeId` int(100) NOT NULL,
  `memberTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_info`
--

CREATE TABLE `news_info` (
  `news_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_info`
--
ALTER TABLE `attendance_info`
  ADD PRIMARY KEY (`attendance_infoId`);

--
-- Indexes for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD PRIMARY KEY (`committee_detailsId`);

--
-- Indexes for table `committee_info`
--
ALTER TABLE `committee_info`
  ADD PRIMARY KEY (`committee_infoId`);

--
-- Indexes for table `donation_info`
--
ALTER TABLE `donation_info`
  ADD PRIMARY KEY (`donation_infoId`);

--
-- Indexes for table `event_info`
--
ALTER TABLE `event_info`
  ADD PRIMARY KEY (`event_infoId`);

--
-- Indexes for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  ADD PRIMARY KEY (`event_payment_infoId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`eventTypeId`);

--
-- Indexes for table `logo_info`
--
ALTER TABLE `logo_info`
  ADD PRIMARY KEY (`logo_infoId`);

--
-- Indexes for table `member_info`
--
ALTER TABLE `member_info`
  ADD PRIMARY KEY (`memberId`),
  ADD UNIQUE KEY `memberEmail` (`memberEmail`),
  ADD UNIQUE KEY `memberNumber` (`memberNumber`);

--
-- Indexes for table `member_login_type`
--
ALTER TABLE `member_login_type`
  ADD PRIMARY KEY (`memberLoginTypeId`);

--
-- Indexes for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  ADD PRIMARY KEY (`member_payment_infoId`);

--
-- Indexes for table `member_type`
--
ALTER TABLE `member_type`
  ADD PRIMARY KEY (`memberTypeId`);

--
-- Indexes for table `news_info`
--
ALTER TABLE `news_info`
  ADD PRIMARY KEY (`news_infoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_info`
--
ALTER TABLE `attendance_info`
  MODIFY `attendance_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_details`
--
ALTER TABLE `committee_details`
  MODIFY `committee_detailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_info`
--
ALTER TABLE `committee_info`
  MODIFY `committee_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donation_info`
--
ALTER TABLE `donation_info`
  MODIFY `donation_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_info`
--
ALTER TABLE `event_info`
  MODIFY `event_infoId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  MODIFY `event_payment_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `eventTypeId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logo_info`
--
ALTER TABLE `logo_info`
  MODIFY `logo_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_info`
--
ALTER TABLE `member_info`
  MODIFY `memberId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_login_type`
--
ALTER TABLE `member_login_type`
  MODIFY `memberLoginTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  MODIFY `member_payment_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_type`
--
ALTER TABLE `member_type`
  MODIFY `memberTypeId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_info`
--
ALTER TABLE `news_info`
  MODIFY `news_infoId` int(100) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
