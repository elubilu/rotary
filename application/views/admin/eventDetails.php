<?php include('header.php');?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Event Details</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Events</a></li>
		  <li class="breadcrumb-item active">Event Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/updateEvents', 'class="addEvents-form"')?>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						    <label for="event_type_eventId">Event Type</label>
						    <select name="event_type_eventId" class="form-control removeDis" disabled required>			
								<option value="<?php echo $value->event_type_eventId ?>" selected="true" disabled="disabled"><?php if($value->event_type_eventId == 1) {echo 'Program';} else {echo "Meeting";}; ?></option>	
								<option value="1">Program</option>	
								<option value="2">Meeting</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('event_type_eventId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventDate">Event Date</label>
						    <?php $value->eventDate=date('Y-m-d', strtotime($value->eventDate));?>
						    <?php echo form_input(['name'=>'eventDate', 'class'=>'form-control removeDis datepicker-here', 'data-position'=>'bottom left', 'disabled'=>'disabled', 'data-language'=>'en', 'value'=>set_value('').$value->eventDate]);?>
					    	<div class="errorClass"><?php echo form_error('eventDate'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group bootstrap-timepicker timepicker">
						    <label for="eventTime">Event Time</label>
						    <?php $value->eventDate=date('h:iA', strtotime($value->eventDate));?>
						    <?php echo form_input(['name'=>'eventDate', 'id'=>'timepicker1', 'type'=>'text', 'class'=>'form-control input-small removeDis', 'disabled'=>'disabled', 'value'=>set_value('eventDate')]);?>
					    	<div class="errorClass"><?php echo form_error('eventDate'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="eventTitle">Event Title</label>
						    <?php echo form_input(['name'=>'eventTitle', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'value'=>set_value('').$value->eventTitle]);?>
					    	<div class="errorClass"><?php echo form_error('eventTitle'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="eventDetails">Event Details</label>
						    <?php echo form_textarea(['name'=>'eventDetails', 'class'=>'form-control removeDis',  'disabled'=>'disabled', 'rows'=>'3', 'value'=>set_value('').$value->eventDetails]);?>
					    	<div class="errorClass"><?php echo form_error('eventDetails'); ?></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventStatua">Event Status</label>
						    <select name="eventStatua" class="form-control removeDis" disabled required>				
								<option value="<?php echo $value->eventStatua ?>" selected disabled><?php if($value->eventStatua == 1) {echo 'Active';} else {echo "Inactive";}; ?></option>	
								<option value="1">Active</option>	
								<option value="2">Inactive</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('eventStatua'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventPlace">Event Place</label>
						    <?php echo form_input(['name'=>'eventPlace', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'value'=>set_value('').$value->eventPlace]);?>
					    	<div class="errorClass"><?php echo form_error('eventPlace'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventPerHeadAmount">Event Cost (Per Head)</label>
						    <?php echo form_input(['name'=>'eventPerHeadAmount', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'value'=>set_value('').$value->eventPerHeadAmount]);?>
					    	<div class="errorClass"><?php echo form_error('eventPerHeadAmount'); ?></div>
						</div>
					</div>
				</div>
        	<?php echo form_input(['name'=>'event_infoId', 'class'=>'form-control removeDis', 'type'=>'hidden','disabled'=>'disabled', 'value'=>set_value('').$value->event_infoId]);?>
        	
        	<div class="row">
        		<div class="col-md-12 edit-buttons">
					<a type="button" class="btn btn-light edit" id="removeDisabled"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

					<button type="submit" class="btn save hidden hiddenButton"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
					
					<a class="btn cancel hidden hiddenButton" id="addDisabled" ><i class="fa fa-reply-all" aria-hidden="true"></i> Cancel</a>
					
					<a class="btn btn-light back" href="<?php echo base_url("admin/allEvents"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
        		</div>
        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
