<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Sub Committee</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Committee</a></li>
		  <li class="breadcrumb-item active">Sub Committee</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
		<div class="jumborton">
			<div class="row pull-right">
				<a class="btn btn-primary create m-b-10" href="<?php echo base_url("admin/addSubCommittee"); ?>" role="button">Add Committee</a>
			</div>
			<div class="row">
				<table class="table table-bordered table-striped">
					<thead class="thead-inverse">
						<tr>
							<th>#</th>
							<th>COMMITTEE TITLE</th>	
							<th>MEMBER NAME</th>	
							<th>RESPONSIBILITY</th>		
							<th>YEAR</th>		
							<th>OPTIONS</th>		
						</tr>
					</thead>
					
					<tbody>
						<?php foreach($data as $value){ ?>
					<tr>
						<td></td>
						<td><?php echo $value->subCommitteeTitle ?></td>
						<td><?php  ?></td> 
						<td><?php  ?></td>
						<td><?php echo date('M-d, Y',strtotime($value->subCommitteeYear)); ?></td>
					
							<td class="option">
								<a href="<?php echo base_url("admin/subCommitteeDetails/{$value->subCommitteeInfoId}"); ?>" title="Details">
									<span class="fa-stack fa-lg">
									  <i class="fa fa-circle fa-stack-2x"></i>
									  <i class="fa fa-info fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="<?php echo base_url("admin/addMemberForSubcommittee/{$value->subCommitteeInfoId}"); ?>" title="Add Member">
									<span class="fa-stack fa-lg">
									  <i class="fa fa-circle fa-stack-2x"></i>
									  <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</td> 
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php include('footer.php');?>
