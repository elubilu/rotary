 <?php  include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Attendance Sheet</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item active">Attendance Sheet</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<div class="row">
				<div class="col-md-6">
						<div class="form-group">
						    <h1><label for="eventTitle">Meeting Title:</label></h1>
						     <h2 class=""><?php echo $data1->eventTitle; ?></h2>
						</div>					
				</div>
				<div class="col-md-6">
						<div class="form-group">
						    <h1><label for="eventTitle">Meeting Date:</label></h1>
						     <h2><?php echo date('M-d, Y  h:i a',strtotime($data1->meetingDate)); ?></h2>
						</div>
					</div>					
				</div>
        	<?php if($data){ ?>		
        	<div class="attendenceTable">
        		<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>#</th>
				      <th>Name</th>
				      <th>ID</th>
				      <th>Position</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($data as $value){ ?>
				    <tr>
				     <td></td>
				      <td><?php echo $value->memberName ?></td>
				      <td><?php echo $value->clubId ?></td>
				      <td><?php echo $value->Position ?></td>
				    </tr>
				    <?php } ?>
				    
				  </tbody>
				</table>
        	</div>
        	<?php } ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
