<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Add Events</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Events</a></li>
		  <li class="breadcrumb-item active">Add Events</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/insertEvents', 'class="addEvents-form"')?>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
						    <label for="event_type_eventId">Event Type</label>
						    <select name="event_type_eventId" class="form-control">		
								<option value="1">Program</option>	
								<option value="2">Meeting</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('event_type_eventId'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="eventDate">Event Date</label>
						    <?php echo form_input(['name'=>'eventDate', 'class'=>'form-control datepicker-here', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('eventDate')]);?>
					    	<div class="errorClass"><?php echo form_error('eventDate'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group bootstrap-timepicker timepicker">
						    <label for="eventTime">Event Time</label>
						    <?php echo form_input(['name'=>'eventTime', 'id'=>'timepicker1', 'type'=>'text', 'class'=>'form-control input-small', 'value'=>set_value('eventTime')]);?>
					    	<div class="errorClass"><?php echo form_error('eventTime'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="eventTitle">Event Title</label>
						    <?php echo form_input(['name'=>'eventTitle', 'class'=>'form-control', 'value'=>set_value('eventTitle')]);?>
					    	<div class="errorClass"><?php echo form_error('eventTitle'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="eventDetails">Event Details</label>
						    <?php echo form_textarea(['name'=>'eventDetails', 'class'=>'form-control', 'rows'=>'3', 'value'=>set_value('eventDetails')]);?>
					    	<div class="errorClass"><?php echo form_error('eventDetails'); ?></div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
						    <label for="eventPlace">Event Place</label>
						    <?php echo form_input(['name'=>'eventPlace', 'class'=>'form-control', 'value'=>set_value('eventPlace')]);?>
					    	<div class="errorClass"><?php echo form_error('eventPlace'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="eventPerHeadAmount">Event Cost (Per Head)</label>
						    <?php echo form_input(['name'=>'eventPerHeadAmount', 'class'=>'form-control', 'value'=>set_value('eventPerHeadAmount')]);?>
					    	<div class="errorClass"><?php echo form_error('eventPerHeadAmount'); ?></div>
						</div>
					</div>
				</div>
        	
	        	<div class="row">
	        		<div class="col-md-12 add-buttons">
	        			<button type="submit" class="btn btn-light add"><i class="fa fa-check-square-o" aria-hidden="true"></i> Add</button>
	        			<button type="reset" class="btn btn-light reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
        </div>
    </div>
</div>

<?php include('footer.php');?>
