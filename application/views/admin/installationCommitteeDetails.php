<?php include('header.php');?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Installation Committee Details</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item"><a href="#">Home</a></li>
		  <li class="breadcrumb-item"><a href="#">Committee</a></li>
		  <li class="breadcrumb-item active">Installation Committee Details</li>
		</ol>
	</div>
</div>

<?php include('messages.php');?>

<div class="row">
	<div class="col-md-12">
        <div class="jumborton">
        	<?php echo form_open('admin/updateInstallationCommitteeDetails', 'class="addSubCommittee-form"')?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						    <label for="instalationCommitteeTitle">Installation Committee Title</label>
						    <?php echo form_input(['name'=>'instalationCommitteeTitle', 'class'=>'form-control removeDis', 
						    'disabled'=>'disabled', 'value'=>set_value('instalationCommitteeTitle').$data->instalationCommitteeTitle]);?>
					    	<div class="errorClass"><?php echo form_error('instalationCommitteeTitle'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="committee_member_type">Select Member For Committee</label>
						    <select name="committee_member_type" class="form-control removeDis" disabled required>					
								<option value="1">Member 1</option>	
								<option value="2">Member 2</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('committee_member_type'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="instalationCommitteeYear">Year</label>
						    <?php echo form_input(['name'=>'instalationCommitteeYear', 'class'=>'form-control datepicker-here removeDis',
						     'disabled'=>'disabled', 'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('instalationCommitteeYear').$data->instalationCommitteeYear]);?>
					    	<div class="errorClass"><?php echo form_error('instalationCommitteeYear'); ?></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
						    <label for="event_info_instalationCommitteeEventId">Event</label>
						    <select name="event_info_instalationCommitteeEventId" class="form-control removeDis" disabled required>		
								<option value="1">Program</option>	
								<option value="2">Meeting</option>	
							</select>
					    	<div class="errorClass"><?php echo form_error('event_info_instalationCommitteeEventId'); ?></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="committee_member_responsibility">Responsibility</label>
						    <?php echo form_textarea(['name'=>'committee_member_responsibility', 'class'=>'form-control removeDis', 'disabled'=>'disabled', 'rows'=>'3', 'value'=>set_value('address')]);?>
					    	<div class="errorClass"><?php echo form_error('committee_member_responsibility'); ?></div>
						</div>
					</div>
				</div>	
				 <?php echo form_input(['name'=>'instalationCommitteeInfoId', 'type'=>'hidden', 'class'=>'form-control datepicker-here removeDis', 'disabled'=>'disabled', 
						    'data-position'=>'bottom left', 'data-language'=>'en', 'value'=>set_value('instalationCommitteeInfoId').$data->instalationCommitteeInfoId]);?>
	        	<div class="row">
	        		<div class="col-md-12 edit-buttons">
						<a type="button" class="btn btn-light edit" id="removeDisabled"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

						<button type="submit" class="btn save hidden hiddenButton"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>

						<a class="btn cancel hidden hiddenButton" id="addDisabled" ><i class="fa fa-reply-all" aria-hidden="true"></i>  Cancel</a>
						
						<a class="btn btn-light back" href="<?php echo base_url("admin/installationCommittee"); ?>" role="button"><i class="fa fa-undo" aria-hidden="true"></i> Back</a>
	        		</div>
	        	</div>
        	<?php echo form_close() ?>
    	</div>
	</div>
</div>
<?php include('footer.php');?>
