﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	
    <title>rotary</title>
	
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/bootstrap.min.css');?>">
	
	
    <!-- bootsnav -->
    <link href="<?php echo base_url('assets/front-end/css/bootsnav.css');?>" rel="stylesheet">
	
    <!-- animate.css -->
    <link href="<?php echo base_url('assets/front-end/css/animate.css'); ?>" rel="stylesheet">
	
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	
    <!-- slider -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/vegas.min.css'); ?>">
	
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.theme.default.min.css'); ?>">
	
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400i,700|Montserrat+Alternates:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

    <!-- custom css -->
    <link href="<?php echo base_url('assets/front-end/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/front-end/css/responsive.css');?>" rel="stylesheet">
	
</head>

<body>
    <section id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="fst-logo">
                                <img src="<?php echo base_url('assets/front-end/images/logo/1.png'); ?>" class="img-responsive" alt="logo">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="header-address">
                                <ul>
                                    <li><i class="fa fa-volume-control-phone fa-border" aria-hidden="true"></i><span>+8801725697412</span></li>
                                    <li><i class="fa fa-envelope fa-border" aria-hidden="true"></i><span>mail@gmail.com</span></li>
                                </ul>
                            </div>
                        </div>
						<div class="header-button col-md-3 col-sm-3">
							<div class="dropdown">
							  <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Meeting Information
							  </button>
							  <ul class="dropdown-menu" aria-labelledby="dLabel">
								<p><b>Winter Time:</b> 06.30PM</p>
								<p><b>Summer Time:</b> 06.30PM</p>
								<p><b>Place:</b> Kamria Complex, Barutkhana, East Zindabazar, Sylhet</p>
							  </ul>
							</div>
						</div>
					</div>	
                </div>
            </div>
        </div>
    </section>

    <nav class="navbar navbar-default navbar-scrollspy navbar-sticky bootsnav" data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand navbar-brand-res hidden" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="<?php echo base_url('home/index'); ?>">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Club History</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/clubHistory'); ?>">History</a></li>
                            <li><a href="<?php echo base_url('home/clubPresident'); ?>">Club President</a></li>
                            <li><a href="<?php echo base_url('home/ourLeader'); ?>">Our Leader</a></li>
                            <li><a href="<?php echo base_url('home/director'); ?>">Director</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown">Member List</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/memberList'); ?>">All Members</a></li>
                            <li><a href="<?php echo base_url('home/iCom'); ?>">INSTALLATION COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/subCom'); ?>">SUB COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/board'); ?>">BOARD OF DIRECTORS 2017-2018</a></li>
                            <li><a href="<?php echo base_url('home/pastLeaders'); ?>">Our Past Leaders</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('home/pp'); ?>">Permanent Project</a></li>
                    
                    <a class="navbar-brand navbar-brand-sec" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
                    
                    <li><a href="<?php echo base_url('home/photos'); ?>">Photo Gallery</a></li>
                    <li><a href="<?php echo base_url('home/allNews'); ?>">All News</a></li>
                    <li><a href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                   <?php if($check){ ?> <li><a href="<?php echo base_url('home/logout'); ?>">Logout</a></li>
                   <?php } else { ?> <li><a href="<?php echo base_url('home/login'); ?>">Login</a></li> <?php } ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

    <section id="after-nav">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <div id="fst-slider"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="national-anthem">
                                <div class="dv-header">
                                    <h1><span>জাতীয় সংগীত</span></h1>
                                </div>
                                <p>
										আমার সোনার বাংলা
                                    <br> আমি তোমায় ভালবাসি
                                    <br>
                                    <br> চিরদিন তোমার আকাশ ।।
                                    <br> তোমার বাতাস
                                    <br> আমার প্রানে
                                    <br> ওমা আমার প্রানে বাজায় বাঁশী
                                    <br>
                                    <br> ওমা ফাগুনে তোর আমার বনে ঘ্রানে পাগল করে
                                    <br> মরি হায়
                                    <br> হায় রে ওমা
                                    <br> ফাগুনে তোর আমার বনে ঘ্রানে পাগল করে ।
                                    <br> ওমা অগ্রানে তোর ভরা খেতে
                                    <br> কি দেখেছি
                                    <br> আমি কি দেখেছি মধুর হাসি
                                    <br> সোনার বাংলা
                                    <br> আমি তোমায় ভালবভাসি
                                    <br>
                                    <br> কি শোভা কি ছায়া গো
                                    <br> কি স্নেহ কি মায়া গো
                                    <br> কি আঁচল বিছায়েছ বটের মুলে
                                    <br> নদীর কূলে কূলে
                                    <br>
                                    <br> মা তোর মুখের বানী
                                    <br> আমার কানে লাগে সুধার মতো
                                    <br> মরি হায় , হায় রে মা তোর
                                    <br> মুখের বানী
                                    <br> আমার কানে লাগে সুধার মতো
                                    <br>
                                    <br> মা তোর বদন খানি মলিন হলে
                                    <br> আমি নয়ন
                                    <br> ওমা আমি নয়ন জ্বলে ভাসি
                                    <br>
                                    <br> সোনার বাংলা আমি তোমায় ভালবাসি ।
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="rotary-notice">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="row">
                        <div class="col-md-4">
                           
                            <div class="notice-board text-center">

                                <div class="owl-carousel">
                                    <?php if($data){ ?>
                                    <?php foreach ($data as  $value) { ?>
                                     <?php $value->eventDate= date(' dM, Y h:i A', strtotime($value->eventDate)); ?>
                                    <div class="item">
                                        <h1>Notice</h1>
                                        <hr>
                                        <h3><?php echo $value->eventTitle ?></h3>
                                        <p><strong>Venue: </strong> <?php echo $value->eventPlace ?></p>
                                        <p><strong>Time :</strong><?php echo $value->eventDate ?></p>

                                        <div class="full-notice">
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target=".show-notice-o">See Full Notice</button>

                                            <div class="modal fade show-notice-o" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h1 class="modal-title" id="gridSystemModalLabel">Notice</h1>
                                                        </div>
                                                         <?php foreach ($data as  $value) { ?>
                                                        
                                                        <div class="modal-body">
                                                            <div class="full-notice-all">
                                                                <h3><?php echo $value->eventTitle ?></h3>
                                                                <p><strong>Venue: </strong> <?php echo $value->eventPlace ?></p>
                                                                <p><strong>Time :</strong><?php echo $value->eventDate ?></p>
                                                            </div>
                                                        </div>
                                                         <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                     <?php } } else{ ?> 
                                        <div class="item">
                                            <h1>Notice</h1>
                                            <hr>
                                            <h3> No Event to Show </h3>
                                            <p><strong>Venue: </strong> </p>
                                            <p><strong>Time :</strong></p>

                                            <div class="full-notice">
                                                <button type="button" class="btn btn-default" data-toggle="modal" data-target=".show-notice-t">See Full Notice</button>

                                                <div class="modal fade show-notice-t" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h1 class="modal-title" id="gridSystemModalLabel">Notice</h1>
                                                            </div>
                                                             <?php foreach ($data as  $value) { ?>
                                                            
                                                            <div class="modal-body">
                                                                <div class="full-notice-all">
                                                                    <h3><?php echo $value->eventTitle ?></h3>
                                                                    <p><strong>Venue: </strong> <?php echo $value->eventPlace ?></p>
                                                                    <p><strong>Time :</strong><?php echo $value->eventDate ?></p>
                                                                </div>
                                                            </div>
                                                             <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                     <?php } ?>
                                </div>
                            </div>
                        </div>
						
                        <div class="col-md-8 w-rotary">
                            <div class="what-rotary">
                                <h1>What is Rotary</h1>
                                <hr>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                    <br>
                                    <br> t has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="test-news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="owl-carousel">
                                <?php //print_r($value); exit();?>
                                  <?php foreach ($valu as  $value) { ?>
                                <div class="item">
                                    <div class="news">
                                        <div class="row">

                                            <div class="col-md-7">
                                                <img src="<?php echo base_url($value->newsMainImage); ?>" class="img-responsive" alt="logo">
                                                <div class="date-icon">
                                                   <?php $dat= date('d', strtotime($value->newsDate)); ?>
                                                   <?php $month= date('F', strtotime($value->newsDate)); ?>
                                    
                                                   
                                                    <h2><?php echo $dat; ?></h2>
                                                    <h2><?php echo $month; ?></h2>
                                                    
                                                </div>
                                                <ul class="share-icon">
                                                    <li><i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-5 w-best">
                                                <div class="w-best">
                                                    <h1><?php echo $value->newsTitle; ?></h1>
                                                </div>
                                                <hr class="fst-hr">
                                                <hr class="scnd-hr">
                                                <p class="news-details">
                                                    <?php 
                                                    $this->load->helper('text');
                                                    $value->newsDetails=word_limiter($value->newsDetails, 50);
                                                    echo $value->newsDetails; ?>
                                                </p>
                                                <a target="_blank" href="<?php echo base_url("home/news/{$value->newsInfoId}"); ?>">Read More....</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-4 w-best">
                            <div class="way-best">
                                <h1>The 4 Way Test</h1>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>1.</b> Is it the true?</li>
                                    <li class="list-group-item"><b>2.</b> Is it Fair to all concern?</li>
                                    <li class="list-group-item"><b>3.</b> Ortantly it is responsible for synthesis of your DNA and RNA. A new report that ?</li>
                                    <li class="list-group-item"><b>4.</b> And enzymes. It contributes to the development of bones and?</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="object-invocation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-5 invocation">
                            <div class="main-invocation">
                                <h1>Invocation</h1>
                                <hr>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-7 object">
                            <div class="main-object">
                                <h1>Object of Rotary</h1>
                                <hr>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                    <br>
                                    <br>
                                    <b>FIRST: </b>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    <br>
                                    <br>
                                    <b>SECOND: </b>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                                    <br>
                                    <br>
                                    <b>THIRD: </b>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                                    <br>
                                    <br>
                                    <b>FOURTH: </b>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="copywrite-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cf-left">
                        <i class="fa fa-copyright" aria-hidden="true"></i>
                        <h4><strong>rotaryclub</strong> 2016</h4>
                    </div>
                    <div class="cf-right">
                        <h4>Design by StarLab IT</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/front-end/js/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/front-end/js/bootstrap.min.js'); ?>"></script>
	
    <!-- bootsnav -->
	<script src="<?php echo base_url('assets/front-end/js/bootsnav.js'); ?>"></script>
	
    <!-- slider -->
	<script src="<?php echo base_url('assets/front-end/js/vegas.min.js'); ?>"></script>
	
    <!-- owl carousle -->
	<script src="<?php echo base_url('assets/front-end/js/owl.carousel.min.js'); ?>"></script>
	
    <!-- custom js -->
	<script src="<?php echo base_url('assets/front-end/js/navbar-pic.js'); ?>"></script>
	<script src="<?php echo base_url('assets/front-end/js/script.js'); ?>"></script>
	
</body>

</html>