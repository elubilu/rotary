<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {
	
	public function get_all_members()
	{
			$data=$this->db 
						->select('*,member_type.memberTypeTitle')
						->from('member_info')
						->join('member_type','member_info.memberTypeId=member_type.memberTypeId')
						->where('memberStatus',1)
						->order_by('member_info.memberPositionType', 'asc')
						->order_by('member_info.memberName', 'asc')
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_all_events()
	{
			$data=$this->db 
						->select('*')
						->from('event_info')
						->order_by('event_info.eventDate', 'desc')
						->limit(3)
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_all_news()
	{
			$data=$this->db 
						->select('*')
						->from('news_info')
						->order_by('news_info.newsDate', 'desc')
						->limit(6)
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_news_by_id($id)
	{
			 $data=$this->db 
						->select('*')
						->from('news_info')
						->where('newsInfoId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_member_details_by_id($id)
	{
			 $data=$this->db 
						->select('*')
						->from('member_info')
						->where('memberId',$id)
						->get();
		if($data)
		{
			return $data->row();
		}
	}
	public function get_all_images_by_id($id)
	{
			 $data=$this->db 
						->select('*')
						->from('news_images')
						->where('news_info_newsId',$id)
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_all_ins_com_members($info)
	{
	 $infos=array();
         foreach ($info as  $value) {
         
			 $data=$this->db 
						->select('installation_committee_details.*,instalation_committee_info.instalationCommitteeTitle,member_info.memberId,member_info.memberName,member_info.memberImage')
						->from('installation_committee_details')
						->join('member_info','member_info.memberId=installation_committee_details.member_info_installationCommitteeMemberId')
						->join('instalation_committee_info','instalation_committee_info.instalationCommitteeInfoId=installation_committee_details.installation_committee_Info_installationCommitteeInfoId')
						->where('installation_committee_Info_installationCommitteeInfoId',$value->instalationCommitteeInfoId)
						->get();
              $infos[] = $data->result();
			}
		if($infos)
		{
			return $infos;
		}
	}
	public function get_all_ins_committee()
     {    
			 $data=$this->db 
						->select('*')
						->from('instalation_committee_info')
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_all_sub_com_members()
	{
			 $data=$this->db 
						->select('sub_committee_details.*,member_info.memberName,member_info.memberId,member_info.memberImage,sub_committee_info.*')
						->from('sub_committee_details')
						->join('member_info','member_info.memberId=sub_committee_details.member_info_subCommitteeMemberId')
						->join('sub_committee_info','sub_committee_info.subCommitteeInfoId=sub_committee_details.sub_committee_Info_subCommitteeInfoId')
						->get();
		if($data)
		{
			return $data->result();
		}
	}
	public function get_notification_details_by_id($id)
	{
			 $data=$this->db 
						->select('event_info.eventTitle,event_member_info.perMemberCost,event_member_info.eventCostPaidAmount,event_info.eventDate')
						->from('event_member_info')
						->join('event_info','event_member_info.event_info_eventId=event_info.event_infoId')
						->where('member_info_memberId',$id)
						->where('eventCostPaymentStatus !=',1)
						->get();
		
		if($data)
		{
			return $data->result();
		}
	}
	public function get_pass_by_id($id)
	{
			 $data=$this->db 
						->select('memberPassword')
						->from('member_info')
						->where('memberId',$id)
						->get();
		if($data)
		{
			return $data->row()->memberPassword;
		}
	}
	public function update_user_password($data)
	{
		//print_r($data);exit();
		$data=$this->db
		  				->where('memberId',$data['memberId'])
		   				->update('member_info',$data);
		 return  $data;
	}
}