-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2017 at 11:28 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotary`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_info`
--

CREATE TABLE `attendance_info` (
  `attendance_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_details`
--

CREATE TABLE `committee_details` (
  `committee_detailsId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_info`
--

CREATE TABLE `committee_info` (
  `committee_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `donation_info`
--

CREATE TABLE `donation_info` (
  `donation_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_info`
--

CREATE TABLE `event_info` (
  `event_infoId` int(11) NOT NULL,
  `eventTitle` varchar(600) COLLATE utf8_bin NOT NULL,
  `eventDetails` text COLLATE utf8_bin NOT NULL,
  `eventPlace` varchar(400) COLLATE utf8_bin NOT NULL,
  `event_type_eventId` int(11) NOT NULL,
  `eventDate` datetime NOT NULL,
  `eventPerHeadAmount` double NOT NULL,
  `eventStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `eventAddedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_member_info`
--

CREATE TABLE `event_member_info` (
  `eventMemberInfoId` int(11) NOT NULL,
  `event_info_eventId` int(11) NOT NULL,
  `member_info_memberId` int(11) NOT NULL,
  `perMemberCost` double NOT NULL,
  `eventCostPaymentStatus` int(2) NOT NULL DEFAULT '0' COMMENT '1=done, 2=half Payment, 0=pending ',
  `eventCostPaidAmount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_payment_info`
--

CREATE TABLE `event_payment_info` (
  `eventPaymentInfoId` int(11) NOT NULL,
  `event_info_eventId` int(11) NOT NULL,
  `eventPaidAmount` double NOT NULL,
  `member_info_memberId` int(11) NOT NULL,
  `paymentPaidType` int(2) NOT NULL COMMENT '1=paid, 0=half Payment',
  `paymentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paymentReceivedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `eventTypeId` int(100) NOT NULL,
  `eventTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`eventTypeId`, `eventTypeTitle`) VALUES
(1, 'Programme'),
(2, 'Meeting');

-- --------------------------------------------------------

--
-- Table structure for table `logo_info`
--

CREATE TABLE `logo_info` (
  `logo_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE `member_info` (
  `memberId` int(100) NOT NULL,
  `memberName` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberNumber` varchar(100) COLLATE utf8_bin NOT NULL,
  `clubId` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberJoinDate` date NOT NULL,
  `memberImage` varchar(750) COLLATE utf8_bin NOT NULL,
  `Position` varchar(100) COLLATE utf8_bin NOT NULL,
  `BloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `WeedingDate` date NOT NULL,
  `Birthday` date NOT NULL,
  `SpouseName` varchar(100) COLLATE utf8_bin NOT NULL,
  `SpouseBirthday` date NOT NULL,
  `SpouseBloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddress` text COLLATE utf8_bin NOT NULL,
  `memberMobile` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberEmail` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberPassword` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberStatus` int(2) NOT NULL DEFAULT '1' COMMENT '1=active, 0=inactive',
  `memberTypeId` int(100) NOT NULL,
  `memberPositionType` int(6) NOT NULL DEFAULT '4' COMMENT 'to sort member positions for view',
  `memberGender` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddedDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memberLoginTypeId` int(100) NOT NULL,
  `memberTotalBalance` double NOT NULL,
  `memberDueAmount` double NOT NULL,
  `memberEditDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`memberId`, `memberName`, `memberNumber`, `clubId`, `memberJoinDate`, `memberImage`, `Position`, `BloodGroup`, `WeedingDate`, `Birthday`, `SpouseName`, `SpouseBirthday`, `SpouseBloodGroup`, `memberAddress`, `memberMobile`, `memberEmail`, `memberPassword`, `memberStatus`, `memberTypeId`, `memberPositionType`, `memberGender`, `memberAddedDate`, `memberLoginTypeId`, `memberTotalBalance`, `memberDueAmount`, `memberEditDate`) VALUES
(1, 'Kshoma Kanta Chakrabartty', '12345', 'Accounting Management', '2007-12-01', 'images/01.jpg', 'President', 'B+', '1986-02-16', '1955-02-12', 'Ratna Rani Chakrabartty', '1959-05-05', 'O+', 'Mohona 104, Block#B, Korerpara, Sylhet.', '01720147149', 'rtn.kshoma@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 1, 1, 'Male', '2017-08-23 15:25:38', 1, 1000, 0, '2017-08-23 09:28:31'),
(5, 'Rtn. Ali Akbar', '02', 'Ready-made Garments', '2016-12-31', 'images/02.jpg', 'President Elect', 'O+', '2017-08-08', '2017-04-24', 'none', '2001-01-30', 'B-', 'Village - Lawai (Porbothpur)\r\nP.S - Dakshin Surma\r\nP.O - Sylhet-3100', '01720952323', 'rtnakbaratsouth@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 2, 'Male', '2017-08-26 17:04:17', 2, 0, 0, '2017-08-26 11:04:17'),
(6, 'Rtn Zubayer Ahmed', '03', 'C.I Sheet Whole Saller', '2015-03-20', 'images/032.jpg', 'President Elect', 'O+', '2017-04-13', '2017-03-12', 'Mrs. Noorjagan Fatema', '1970-01-01', 'AB-', '23 Suravi, Rajpara, Tilagorh, Sylhet-3100', '01712109583', 'zubayer722377@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 3, 'Male', '2017-08-26 17:12:40', 2, 0, 0, '2017-08-26 11:12:40'),
(7, 'Rtn. IPP Jamal Uddin Ahmed RFSM', '04', 'Wooden Product', '1970-01-01', 'images/04.jpg', 'President', 'A+', '2017-02-06', '2017-05-01', 'Salma Ahmed', '1970-01-01', '', 'House #19, Road #21, Block - B, Shahjalal Uposhohor, Sylhet-3100', '01684203028', 'syl.jamal65@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 4, 4, 'Male', '2017-08-26 17:22:06', 2, 0, 0, '2017-08-26 11:22:06'),
(8, 'Rtn. PDG. Engr. Mohammad Abdul Latif MPHF MD', '05', 'Civil Engineering', '1988-08-01', 'images/052.jpg', 'President', 'B+', '2017-04-21', '2017-03-01', 'Nadira Ahmed Latif', '2017-06-01', '', 'Jamuna Consultant,  105 Raja Mansion (2nd Floor), Zindabazar', '08212832218', 'malatif47@yahoo.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 1, 'Male', '2017-08-26 17:30:55', 1, 0, 0, '2017-08-26 11:30:55'),
(9, 'Rtn. PP Abdul Hamid Choudhury', '06', 'Iron & Steel Whole Selling & Real Estate', '2005-01-20', 'images/06.jpg', 'PP', 'O+', '2017-09-27', '2017-06-30', 'Mujiba Hamid Chowdhury', '2017-11-05', '', 'Sraboni A-4, Shah Chisti Road, Pathantola, Sylhet-3100', '01711331483', 'nai@yahoo.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 4, 'Male', '2017-08-26 17:36:03', 2, 0, 0, '2017-08-26 11:36:03'),
(10, 'Rtn PP M.A. Salam Choudhury RFSM', '07', 'Textile Manufacturing', '2017-02-01', 'images/17.jpg', 'PP', 'O+', '2017-07-02', '2017-04-02', 'Razia Sulana Chowdhury', '1970-01-01', 'B+', 'Choudhury Kutir, 149, Surmavelly, Topkhana, Sylhet-3100', '01711349056', 'nai@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 4, 'Male', '2017-08-26 17:42:34', 2, 0, 0, '2017-08-26 11:42:34'),
(11, 'Rtn. PP M.A Wahid Chowdhury RFSM', '08', 'Building Construction ', '1995-05-05', 'images/08__Rtn__PP_M_A_Wahid_Chowdhury_RFSM.jpg', 'PP', 'A+', '2017-03-16', '2017-07-18', 'Muesheda Hazera Chowdhury', '2017-12-08', '', 'Udayan-47, Elias Kandhi, Kastobir, Sylhet-3100', '01711847932', 'ssok@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 4, 4, 'Male', '2017-08-26 17:51:26', 2, 0, 0, '2017-08-26 11:51:26'),
(12, 'Rtn. PP Md. Abdul Malik Suzon RFSM', '09', 'Building Construction ', '2017-08-03', 'images/09__Rtn__PP_Md__Abdul_Malik_Suzon_RFSM.jpg', 'PP', 'A-', '2017-03-29', '2017-04-23', 'Asia Khanam Sikdar', '2017-02-26', '', 'Sitara Monzil, West kajidighi, Kazitula, Sylhet-3100', '01199303960', 'shosyl@hotmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 4, 'Male', '2017-08-26 18:03:38', 1, 0, 0, '2017-08-26 12:03:38'),
(13, 'Rtn. PP Abul Khair Muhammed Ataul Karim PHF', '10', 'Small Industry Set', '1999-09-10', 'images/10__Rtn__PP_Abul_Khair_Muhammed_Ataul_Karim_PHF.jpg', 'PP', 'O+', '2017-04-14', '2017-01-01', 'sabera Akter Chowdhury', '1970-01-01', '', 'Abu Bakar MArchant &amp; Son, New Market, Laldighirpar,Sylhet', '01718094028', 'sholima2002@gmail.com', '25d55ad283aa400af464c76d713c07ad', 1, 5, 4, 'Male', '2017-08-26 18:14:46', 2, 0, 0, '2017-08-26 12:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `member_login_type`
--

CREATE TABLE `member_login_type` (
  `memberLoginTypeId` int(100) NOT NULL,
  `memberLoginTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_login_type`
--

INSERT INTO `member_login_type` (`memberLoginTypeId`, `memberLoginTypeTitle`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `member_payment_info`
--

CREATE TABLE `member_payment_info` (
  `member_payment_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_type`
--

CREATE TABLE `member_type` (
  `memberTypeId` int(100) NOT NULL,
  `memberTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_type`
--

INSERT INTO `member_type` (`memberTypeId`, `memberTypeTitle`) VALUES
(1, 'MPHF'),
(2, 'MPHF MD'),
(3, 'PHF'),
(4, 'RFSM'),
(5, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `news_info`
--

CREATE TABLE `news_info` (
  `news_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_info`
--
ALTER TABLE `attendance_info`
  ADD PRIMARY KEY (`attendance_infoId`);

--
-- Indexes for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD PRIMARY KEY (`committee_detailsId`);

--
-- Indexes for table `committee_info`
--
ALTER TABLE `committee_info`
  ADD PRIMARY KEY (`committee_infoId`);

--
-- Indexes for table `donation_info`
--
ALTER TABLE `donation_info`
  ADD PRIMARY KEY (`donation_infoId`);

--
-- Indexes for table `event_info`
--
ALTER TABLE `event_info`
  ADD PRIMARY KEY (`event_infoId`);

--
-- Indexes for table `event_member_info`
--
ALTER TABLE `event_member_info`
  ADD PRIMARY KEY (`eventMemberInfoId`);

--
-- Indexes for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  ADD PRIMARY KEY (`eventPaymentInfoId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`eventTypeId`);

--
-- Indexes for table `logo_info`
--
ALTER TABLE `logo_info`
  ADD PRIMARY KEY (`logo_infoId`);

--
-- Indexes for table `member_info`
--
ALTER TABLE `member_info`
  ADD PRIMARY KEY (`memberId`),
  ADD UNIQUE KEY `memberEmail` (`memberEmail`),
  ADD UNIQUE KEY `memberNumber` (`memberNumber`);

--
-- Indexes for table `member_login_type`
--
ALTER TABLE `member_login_type`
  ADD PRIMARY KEY (`memberLoginTypeId`);

--
-- Indexes for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  ADD PRIMARY KEY (`member_payment_infoId`);

--
-- Indexes for table `member_type`
--
ALTER TABLE `member_type`
  ADD PRIMARY KEY (`memberTypeId`);

--
-- Indexes for table `news_info`
--
ALTER TABLE `news_info`
  ADD PRIMARY KEY (`news_infoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_info`
--
ALTER TABLE `attendance_info`
  MODIFY `attendance_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_details`
--
ALTER TABLE `committee_details`
  MODIFY `committee_detailsId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_info`
--
ALTER TABLE `committee_info`
  MODIFY `committee_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donation_info`
--
ALTER TABLE `donation_info`
  MODIFY `donation_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_info`
--
ALTER TABLE `event_info`
  MODIFY `event_infoId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_member_info`
--
ALTER TABLE `event_member_info`
  MODIFY `eventMemberInfoId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  MODIFY `eventPaymentInfoId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `eventTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `logo_info`
--
ALTER TABLE `logo_info`
  MODIFY `logo_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_info`
--
ALTER TABLE `member_info`
  MODIFY `memberId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `member_login_type`
--
ALTER TABLE `member_login_type`
  MODIFY `memberLoginTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  MODIFY `member_payment_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_type`
--
ALTER TABLE `member_type`
  MODIFY `memberTypeId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `news_info`
--
ALTER TABLE `news_info`
  MODIFY `news_infoId` int(100) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
