$(function() {
    $('#side-menu').metisMenu();
});

/* Multi Select */
$(function() {
	$('#multipleSelect').multiselect({
        buttonWidth: '100%',
        buttonHeight: '34px',
    });
});

/* Select 2 */
$(function() {
    $('.forselect2').select2();
});

/* ToolTip */
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});


/* Quantity Increase */
$(function () {
  var q = $('.quantity');
	var something = 100;
	q.each(function(){
		var $this = $(this),
			button = $this.children('button'),
			input = $this.children('input[type="number"]'),
			val = +input.val();

		button.on('click',function(){
			if($(this).hasClass('minus')){
				if(val === 1) return false;
				else input.val(--val);
			}
			else{
				if(val === something )return false;
				else input.val(++val);
			}
		});
		
	});
});

/* Nice Scroll */
$(function(){ 
	$(".order-table-container .thumbnail").niceScroll({cursorcolor:"#D9534F"});
	$(".panel-body").niceScroll({cursorcolor:"#D9534F", zindex:"999"});
});
/*$(function () {
	var windowHeight = $(this).height();
	var navHeight = $('.top-navbar').height();
	var pageWrapperHeight = $('#page-wrapper').height();
	//alert(pageWrapperHeight);
	var result = windowHeight - pageWrapperHeight - navHeight + 25;
	$('.footer').css('margin-top', result);
});*/

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});
$(function() {
   /* $(".abcd").autofix_anything({
		customOffset: 60, // You can define custom offset for when you want the container to be fixed. This option takes the number of pixels from the top of the page. The default value is false which the plugin will automatically fix the container when the it is in the viewport
		manual: false, // Toggle this to true if you wish to manually fix containers with the public method. Default value is false
		onlyInContainer: false // Set this to false if you don't want the fixed container to limit itself to the parent's container.
	});*/
});

/* Affix */
$(function() {
	$('#abcd').affix({
		offset: {
			top: 60,
			bottom: 100
		}
	});	
	$('#xyz').affix({
		offset: {
			top: 60,
			bottom: 100
		}
	});	
	
});
/* Chevron Rotator */
$(function() {
	$(".panel-heading").click(function(){
		$('.rotate').toggleClass("down") ; 
	});
});

/* Active Link */
$(function() {
	$('.menu-name-container li a').on('click', function(){ 
		$('li').removeClass('active');
		$(this).parent().addClass('active');
	});
});


/* Collapse Setup */
$(function() {
	$('.targetDiv').hide();
	$('.menuNameButtons a').click(function(){
		$('.targetDiv').hide();
		$('#menuDetails'+$(this).attr('id')).show();
		$(this).focus('background-color','#cfcfcf');
	});
});

$(function(){ 
	$(".sidebar").niceScroll({cursorcolor:"#D9534F"});
});
$(function(){ 
	$('#startDate').datepicker();
    $('#endDate').datepicker();	
});

















