-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2017 at 03:16 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotary`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_info`
--

CREATE TABLE `attendance_info` (
  `attendance_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `committee_details`
--

CREATE TABLE `committee_details` (
  `committee_detailsId` int(100) NOT NULL,
  `committee_member_title` varchar(100) COLLATE utf8_bin NOT NULL,
  `committee_member_type` varchar(100) COLLATE utf8_bin NOT NULL,
  `committee_member_responsibility` varchar(100) COLLATE utf8_bin NOT NULL,
  `committee_member_added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `committee_details`
--

INSERT INTO `committee_details` (`committee_detailsId`, `committee_member_title`, `committee_member_type`, `committee_member_responsibility`, `committee_member_added_date`) VALUES
(1, 'Hello', '2', 'Norhing', '2017-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `committee_info`
--

CREATE TABLE `committee_info` (
  `committee_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `donation_info`
--

CREATE TABLE `donation_info` (
  `donation_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `event_info`
--

CREATE TABLE `event_info` (
  `event_infoId` int(100) NOT NULL,
  `event_typeId` int(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_cost` double NOT NULL,
  `event_title` varchar(100) COLLATE utf8_bin NOT NULL,
  `event_details` text COLLATE utf8_bin NOT NULL,
  `event_place` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_info`
--

INSERT INTO `event_info` (`event_infoId`, `event_typeId`, `event_date`, `event_cost`, `event_title`, `event_details`, `event_place`) VALUES
(1, 1, '2017-08-09', 1000, 'meet', 'hello', 'Sylhet');

-- --------------------------------------------------------

--
-- Table structure for table `event_payment_info`
--

CREATE TABLE `event_payment_info` (
  `event_payment_infoId` int(100) NOT NULL,
  `payment_nameId` varchar(100) COLLATE utf8_bin NOT NULL,
  `payment_event_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `payment_amount` double NOT NULL,
  `payment_TypeId` varchar(100) COLLATE utf8_bin NOT NULL,
  `payment_due` double NOT NULL,
  `payment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `event_payment_info`
--

INSERT INTO `event_payment_info` (`event_payment_infoId`, `payment_nameId`, `payment_event_name`, `payment_amount`, `payment_TypeId`, `payment_due`, `payment_date`) VALUES
(2, '1', '2', 10000, '1', 100, '2017-08-15'),
(3, '1', '1', 10000, '1', 1000, '2017-08-15');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `eventTypeId` int(100) NOT NULL,
  `eventTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `logo_info`
--

CREATE TABLE `logo_info` (
  `logo_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE `member_info` (
  `memberId` int(100) NOT NULL,
  `memberName` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberNumber` varchar(100) COLLATE utf8_bin NOT NULL,
  `clubId` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberJoinDate` date NOT NULL,
  `Position` varchar(100) COLLATE utf8_bin NOT NULL,
  `BloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `WeedingDate` date NOT NULL,
  `Birthday` date NOT NULL,
  `SpouseName` varchar(100) COLLATE utf8_bin NOT NULL,
  `SpouseBirthday` date NOT NULL,
  `SpouseBloodGroup` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddress` text COLLATE utf8_bin NOT NULL,
  `memberMobile` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberEmail` varchar(100) COLLATE utf8_bin NOT NULL,
  `Status` int(100) NOT NULL,
  `memberTypeId` int(100) NOT NULL,
  `memberGender` varchar(100) COLLATE utf8_bin NOT NULL,
  `memberAddedDate` date NOT NULL,
  `memberLoginTypeId` int(100) NOT NULL,
  `memberTotalBalance` double NOT NULL,
  `memberDueAmount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`memberId`, `memberName`, `memberNumber`, `clubId`, `memberJoinDate`, `Position`, `BloodGroup`, `WeedingDate`, `Birthday`, `SpouseName`, `SpouseBirthday`, `SpouseBloodGroup`, `memberAddress`, `memberMobile`, `memberEmail`, `Status`, `memberTypeId`, `memberGender`, `memberAddedDate`, `memberLoginTypeId`, `memberTotalBalance`, `memberDueAmount`) VALUES
(3, 'Ahmed Khan', '2', '2', '2017-08-08', 'Member', 'B+', '2017-08-09', '2017-08-02', 'Hello', '2017-08-02', 'B+', 'Test', '01748084106', 'rasel@starlabit.com', 0, 1, '1', '2017-08-13', 1, 2500, 500);

-- --------------------------------------------------------

--
-- Table structure for table `member_login_type`
--

CREATE TABLE `member_login_type` (
  `memberLoginTypeId` int(100) NOT NULL,
  `memberLoginTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_payment_info`
--

CREATE TABLE `member_payment_info` (
  `member_payment_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `member_type`
--

CREATE TABLE `member_type` (
  `memberTypeId` int(100) NOT NULL,
  `memberTypeTitle` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_info`
--

CREATE TABLE `news_info` (
  `news_infoId` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_info`
--
ALTER TABLE `attendance_info`
  ADD PRIMARY KEY (`attendance_infoId`);

--
-- Indexes for table `committee_details`
--
ALTER TABLE `committee_details`
  ADD PRIMARY KEY (`committee_detailsId`);

--
-- Indexes for table `committee_info`
--
ALTER TABLE `committee_info`
  ADD PRIMARY KEY (`committee_infoId`);

--
-- Indexes for table `donation_info`
--
ALTER TABLE `donation_info`
  ADD PRIMARY KEY (`donation_infoId`);

--
-- Indexes for table `event_info`
--
ALTER TABLE `event_info`
  ADD PRIMARY KEY (`event_infoId`);

--
-- Indexes for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  ADD PRIMARY KEY (`event_payment_infoId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`eventTypeId`);

--
-- Indexes for table `logo_info`
--
ALTER TABLE `logo_info`
  ADD PRIMARY KEY (`logo_infoId`);

--
-- Indexes for table `member_info`
--
ALTER TABLE `member_info`
  ADD PRIMARY KEY (`memberId`),
  ADD UNIQUE KEY `memberEmail` (`memberEmail`),
  ADD UNIQUE KEY `memberNumber` (`memberNumber`);

--
-- Indexes for table `member_login_type`
--
ALTER TABLE `member_login_type`
  ADD PRIMARY KEY (`memberLoginTypeId`);

--
-- Indexes for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  ADD PRIMARY KEY (`member_payment_infoId`);

--
-- Indexes for table `member_type`
--
ALTER TABLE `member_type`
  ADD PRIMARY KEY (`memberTypeId`);

--
-- Indexes for table `news_info`
--
ALTER TABLE `news_info`
  ADD PRIMARY KEY (`news_infoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_info`
--
ALTER TABLE `attendance_info`
  MODIFY `attendance_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committee_details`
--
ALTER TABLE `committee_details`
  MODIFY `committee_detailsId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `committee_info`
--
ALTER TABLE `committee_info`
  MODIFY `committee_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donation_info`
--
ALTER TABLE `donation_info`
  MODIFY `donation_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_info`
--
ALTER TABLE `event_info`
  MODIFY `event_infoId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `event_payment_info`
--
ALTER TABLE `event_payment_info`
  MODIFY `event_payment_infoId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `eventTypeId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logo_info`
--
ALTER TABLE `logo_info`
  MODIFY `logo_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_info`
--
ALTER TABLE `member_info`
  MODIFY `memberId` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `member_login_type`
--
ALTER TABLE `member_login_type`
  MODIFY `memberLoginTypeId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_payment_info`
--
ALTER TABLE `member_payment_info`
  MODIFY `member_payment_infoId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_type`
--
ALTER TABLE `member_type`
  MODIFY `memberTypeId` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_info`
--
ALTER TABLE `news_info`
  MODIFY `news_infoId` int(100) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
