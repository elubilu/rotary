<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Permanent Project</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/bootstrap.min.css');?>">
    
    
    
    <!-- bootsnav -->
    <link href="<?php echo base_url('assets/front-end/css/bootsnav.css');?>" rel="stylesheet">
    
    <!-- animate.css -->
    <link href="<?php echo base_url('assets/front-end/css/animate.css'); ?>" rel="stylesheet">
    
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- slider -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/vegas.min.css'); ?>">
    
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.theme.default.min.css'); ?>">
    
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400i,700|Montserrat+Alternates:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

    <!-- custom css -->
    <link href="<?php echo base_url('assets/front-end/css/permanent_project.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/front-end/css/responsive.css');?>" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top bootsnav" data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand navbar-brand-res hidden" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="<?php echo base_url('home/index'); ?>">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Club History</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/clubHistory'); ?>">History</a></li>
                            <li><a href="<?php echo base_url('home/clubPresident'); ?>">Club President</a></li>
                            <li><a href="<?php echo base_url('home/ourLeader'); ?>">Our Leader</a></li>
                            <li><a href="<?php echo base_url('home/director'); ?>">Director</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown">Member List</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/memberList'); ?>">All Members</a></li>
                            <li><a href="<?php echo base_url('home/iCom'); ?>">INSTALLATION COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/subCom'); ?>">SUB COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/board'); ?>">BOARD OF DIRECTORS 2017-2018</a></li>
                            <li><a href="<?php echo base_url('home/pastLeaders'); ?>">Our Past Leaders</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('home/pp'); ?>">Permanent Project</a></li>
                    
                    <a class="navbar-brand navbar-brand-sec" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
                    
                    <li><a href="<?php echo base_url('home/photos'); ?>">Photo Gallery</a></li>
                    <li><a href="<?php echo base_url('home/allNews'); ?>">All News</a></li>
                    <li><a href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                     <?php if($check){ ?> <li><a href="<?php echo base_url('home/logout'); ?>">Logout</a></li>
                   <?php } else { ?> <li><a href="<?php echo base_url('home/login'); ?>">Login</a></li> <?php } ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

    <section id="club-history">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="history-heading text-center">
                        <h1><span>PERMANENT PROJECTS</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="project">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-project">
                        <div class="projects">
                            <h3><strong>Jalalabad Rotary Disabled Rehabilitation Center & Hospital:</strong></h3>
                            <hr>
                            <p>
                                It is one of the best signature projects in Bangladesh Rotary for which the members of Rotary club of Jalalabad take immense pride for being its initiator and manager. The idea of establishing the center was picked up by the Past President and now PDG Dr. Monzurul Hoque Choudhury during his visit to Texas in 1996 as Team Leader of Group Study Exchange. The center was started with the zeal and financial support of the members of the club and got momentum with the visit of a team of expert from Abilene Rehab center, Texas, USA headed by PDG Janet Holland and spontaneous support from home and abroad. The arrangement of fund for Elevator and Generator by Mr. Aminul Islam and Mr. Bill Sinclear (UK) deserve special mention. Apart from that a substantial amount of fund was raised from different generous donors from UK with the assistance of UK Support Group. Recently US Support Group has also been formed with the support of Rotary Club of Central Bronx, USA. The piece of land received from Jatiyo Shikka Kendro on long term lease facilitated the Hospital to own a 5-story building for its regular operation.
                                <br>
                                <br> The Center is run and managed by a Management Board and in addition there is also a Board of Trustee. JDRCH, also known as Jalalabad Rotary Hospital started out patient service from January 2001 and formally started its operation with indoor and out door services to facilitate physiotherapy, electro therapy & occupational therapy for people with disability from 25th January 2002; the management is pursuing a policy to improve and update its services, particularly with the patronization of honorable and valued donors. The center is located at Hazrath Manik Pir (R:) Road, Kumarpara, Sylhet; Phone: 0821- 720881. E.mail: jrotaryhospice@gmail.com
                                <br>
                                <br> PDG Rtn. Dr. Monzurul Hoque Choudhury is the Founder Chairman of Management Board and Chairman of Trustee Board. Subsequently, the Management Board was headed by Rtn. PP Mahboob Subhani Choudhury as Acting Chairman & Chairman; PDG Rtn. Dr. Monzurul Hoque Choudhury and Rtn. PP Dr. Mohammed Abdus Salam as chairman; the current Chairman is Rtn. PP Mahboob Subhani Choudhury. The members of the Rotary Club of Jalalabad have a dream to make it a center of excellence with diversified service opportunities.
                            </p>
                        </div>
                        <div class="projects">
                            <h3><strong>Free Friday Clinic:</strong></h3>
                            <hr>
                            <p>
                                Aimed at taking care of primary health & nutrition of underprivileged people of the society where medical service is provided by our club member Rtn. Dr. Shamim Ahmed on every Friday (9.30 a.m to 11.30 a.m) at Mojumdary, Sylhet. This project was started in 1995, while Rtn. M.A. Mannan was president of the club.
                            </p>
                        </div>
                        <div class="projects">
                            <h3><strong>Jalalabad Rotary School:</strong></h3>
                            <hr>
                            <p>
                                To ensure the functional literacy for the underprivileged children, a school was launched at Eidgah Sylhet on August 25, 2003 with the initiative of Rtn. PP Adv. Saleh Ahmed PHF. The spontaneous support of some generous club members and continuous involvement of Rtn. Latifa Jahangir had facilitated functional education for the under privileged. In 2010, Rtn. Jamil Ahmed Chowdhury provided a boost to the project by ensuring collaboration with a US based Bangladesh charity BACHAO (Bangladesh American Charitable Organization)
                            </p>
                        </div>
                        <div class="projects">
                            <h3><strong>Jalalabad Rotary Literacy Fund:</strong></h3>
                            <hr>
                            <p>
                                Launched during the tenure of club president (2008-’09) Rtn. PP Prof. Dr. Mohammed Abdus Salam PHF, MC with an aim at awarding scholarships each year to meritorious and underprivileged students of secondary schools. Offering himself the seed money for Tk. 50,000/- (fifty thousand), Rtn. Salam also maintained his determination to raise the endowment fund to Tk. 10,00,000/- (ten lac) to achieve the desired goal.
                            </p>
                        </div>
                        <div class="projects">
                            <h3><strong>Our Corporate Partner:</strong></h3>
                            <hr>
                            <p>
                                Since 2005 Chevron-Bangladesh has been working with Rotary Club of Jalalabad for implementing different projects as our corporate partner. Chevron’s contribution has exceeded taka 3.2 million in materializing projects like Eye camps, Road safety awareness, Development of Rehab Center etc.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="copywrite-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cf-left">
                        <i class="fa fa-copyright" aria-hidden="true"></i>
                        <h4><strong>rotaryclub</strong> 2016</h4>
                    </div>
                    <div class="cf-right">
                        <h4>Design by StarLab IT</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/front-end/js/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/front-end/js/bootstrap.min.js'); ?>"></script>
    
    <!-- bootsnav -->
    <script src="<?php echo base_url('assets/front-end/js/bootsnav.js'); ?>"></script>
    
    <!-- slider -->
    <script src="<?php echo base_url('assets/front-end/js/vegas.min.js'); ?>"></script>
    
    <!-- owl carousle -->
    <script src="<?php echo base_url('assets/front-end/js/owl.carousel.min.js'); ?>"></script>
    
    <!-- custom js -->
    <script src="<?php echo base_url('assets/front-end/js/navbar-pic.js'); ?>"></script>

</body>

</html>