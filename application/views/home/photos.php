<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Club Photos</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/bootstrap.min.css');?>">
    
    <!-- custom css -->
    <link href="<?php echo base_url('assets/front-end/css/photos.css');?>" rel="stylesheet">
    
    <!-- bootsnav -->
    <link href="<?php echo base_url('assets/front-end/css/bootsnav.css');?>" rel="stylesheet">
    
    <!-- animate.css -->
    <link href="<?php echo base_url('assets/front-end/css/animate.css'); ?>" rel="stylesheet">
    
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- slider -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/vegas.min.css'); ?>">
    
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/front-end/css/owl.theme.default.min.css'); ?>">
    
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,400i,700|Montserrat+Alternates:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">

    <!-- custom css -->
    <link href="<?php echo base_url('assets/front-end/css/photos.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/front-end/css/responsive.css');?>" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top bootsnav" data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand navbar-brand-res hidden" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="<?php echo base_url('home/index'); ?>">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">Club History</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/clubHistory'); ?>">History</a></li>
                            <li><a href="<?php echo base_url('home/clubPresident'); ?>">Club President</a></li>
                            <li><a href="<?php echo base_url('home/ourLeader'); ?>">Our Leader</a></li>
                            <li><a href="<?php echo base_url('home/director'); ?>">Director</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown">Member List</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('home/memberList'); ?>">All Members</a></li>
                            <li><a href="<?php echo base_url('home/iCom'); ?>">INSTALLATION COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/subCom'); ?>">SUB COMMITTEE</a></li>
                            <li><a href="<?php echo base_url('home/board'); ?>">BOARD OF DIRECTORS 2017-2018</a></li>
                            <li><a href="<?php echo base_url('home/pastLeaders'); ?>">Our Past Leaders</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('home/pp'); ?>">Permanent Project</a></li>
                    
                    <a class="navbar-brand navbar-brand-sec" href="#brand"><img src="<?php echo base_url('assets/front-end/images/logo/2.png'); ?>" class="logo" alt=""></a>
                    
                    <li><a href="<?php echo base_url('home/photos'); ?>">Photo Gallery</a></li>
                    <li><a href="<?php echo base_url('home/allNews'); ?>">All News</a></li>
                    <li><a href="<?php echo base_url('home/contactUs'); ?>">Contact Us</a></li>
                     <?php if($check){ ?> <li><a href="<?php echo base_url('home/logout'); ?>">Logout</a></li>
                   <?php } else { ?> <li><a href="<?php echo base_url('home/login'); ?>">Login</a></li> <?php } ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

    <section id="club-history">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="history-heading text-center">
                        <h1><span>Club Photos</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="photos">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slide-show-button">
                      <div class="slide-btn">
                        <button class="btn btn-default" type="submit">See Slideshow</button>
                      </div>
                      <div class="thumb-btn">
                        <button class="btn btn-default hidden" type="submit">See Thumbnail</button>
                      </div>
                    </div>
                    <div class="owl-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/7.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/7.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/10.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/10.jpg'); ?>"</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/8.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/8.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/9.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/6.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/6.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/7.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/7.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/1.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/4.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <div class="img-cont">
                                            <a href="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/front-end/images/slider/5.jpg'); ?>" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="slideshow hidden">
                  <div id="photo-slider"></div>
                </div>
              </div>
            </div>
        </div>
    </section>

    <section id="copywrite-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cf-left">
                        <i class="fa fa-copyright" aria-hidden="true"></i>
                        <h4><strong>rotaryclub</strong> 2016</h4>
                    </div>
                    <div class="cf-right">
                        <h4>Design by StarLab IT</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- jQuery -->
    <script src="<?php echo base_url('assets/front-end/js/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/front-end/js/bootstrap.min.js'); ?>"></script>
    
    <!-- bootsnav -->
    <script src="<?php echo base_url('assets/front-end/js/bootsnav.js'); ?>"></script>
    
    <!-- slider -->
    <script src="<?php echo base_url('assets/front-end/js/vegas.min.js'); ?>"></script>
    
    <!-- owl carousle -->
    <script src="<?php echo base_url('assets/front-end/js/owl.carousel.min.js'); ?>"></script>
    
    <!-- custom js -->
    <script src="<?php echo base_url('assets/front-end/js/navbar-pic.js'); ?>"></script>
    <script src="<?php echo base_url('assets/front-end/js/photos.js'); ?>"></script>
</body>

</html>